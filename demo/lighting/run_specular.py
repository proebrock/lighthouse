import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_load, mesh_transform, \
    mesh_set_vertex_colors, mesh_create_sphere, geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.light_point import LightPoint
from camsimlib.material import Material



if __name__ == '__main__':
    # Create camera and set its model parameters
    cam = CameraModel(chip_size=(40, 30),
                      focal_length=(50, 55),
    )
    cam.scale_resolution(40)

    # Create mesh
    mesh = mesh_load('../../data/knot.ply')
    mesh_set_vertex_colors(mesh, (0, 0, 1))
    mesh.translate(-mesh.get_center()) # De-mean
    mesh.compute_vertex_normals()
    mesh_transform(mesh, Trafo3d(t=[0, 0, 350], rpy=np.deg2rad([-5, 10, 0])))

    # Lights and materials
    point_light = LightPoint(position=(250, 250, 0), color=(1, 1, 1))
    material = Material(ka=0.1, kd=0.9, ks=0.9, alpha=30)

    if True:
        # Visualize scene
        cam_cs = cam.get_cs(size=50.0)
        cam_frustum = cam.get_frustum(size=300.0)
        point_light_sphere = mesh_create_sphere(radius=20.0)
        point_light_sphere.translate(point_light.get_position())
        mesh_set_vertex_colors(point_light_sphere, (1, 1, 0))
        geometries_visualize([cam_cs, cam_frustum, point_light_sphere, mesh])

    # Snap image
    tic = time.monotonic()
    depth_image, color_image, pcl = cam.snap([mesh],
        materials=[material], lights=[point_light])
    toc = time.monotonic()
    print(f'Snapping image took {(toc - tic):.1f}s')

    # Show image
    fig, ax = plt.subplots()
    nanidx = np.where(np.isnan(color_image))
    img = color_image.copy()
    img[nanidx[0], nanidx[1], :] = (0, 1, 1)
    ax.imshow(img)
    ax.set_axis_off()
    plt.show()
