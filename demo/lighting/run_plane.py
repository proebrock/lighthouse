import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_transform, mesh_create_image, \
    geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.light_point import LightPoint
from camsimlib.light_parallel import LightParallel



if __name__ == '__main__':
    # Create camera and set its model parameters
    cam = CameraModel(chip_size=(40, 30),
                      focal_length=(50, 50))
    cam.scale_resolution(4)

    img = 255 * np.ones((100, 100, 3))
    mesh = mesh_create_image(img, pixel_size=5.0)
    mesh.translate(-mesh.get_center()) # De-mean
    mesh_transform(mesh, Trafo3d(t=[0, 0, 350], rpy=np.deg2rad([0, 0.2, -0.3])))
    mesh.compute_vertex_normals()

    if True:
        # Visualize scene
        cam_cs = cam.get_cs(size=50.0)
        cam_frustum = cam.get_frustum(size=300.0)
        geometries_visualize([cam_cs, cam_frustum, mesh])

    if True:
        lights = [
            LightPoint((-100, 0, 0)),
            LightPoint((0, 0, 0)),
            LightPoint((100, 0, 0)),
            LightParallel((0, 0, 1)),
            LightParallel((0, 1, 1)),
            LightParallel((0, 2, 1)),
        ]
        use_colormap = (True, True, True, False, False, False)
        fig = plt.figure()
        for i in range(len(lights)):
            print(f'Snapping image {i+1}/{len(lights)}')
            tic = time.monotonic()
            depth_image, color_image, pcl = cam.snap([ mesh ], \
                lights=[ lights[i] ])
            toc = time.monotonic()
            print(f'    Snapping image {i+1}/6 took {(toc - tic):.1f}s')
            ax = fig.add_subplot(2, 3, i+1)
            if use_colormap[i]:
                ax.imshow(color_image[:,:,0], cmap='gray')
            else:
                ax.imshow(color_image)
            ax.set_axis_off()
            ax.set_aspect('equal')
            ax.set_title(str(lights[i]))

    plt.show()
