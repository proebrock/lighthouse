import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_load, mesh_transform, \
    mesh_set_vertex_colors, geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.light_point import LightPoint
from camsimlib.light_parallel import LightParallel



if __name__ == '__main__':
    # Create camera and set its model parameters
    cam = CameraModel(chip_size=(40, 30),
                      focal_length=(50, 55),
                      distortion=(-0.1, 0.1, 0.05, -0.05, 0.2, 0.08),
    )
    cam.scale_resolution(40)

    # Create mesh
    mesh = mesh_load('../../data/knot.ply')
    mesh_set_vertex_colors(mesh, (1, 0, 0))
    mesh.translate(-mesh.get_center()) # De-mean
    mesh.compute_vertex_normals()
    mesh_transform(mesh, Trafo3d(t=[0, 0, 350], rpy=np.deg2rad([-5, 10, 0])))

    if True:
        # Visualize scene
        cam_cs = cam.get_cs(size=50.0)
        cam_frustum = cam.get_frustum(size=300.0)
        geometries_visualize([cam_cs, cam_frustum, mesh])

    if True:
        lights = [
            LightPoint((-100, 0, 0)),
            LightPoint((0, 0, 0)),
            LightPoint((100, 0, 0)),
            LightParallel((0, -1, 1)),
            LightParallel((0, 0, 1)),
            LightParallel((0, 1, 1)),
        ]
        fig = plt.figure()
        for i in range(len(lights)):
            print(f'Snapping image {i+1}/{len(lights)}')
            tic = time.monotonic()
            depth_image, color_image, pcl = cam.snap([ mesh ], \
                lights=[ lights[i] ])
            toc = time.monotonic()
            print(f'    Snapping image {i+1}/6 took {(toc - tic):.1f}s')
            ax = fig.add_subplot(2, 3, i+1)
            ax.imshow(color_image)
            ax.set_axis_off()
            ax.set_aspect('equal')
            ax.set_title(str(lights[i]))

    plt.show()
