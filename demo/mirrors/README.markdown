# Lighthouse: Mirrors

To demonstrate mirrors and reflections, we place a 3D object (fox head) into a scene. The red surface is a mirror, that is slightly concave. The blue frame is a solid frame. We have a camera that sees parts of the fox head and the image in the mirror. The yellow sphere shows the position of a point light source.

![](images/fox_scene.png)

The result shows the distorted reflected image due to the curvature of the mirror as well as the original fox head. The lighting of the head is correct, the point light is located to the left, so the reflected right side of the head appears darker.

![](images/fox_image.png)

Another example is the so-called "dent mirror" used to detect small dents in a car body or paint work. To assess the damage, a screen with black and white stripes is placed next to the car body.

![](images/screen_scene.png)

The dent appears in the mirrored image of the screen as a disturbance of the stripe pattern.

![](images/screen_image.png)
