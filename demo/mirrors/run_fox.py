import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_create, mesh_create_cs, mesh_load, mesh_set_vertex_colors, \
    mesh_create_sphere, mesh_create_surface_from_function, geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.material import Material
from camsimlib.light_point import LightPoint



def generate_frame(inner_width, outer_width):
    vertex_positions = np.array((
        ( outer_width/2.0,  outer_width/2.0, 0.0),
        (-outer_width/2.0,  outer_width/2.0, 0.0),
        (-outer_width/2.0, -outer_width/2.0, 0.0),
        ( outer_width/2.0, -outer_width/2.0, 0.0),
        ( inner_width/2.0,  inner_width/2.0, 0.0),
        (-inner_width/2.0,  inner_width/2.0, 0.0),
        (-inner_width/2.0, -inner_width/2.0, 0.0),
        ( inner_width/2.0, -inner_width/2.0, 0.0),
        ( inner_width/2.0,  outer_width/2.0, 0.0),
        (-outer_width/2.0,  inner_width/2.0, 0.0),
        (-inner_width/2.0, -outer_width/2.0, 0.0),
        ( outer_width/2.0, -inner_width/2.0, 0.0),
    ))
    triangle_indices = np.array((
        (0, 8, 11),
        (1, 9, 8),
        (2, 10, 9),
        (3, 11, 10),
        (4, 8, 9),
        (5, 9, 10),
        (6, 10, 11),
        (7, 11, 8),
    ), dtype=int)
    mesh = mesh_create(vertex_positions, triangle_indices)
    mesh.compute_vertex_normals()
    return mesh



if __name__ == '__main__':
    # Camera
    cam = CameraModel(chip_size=(120, 90),
                      focal_length=(100, 100),
                    )
    cam.scale_resolution(20)
    cam.place((400, 0, -400))
    cam.look_at((0, 0, 0))
    cam.roll(np.deg2rad(90))

    # Mirror
    fun = lambda x: np.cos(x[:, 0]) * np.cos(x[:, 1])
    mirror = mesh_create_surface_from_function(fun,
        xrange=(-np.pi/2, np.pi/2), yrange=(-np.pi/2, np.pi/2),
        num=(50, 50), scale=(360.0, 360.0, -25.0))
    mirror.translate((-180, -180, 0.0))
    T = Trafo3d(rpy=np.deg2rad((0, 180.0-45/2, 0)))
    mirror.transform(T.get_homogeneous_matrix())
    mesh_set_vertex_colors(mirror, (1, 0, 0))

    # Mirror frame
    frame = generate_frame(360, 390)
    T = Trafo3d(rpy=np.deg2rad((0, 180.0-45/2, 0)))
    frame.transform(T.get_homogeneous_matrix())
    mesh_set_vertex_colors(frame, (0, 0, 1))

    # Object
    fox = mesh_load('../../data/fox_head.ply')
    fox.translate(-fox.get_center())
    fox.scale(100.0, center=(0, 0, 0))
    fox.translate((0.0, 0.0, -250.0))
    fox.compute_vertex_normals()

    # Lights
    point_light = LightPoint((300, 0, 0))

    # Visualize scene
    if True:
        world_cs = mesh_create_cs(size=100.0)
        cam_cs = cam.get_cs(50.0)
        cam_frustum = cam.get_frustum(200.0)
        point_light_sphere = mesh_create_sphere(radius=30.0)
        point_light_sphere.translate(point_light.get_position())
        mesh_set_vertex_colors(point_light_sphere, (1, 1, 0))
        geometries_visualize([world_cs, cam_cs, cam_frustum, \
            point_light_sphere, fox, mirror, frame])

    # Snap image
    meshes = [fox, mirror, frame]
    materials = [
        Material(is_mirror=False),
        Material(is_mirror=True),
        Material(is_mirror=False),
    ]
    tic = time.monotonic()
    depth_image, color_image, pcl = cam.snap(meshes, \
        materials=materials, lights=[point_light])
    toc = time.monotonic()
    print(f'Snapping image took {(toc - tic):.1f}s')

    # Show image
    fig, ax = plt.subplots()
    nanidx = np.where(np.isnan(color_image))
    img = color_image.copy()
    img[nanidx[0], nanidx[1], :] = (0, 1, 1)
    ax.imshow(img)
    ax.set_axis_off()
    plt.show()

    # Visualize images and point cloud
    #geometries_visualize([cam_cs, pcl])
