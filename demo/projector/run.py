import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from trafolib.trafo3d import Trafo3d
from common.image_utils import image_load
from common.geometry_utils import mesh_create_quad, mesh_load, mesh_set_vertex_colors, \
    mesh_create_cs, mesh_create_sphere, geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.light_point import LightPoint
from camsimlib.light_projector import LightProjector



if __name__ == '__main__':
    # Camera
    cam = CameraModel(chip_size=(120, 90),
                      focal_length=(100, 100),
                    )
    #cam.set_distortion((-0.1, 0.1, 0.05, -0.05, 0.2, 0.08))
    cam.scale_resolution(20)
    cam.place((-500, 0, 500))
    cam.look_at((0, 0, 0))
    cam.roll(np.deg2rad(-90))

    # Image file displayed by projector
    projector_image = image_load('../../data/lena.jpg')
    # Duplicate two instances of image next to each other with changed colors
    projector_image = np.hstack((projector_image, projector_image[:,:,2::-1]))

    # Light sources
    point_light = LightPoint((-500, 0, 270))
    projector = LightProjector(image=projector_image,
        focal_length=(2000, 1000))
    projector.place((-600, 0, 100))
    projector.look_at((0, 0, 0))
    projector.roll(np.deg2rad(-90))

    # Object
    # Simple plane
    plane = mesh_create_quad((800, 800), color=(1, 1, 1))
    plane.compute_triangle_normals()
    plane.compute_vertex_normals()
    plane.translate(-plane.get_center())
    T = Trafo3d(t=(500, 0, 0), rpy=np.deg2rad((0, -70, 0)))
    plane.transform(T.get_homogeneous_matrix())
    # More complex object: Fox head
    fox = mesh_load('../../data/fox_head.ply')
    fox.compute_triangle_normals()
    fox.compute_vertex_normals()
    fox.translate(-fox.get_center())
    fox.scale(200, center=(0, 0, 0))
    mesh_set_vertex_colors(fox, (1.0, 1.0, 1.0))

    # Visualize scene
    if False:
        world_cs = mesh_create_cs(size=200.0)
        point_light_sphere = mesh_create_sphere(radius=30)
        point_light_sphere.translate(point_light.get_position())
        mesh_set_vertex_colors(point_light_sphere, (1, 1, 0))
        proj_cs = projector.get_cs(size=50.0)
        proj_frustum = projector.get_frustum(size=100.0)
        cam_cs = cam.get_cs(size=50.0)
        cam_frustum = cam.get_frustum(size=100.0)
        geometries_visualize([world_cs, point_light_sphere, plane, fox, \
            cam_cs, cam_frustum, proj_cs, proj_frustum])

    # Snap image
    tic = time.monotonic()
    depth_image, color_image, pcl = cam.snap([plane, fox], \
        lights=[ point_light, projector ])
    toc = time.monotonic()
    print(f'Snapping image took {(toc - tic):.1f}s')

    # Show raw image
    fig, ax = plt.subplots()
    ax.imshow(projector_image)
    ax.set_axis_off()

    # Show resulting image
    fig, ax = plt.subplots()
    nanidx = np.where(np.isnan(color_image))
    img = color_image.copy()
    img[nanidx[0], nanidx[1], :] = (0, 1, 1)
    ax.imshow(img)
    ax.set_axis_off()
    plt.show()

    geometries_visualize([pcl])
