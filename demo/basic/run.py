import os
import sys
import time
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../../'))
from common.image_utils import image_3float_to_rgb, image_float_to_rgb, image_show
from common.geometry_utils import mesh_create_quad, mesh_create_sphere, \
    mesh_translate, mesh_transform, mesh_set_vertex_colors, \
    geometries_visualize
from trafolib.trafo3d import Trafo3d
from camsimlib.camera_model import CameraModel



if __name__ == '__main__':
    # Create camera and set its model parameters
    cam = CameraModel(chip_size=(40, 30),
                      focal_length=(50, 55),
                      distortion=(-0.1, 0.1, 0.05, -0.05, 0.2, 0.08))
    cam.scale_resolution(8)
    cam.place((0, 0, 500))
    cam.look_at((10, 0, 0))
    cam.roll(np.deg2rad(90))
    cam.move_closer(50)

    # Setup scene: Quad
    quad = mesh_create_quad((200, 200), color=(1, 1, 0))
    quad.translate(-quad.get_center()) # De-mean
    mesh_transform(quad, Trafo3d(rpy=np.deg2rad([-25, 25, 0])))
    # Setup scene: Sphere
    sphere = mesh_create_sphere(radius=50.0)
    mesh_translate(sphere, (0, 0, -15))
    mesh_set_vertex_colors(sphere, (1, 0, 0))

    # Visualize scene
    if False:
        cs = cam.get_cs(size=50.0)
        frustum = cam.get_frustum(size=200.0)
        geometries_visualize([cs, frustum, quad, sphere])

    # Snap image
    tic = time.monotonic()
    depth_image, color_image, pcl = cam.snap((quad, sphere))
    toc = time.monotonic()
    print(f'Snapping image took {(toc - tic):.1f}s')

    # Visualize images and point cloud
    color_image = image_3float_to_rgb(color_image, nan_color=(0, 255, 255))
    image_show(color_image, 'Color image')
    depth_image = image_float_to_rgb(depth_image, cmap_name='viridis',
        min_max=None, nan_color=(0, 255, 255))
    image_show(depth_image, 'Depth image')
    plt.show()

    geometries_visualize([ pcl ])

    # Visualize camera rays; makes only sense with few pixels
    if False:
        rays = cam.get_rays()
        rays.scale(300.0)
        geometries_visualize([quad, sphere, rays.get_mesh()])

