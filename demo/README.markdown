# Demo applications

|Application                                           |Description                                               |
|------------------------------------------------------|----------------------------------------------------------|
|[basic](basic)                                        |Show basic capabilities of camsimlib                      |
|[lighting](lighting)                                  |Demonstrate different types of shaders/lighting           |
|[projector](projector)                                |Using a projector to project an image onto a complex mesh |
|[mirrors](mirrors)                                    |Reflecting a mesh by another mirroring mesh               |
