import numpy as np
import open3d as o3d

from common.geometry_utils import lineset_create
from common.math_utils import length_3d, normalize_3d, dot_3d, cross_3d



class Rays:
    """ Class for keeping a number of 3D rays

    Each ray has an origin and a direction. Both are represented by 3D vectors.
    The direction may be a unit vector or have arbitrary length.
    """

    def __init__(self, origs, dirs):
        """ Constructor

        You can provide n ray origins and n ray directions. Or you can
        provide a single ray origin and n ray directions. Or you can
        provide n ray origins and a single ray direction.

        Internally everything is converted internally into n ray origins
        and n ray directions.

        :param origs: Ray origins
        :param dirs: Ray directions
        """
        self.origs = np.reshape(np.asarray(origs), (-1, 3))
        self.dirs = np.reshape(np.asarray(dirs), (-1, 3))
        # Make sure origs and dirs have same size
        if self.origs.shape[0] == self.dirs.shape[0]:
            pass
        elif (self.origs.shape[0] == 1) and (self.dirs.shape[0] > 1):
            n = self.dirs.shape[0]
            self.origs = np.tile(self.origs, (n, 1))
        elif (self.origs.shape[0] > 1) and (self.dirs.shape[0] == 1):
            n = self.origs.shape[0]
            self.dirs = np.tile(self.dirs, (n, 1))
        else:
            raise ValueError(f'Invalid values for ray origins (shape {self.origs.shape}) and ray directions (shape {self.dirs.shape})')



    def __str__(self):
        """ Returns string representation of Rays
        :return: String representation
        """
        return f'Rays({self.origs.shape[0]} rays)'



    def __len__(self):
        """ Returns number of rays
        :return: Number of rays
        """
        return self.origs.shape[0]



    def copy(self):
        """ Returns copy of rays
        :return: Copy of rays
        """
        return Rays(self.origs.copy(), self.dirs.copy())



    def sample_random(self, rng, num):
        """ Returns a new ray object with fewer random selected rays
        :param rng: Random number generator
        :param num: Number of remaining rays
        :return: Rays object with reduced number of rays
        """
        if num < 0 or num > len(self):
            raise Exception('Invalid number of points')
        indices = rng.choice(len(self), num, replace=False)
        return Rays(self.origs[indices, :], self.dirs[indices, :])



    def filter(self, mask):
        """ Returns a new ray object with a limited amount of rays
        :param mask: Mask or indices determining the remaining rays
        :return: Rays object with reduced number of rays
        """
        return Rays(self.origs[mask, :], self.dirs[mask, :])



    def points(self, factors):
        """ Uses factor to convert rays into points
        points = origin + scale * direction
        :param factors: Factors
        :return: Resulting 3D points
        """
        if factors.ndim == 1:
            # Factors shape (len(self), ); one factor per ray
            assert factors.shape[0] == len(self)
            # Resulting shape is (len(self), 3)
            return self.origs + factors[:, np.newaxis] * self.dirs
        elif factors.ndim == 2:
            # Factors shape (m, len(self)); m factors per ray
            assert factors.shape[1] == len(self)
            # Resulting shape is (m, len(self), 3)
            return self.origs + factors[:, :, np.newaxis] * self.dirs



    def scale(self, factor):
        """ Scale length of direction vectors
        :param factor: Scale factor
        """
        self.dirs *= factor



    def dir_lengths(self):
        """ Calculate ray direction lengths
        :return: Lengths of ray direction vectors
        """
        return length_3d(self.dirs)



    def normalize(self):
        """ Normalize length of direction vectors (length of 1.0)
        Zero length directions will not be touched.
        """
        self.dirs = normalize_3d(self.dirs)



    def to_points_distances(self, points):
        """ Calculate distances from each of the rays to each of the points provided
        :param points: 3D Points, one for each of the n rays, shape (n, 3)
        :return: Distances
        """
        if points.shape[0] != len(self):
            raise ValueError(f'Provide correct number of points {points.shape[0]} != {len(self)}')
        if points.shape[1] != 3:
            raise ValueError('Provider 3D points')
        d = cross_3d(self.dirs, points - self.origs)
        return length_3d(d) / self.dir_lengths()



    def intersect_with_plane(self, plane):
        """ Intersect rays with single plane
        If a ray is perpendicular to plane it does not intersect. The
        intersection mask is returned, shape (n, ) for n rays.
        The scale is the factor by which the ray dir has to be scaled with
        to reach the intersection point on the plane (plus ray orig). The
        scale can be positive which means the intersection point is in the
        direction of the ray. If only those points are wanted, filter with
        scale>0. Shape of scales is (m, ) for m intersecting rays out of
        a total of n rays.
        The intersection points are returned as well, shape (m, 3) for
        m intersecting rays out of a total of n rays.
        Plane equation: All points (x,y,z) are on plane that fulfill the
        equation nx*x + ny*y + nz*z + d = 0 with sqrt(nx**2 + ny**2 + nz**2) == 1
        :param plane: Plane, shape (4, ), see above
        :return: points, mask, scales
        """
        if plane.size != 4:
            raise ValueError('Provide a plane in form (nx, ny, nz, d)')
        n = plane[0:3]
        d = plane[3]
        if not np.isclose(np.linalg.norm(n), 1.0):
            raise ValueError('Plane normal vector has to have length 1')
        a = dot_3d(self.origs, n[np.newaxis, :]) + d
        b = dot_3d(self.dirs, n[np.newaxis, :])
        mask = ~np.isclose(b, 0.0)
        scales = -a[mask] / b[mask]
        points = self.origs[mask] + scales[:, np.newaxis] * self.dirs[mask]
        return points, mask, scales



    def get_mesh(self, color=(0, 0, 0)):
        """ Generate mesh representation of rays as an Open3D LineSet
        :param color:
        """
        point_positions = np.vstack((self.origs, self.origs + self.dirs))
        line_indices = np.arange(2 * len(self)).reshape((2, len(self))).T
        line_colors = np.tile(color, (len(self), 1))
        return lineset_create(point_positions, line_indices, line_colors)



    def to_o3d_tensors(self):
        """ Convert rays into Open3D tensor object for usage in ray tracing
        :return: Tensor object
        """
        return o3d.core.Tensor(np.hstack(( \
            self.origs.astype(np.float32), \
            self.dirs.astype(np.float32))))
