import json

import numpy as np

from common.math_utils import dot_3d



class Material:

    def __init__(self, is_mirror=False, ka=0.1, kd=0.9, ks=0., alpha=50):
        """ Constructor
        :param is_mirror: Mesh is mirror with 100% reflection
        :param ka: Ambient light reflection constant
        :param kd: Diffuse light reflection constant
        :param ks: Specular light reflection constant
        :param alpha: Shininess constant, larger for surfaces that are smoother and more mirror-like
        """
        self._is_mirror = is_mirror
        self._ka = np.clip(ka, 0.0, 1.0)
        self._kd = np.clip(kd, 0.0, 1.0)
        self._ks = np.clip(ks, 0.0, 1.0)
        self._alpha = alpha



    @property
    def is_mirror(self):
        return self._is_mirror

    @is_mirror.setter
    def is_mirror(self, value):
        self._is_mirror = value



    @property
    def ka(self):
        return self._ka

    @ka.setter
    def ka(self, value):
        self._ka = np.clip(value, 0.0, 1.0)



    @property
    def kd(self):
        return self._kd

    @kd.setter
    def kd(self, value):
        self._kd = np.clip(value, 0.0, 1.0)



    @property
    def ks(self):
        return self._ks

    @ks.setter
    def ks(self, value):
        self._ks = np.clip(value, 0.0, 1.0)



    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, value):
        self._alpha = value



    def dict_save(self, param_dict):
        """ Save object to dictionary
        :param param_dict: Dictionary to store data in
        """
        param_dict['is_mirror'] = self._is_mirror
        param_dict['ka'] = self._ka
        param_dict['kd'] = self._kd
        param_dict['ks'] = self._ks
        param_dict['alpha'] = self._alpha



    def dict_load(self, param_dict):
        """ Load object from dictionary
        :param param_dict: Dictionary with data
        """
        self._is_mirror = param_dict['is_mirror']
        self._ka = param_dict['ka']
        self._kd = param_dict['kd']
        self._ks = param_dict['ks']
        self._alpha = param_dict['alpha']



    def json_save(self, filename):
        """ Save object to json file
        :param filename: Filename (and path)
        """
        param_dict = {}
        self.dict_save(param_dict)
        with open(filename, mode='w', encoding='utf-8') as file_handle:
            json.dump(param_dict, file_handle, indent=4, sort_keys=True)



    def json_load(self, filename):
        """ Load object from json file
        :param filename: Filename (and path)
        """
        with open(filename, mode='r', encoding='utf-8') as file_handle:
            param_dict = json.load(file_handle)
        self.dict_load(param_dict)



    def reflection_model_shade(self, obj_colors):
        """ Reflection model for points not directly illuminated by light source
        :param obj_colors: Colors of object, shape (n, 3), RGB with values [0..1]
        :return: Colors of object, shape (n, 3), RGB with values [0..1]
        """
        intensity = self._ka
        return obj_colors * intensity



    def reflection_model(self, normals, lights, views,
            reflections, obj_colors, light_colors):
        """ Reflection model for points illuminated by light source

        Local illumination model to calculate colors of surface points
        on objects with certain material properties.

        Implementaion of the Phong reflectance model
        https://en.wikipedia.org/wiki/Phong_reflection_model

        Both light_colors and obj_colors are combined with so-called
        "multiplicative blending" to get the final color sensed by the camera
        https://en.wikipedia.org/wiki/Blend_modes#Multiply

        :param normals: Normal vectors of object, shape (n, 3), normalized
        :param light: Vectors towards the light source, shape (n, 3), normalized
        :param views: Vectors towards the viewer (camera), shape (n, 3), normalized
        :param reflections: Vectors of optimal reflection of incoming light, shape (n, 3), normalized
        :param obj_colors: Colors of object, shape (n, 3), RGB with values [0..1]
        :param light_colors: Colors of incoming light (n, 3), RGB with values [0..1]
        :return: Colors of object, shape (n, 3), RGB with values [0..1]
        """
        # Ambient lighting
        colors_ambient = light_colors * obj_colors * self.ka
        # Diffuse lighting
        intensity_diffuse = self.kd * dot_3d(normals, lights)
        colors_diffuse = light_colors * obj_colors * intensity_diffuse[:, np.newaxis]
        # Specular highlights; just dependent on light color, not object color
        intensity_specular = np.power(dot_3d(views, reflections), self.alpha)
        colors_specular = light_colors * self.ks * intensity_specular[:, np.newaxis]
        # Combine all three
        return colors_ambient + colors_diffuse + colors_specular
