import copy
import json
import numpy as np

from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_create_cs, mesh_create_image
from camsimlib.image_mapping import image_indices_to_points, \
    image_indices_on_chip_mask



class Screen:
    """ A screen or monitor of real world dimensions displaying a colored image

                   Z    X, shape[1], dimensions[0]
                     X------->
       Y             |
       shape[0]      |   .----------------.
       dimensions[1] |   |                |
                     V   |                |
                         |     Image      | height
                         |                |
                         |                |
                         |                |
                         .----------------.
                               width

    An computer screen could have dimensions=(595, 335) and shape=(1440, 2560).
    """
    def __init__(self, dimensions=(16, 10), image_or_shape=(10, 16), pose=None):
        """ Constructor
        :param dimensions: Real-world dimensions of the screen in millimeters (width, height)
        :param image_or_shape: Color image or shape of an image (height, width)
        """
        # dimensions
        self._dimensions = np.asarray(dimensions)
        if self._dimensions.size != 2:
            raise ValueError('Provide proper dimensions')
        # image
        ios = np.asarray(image_or_shape)
        if (ios.ndim == 1) and (ios.size == 2):
            self._image = np.zeros((ios[0], ios[1], 3), dtype=np.uint8)
        elif ios.ndim == 3:
            self._image = ios
        else:
            raise ValueError('Provide proper image or image shape')
        # pose
        if pose is None:
            self._pose = Trafo3d()
        else:
            self._pose = pose



    def __str__(self):
        """ String representation of self
        :return: String representing of self
        """
        return ('Screen(' +
            f'dim={self._dimensions}, ' +
            f'image.shape={self._image.shape}, ' +
            f'pose={self._pose}' +
            ')'
            )



    def __copy__(self):
        """ Shallow copy
        :return: A shallow copy of self
        """
        return self.__class__(dimensions=self._dimensions,
                              image_or_shape=self._image,
                              pose=self._pose)



    def __deepcopy__(self, memo):
        """ Deep copy
        :param memo: Memo dictionary
        :return: A deep copy of self
        """
        result = self.__class__(dimensions=copy.deepcopy(self._dimensions, memo),
                                image_or_shape=copy.deepcopy(self._image, memo),
                                pose=copy.deepcopy(self._pose, memo))
        memo[id(self)] = result
        return result



    def get_dimensions(self):
        """ Get real-world dimensions of the screen in millimeters
        :return: Image
        """
        return self._dimensions



    def get_shape(self):
        return self._image.shape



    def set_image(self, image):
        """ Set image displayed by screen
        Image must have same shape as originally configured with the constructor
        :param image: Image
        """
        if not np.all(image.shape == self._image.shape):
            raise ValueError('Provide image of same dimensions')
        self._image = image



    def get_image(self):
        """ Get image displayed by screen
        :return: Image
        """
        return self._image



    def set_pose(self, pose):
        """ Set pose
        Transformation from world coordinate system to screen as Trafo3d object
        :param pose: 6d pose
        """
        self._pose = pose



    def get_pose(self):
        """ Get pose
        Transformation from world coordinate system to screen as Trafo3d object
        :return: 6d pose
        """
        return self._pose



    def get_cs(self, size=1.0):
        """ Returns a representation of the coordinate system of the screen
        Returns Open3d TriangleMesh object representing the coordinate system
        of the screen that can be used for visualization
        :param size: Length of the coordinate axes of the coordinate system
        :return: Coordinate system as Open3d mesh object
        """
        return mesh_create_cs(self._pose, size)



    def get_mesh(self):
        """ Returns a visual representation of the screen
        Returns Open3d TriangleMesh object representing the screen
        that can be used for visualization
        :return: Visualization as Open3d mesh object
        """
        pixel_sizes = (self._dimensions[0] / self._image.shape[1],
            self._dimensions[1] / self._image.shape[0])
        mesh = mesh_create_image(self._image, pixel_sizes)
        mesh.transform(self._pose.get_homogeneous_matrix())
        return mesh



    def dict_save(self, param_dict):
        """ Save screen parameters to dictionary
        :param param_dict: Dictionary to store parameters in
        """
        param_dict['dimensions'] = self._dimensions.tolist()
        param_dict['image_shape'] = (self._image.shape[0], self._image.shape[1])
        param_dict['pose'] = {}
        self._pose.dict_save(param_dict['pose'])



    def json_save(self, filename):
        """ Save screen parameters to json file
        :param filename: Filename of json file
        """
        param_dict = {}
        self.dict_save(param_dict)
        with open(filename, mode='w', encoding='utf-8') as file_handle:
            json.dump(param_dict, file_handle, indent=4, sort_keys=True)



    def json_load(self, filename):
        """ Load screen parameters from json file
        :param filename: Filename of json file
        """
        with open(filename, mode='r', encoding='utf-8') as file_handle:
            param_dict = json.load(file_handle)
        self.dict_load(param_dict)



    def dict_load(self, param_dict):
        """ Load screen parameters from dictionary
        :param param_dict: Dictionary with screen parameters
        """
        self._dimensions = np.array(param_dict['dimensions'])
        image_shape = np.array(param_dict['image_shape'])
        self._image = np.zeros((image_shape[0], image_shape[1], 3), dtype=np.uint8)
        self._pose.dict_load(param_dict['pose'])



    def image_indices_to_scene(self, indices):
        """ Converts 2D screen indices to 3D scene points (world coordinate system)
        :param p: 2D screen points, shape (n, 2), type float (subpixels allowed)
        :param check_for_valid: True if checks for valid screen coordinates desired
        :return: 3D scene points, shape (n, 3)
        """
        assert indices.ndim == 2
        assert indices.shape[1] == 2
        # Convert indices into points
        p = image_indices_to_points(indices)
        # Scale points to screen dimensions
        P = np.zeros((p.shape[0], 3))
        P[:, 0] = (self._dimensions[0] * p[:, 0]) / self._image.shape[1]
        P[:, 1] = (self._dimensions[1] * p[:, 1]) / self._image.shape[0]
        # Transform points from screen coordinate system
        # to world coordinate system
        P = self._pose * P
        return P



    def indices_on_chip_mask(self, indices):
        chip_size = [ self._image.shape[1], self._image.shape[0] ]
        return image_indices_on_chip_mask(indices, chip_size)
