import numpy as np

from common.math_utils import dot_3d, cross_3d
from camsimlib.ray_tracer import RayTracer
from camsimlib.ray_tracer_result import get_interpolated_normals_and_colors



class RayTracerPython(RayTracer):

    def __init__(self, rays, meshes, materials=None):
        super().__init__(rays, meshes, materials)



    @staticmethod
    def ray_mesh_intersect(ray_orig, ray_dir, triangle_vertices):
        """ Intersection of a single ray with the triangles of the mesh
        Tests intersection of ray with all triangles and returns the one with lowest Z coordinate
        Based on Möller–Trumbore intersection algorithm (see https://scratchapixel.com)
        """
        # Switch off warnings about divide by zero and invalid float op:
        # We do some batch-computations and check the validity of the results later
        with np.errstate(divide='ignore', invalid='ignore'):
            num_tri = triangle_vertices.shape[0]
            rays = np.tile(ray_dir, num_tri).reshape((num_tri, 3))
            # Do all calculation no matter if invalid values occur during calculation
            v0 = triangle_vertices[:, 0, :]
            v0v1 = triangle_vertices[:, 1, :] - v0
            v0v2 = triangle_vertices[:, 2, :] - v0
            pvec = cross_3d(rays, v0v2)
            det = dot_3d(v0v1, pvec)
            inv_det = 1.0 / det
            tvec = ray_orig - v0
            u = inv_det * dot_3d(tvec, pvec)
            qvec = cross_3d(tvec, v0v1)
            v = inv_det * dot_3d(rays, qvec)
            t = inv_det * dot_3d(v0v2, qvec)
            # Check all results for validity
            invalid = np.logical_or.reduce((
                np.isclose(det, 0.0),
                u < 0.0,
                u > 1.0,
                v < 0.0,
                (u + v) > 1.0,
                np.isclose(t, 0.0),
                t < 0.0,
            ))
            valid_idx = np.where(~invalid)[0]
            if valid_idx.size == 0:
                # No intersection of ray with any triangle in mesh
                return None, None, None, None
            # triangle_index is the index of the triangle intersection point with
            # the lowest t, which means it is the intersection point closest to the camera
            triangle_index = valid_idx[t[valid_idx].argmin()]
            scale = t[triangle_index]
            point_cartesic = ray_orig + scale * ray_dir
            point_barycentric = np.array((
                1.0 - u[triangle_index] - v[triangle_index],
                u[triangle_index],
                v[triangle_index]
            ))
            return triangle_index, scale, point_cartesic, point_barycentric



    def run(self):
        """ Run ray tracing
        """
        # Join all triangle vertices of all meshes and
        # keep mesh indices for each triangle
        triangle_vertices = []
        mesh_indices = []
        for i, mesh in enumerate(self._meshes):
            vertices = mesh.vertex.positions.numpy()
            triangle_indices = mesh.triangle.indices.numpy()
            num_triangles = triangle_indices.shape[0]
            triangle_vertices.append(vertices[triangle_indices])
            mesh_indices.extend(num_triangles * [ i ])
        triangle_vertices = np.vstack(triangle_vertices)
        mesh_indices = np.array(mesh_indices)

        self.r.clear(len(self._rays))
        for i in range(len(self._rays)):
            triangle_index, scale, point_cartesic, point_barycentric = \
                RayTracerPython.ray_mesh_intersect(self._rays.origs[i], self._rays.dirs[i],
                                   triangle_vertices)
            if triangle_index is None:
                self.r.mask[i] = False
                continue
            self.r.mask[i] = True
            self.r.scales[i] = scale
            self.r.points[i] = point_cartesic
            mesh_index = mesh_indices[triangle_index]
            triangle_index_in_mesh = triangle_index - \
                np.where(mesh_indices == mesh_index)[0][0]
            self.r.normals[i], self.r.colors[i] = \
                get_interpolated_normals_and_colors(
                    self._meshes,
                    np.array([mesh_index]),
                    np.array([triangle_index_in_mesh]),
                    np.array([point_barycentric]),
                    )
            self.r.mesh_indices[i] = mesh_indices[triangle_index]
        self.r.initial_points = self.r.points.copy()

        # Reduce result to real intersections
        self.r.reduce_to_valid()
