from abc import ABC, abstractmethod

from camsimlib.material import Material
from camsimlib.ray_tracer_result import RayTracerResult



class RayTracer(ABC):

    def __init__(self, rays, meshes, materials=None):
        self._rays = rays
        self._meshes = meshes
        if materials is None:
            # If user did not specify materials, we use default material
            self._materials = []
            for _ in range(len(meshes)):
                self._materials.append(Material())
        else:
            # If user did specity material, we need one per mesh
            if len(meshes) != len(materials):
                raise Exception('Provide material for each mesh')
            self._materials = materials
        self.r = RayTracerResult()



    @abstractmethod
    def run(self):
        raise NotImplementedError
