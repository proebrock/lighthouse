import numpy as np

from common.math_utils import normalize_3d
from camsimlib.image_mapping import image_sample_points_bilinear
from camsimlib.projective_geometry import ProjectiveGeometry
from camsimlib.light import Light, point_visible_from_point



class LightProjector(Light, ProjectiveGeometry):

    def __init__(self, image=np.zeros((30, 40, 3), dtype=np.uint8),
                focal_length=100, principal_point=None,
                distortion=None, pose=None):
        self.set_image(image)
        Light.__init__(self)
        ProjectiveGeometry.__init__(self, focal_length,
            principal_point, distortion, pose)



    def __str__(self):
        """ String representation of self
        :return: String representing of self
        """
        return (super().__str__() +
                ', LightProjector(' +
                f'chip_size={self.get_chip_size()}, ' +
                ')'
                )



    def get_chip_size(self):
        """ Get chip size
        See set_chipsize().
        :return: Chip size
        """
        chip_size = np.array((self._image.shape[1],
            self._image.shape[0]), dtype=int)
        return chip_size



    def set_chip_size(self, chip_size):
        """ Set chip size
        The size of the chip in pixels, width x height
        Unit for chip size is pixels for both width and height.
        :param chip_size: Chip size
        """
        image = np.zeros((chip_size[1], chip_size[0], 3), dtype=np.uint8)
        self.set_image(image)



    def get_image(self):
        """ Get image displayed by projector
        :return: Image
        """
        return np.round(255.0 * self._image).astype(np.uint8)



    def set_image(self, image):
        """ Set image displayed by projector
        :param image: Image
        """
        assert image.ndim == 3 # RBG image
        assert image.shape[2] == 3
        assert image.dtype == np.uint8
        self._image = image.astype(float) / 255.0



    def dict_save(self, param_dict):
        """ Save object parameters to dictionary
        :param param_dict: Dictionary to store projective geometry parameters in
        """
        super().dict_save(param_dict)
        param_dict['image_shape'] = self._image.shape[0:2]



    def dict_load(self, param_dict):
        """ Load object parameters from dictionary
        :param param_dict: Dictionary with projective geometry parameters
        """
        super().dict_load(param_dict)
        shape = param_dict['image_shape']
        self._image = np.zeros((*shape, 3))



    def get_light_vectors(self, points):
        return normalize_3d(-points + self.get_pose().get_translation())



    def get_illumination_mask_and_colors(self, points, normals, meshes, materials):
        # First condition for illuminated points: there is a line of sight
        # between the projector location and the object points
        illu_mask = point_visible_from_point(
            self.get_pose().get_translation(),
            points, normals, meshes, materials)
        # Second condition is that the object points - if projected
        # with the model of the projector - are located on the chip of the projector;
        # if points are on the chip, sample colors from projector chip
        p = self.scene_to_chip(points[illu_mask])[:, 0:2] # Omit distance
        light_colors, on_chip_mask = image_sample_points_bilinear(self._image, p)
        # Update illumination mask of actually illuminated points
        illu_mask[illu_mask] = on_chip_mask
        return illu_mask, light_colors
