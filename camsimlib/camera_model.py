""" Simulation of depth and/or RGB cameras
"""

import copy
import numpy as np

from common.geometry_utils import pcl_create
from camsimlib.projective_geometry import ProjectiveGeometry
from camsimlib.ray_tracer_mirrors import RayTracerMirrors as RayTracer
from camsimlib.material import Material
from camsimlib.shader import Shader
from camsimlib.light_point import LightPoint



class CameraModel(ProjectiveGeometry):
    """ Class for simulating a depth and/or RGB camera

    Camera coordinate system with Z-Axis pointing into direction of view

    Z               X - Axis
                    self.get_chip_size()[0] = width
      X--------->   depth_image second dimension
      |
      |
      |
      |
      V

    Y - Axis
    self.get_chip_size()[1] = height
    depth_image first dimension

    """
    def __init__(self, chip_size=(40, 30), focal_length=100, principal_point=None,
                 distortion=None, pose=None):
        self._chip_size = None
        self.set_chip_size(chip_size)
        super().__init__(focal_length, principal_point,
            distortion, pose)



    def __str__(self):
        """ String representation of self
        :return: String representing of self
        """
        return (super().__str__() +
                ', CameraModel(' +
                f'chip_size={self._chip_size}, ' +
                ')'
                )



    def __copy__(self):
        """ Shallow copy
        :return: A shallow copy of self
        """
        return self.__class__(chip_size=self._chip_size,
                              focal_length=self._focal_length,
                              principal_point=self._principal_point,
                              distortion=self._distortion,
                              pose=self._pose)



    def __deepcopy__(self, memo):
        """ Deep copy
        :param memo: Memo dictionary
        :return: A deep copy of self
        """
        result = self.__class__(chip_size=copy.deepcopy(self._chip_size),
                                focal_length=copy.deepcopy(self._focal_length, memo),
                                principal_point=copy.deepcopy(self._principal_point, memo),
                                distortion=copy.deepcopy(self._distortion.get_coefficients(), memo),
                                pose=copy.deepcopy(self._pose, memo))
        memo[id(self)] = result
        return result



    def set_chip_size(self, chip_size):
        """ Set chip size
        The size of the chip in pixels, width x height
        Unit for chip size is pixels for both width and height.
        :param chip_size: Chip size
        """
        csize = np.asarray(chip_size, dtype=int)
        if csize.size != 2:
            raise ValueError('Provide 2d chip size in pixels')
        if np.any(csize < 1):
            raise ValueError('Provide positive chip size')
        self._chip_size = csize



    def get_chip_size(self):
        """ Get chip size
        See set_chipsize().
        :return: Chip size
        """
        return self._chip_size



    def dict_save(self, param_dict):
        """ Save camera model parameters to dictionary
        :param params: Dictionary to store projective geometry parameters in
        """
        super().dict_save(param_dict)
        param_dict['chip_size'] = self._chip_size.tolist()



    def dict_load(self, param_dict):
        """ Load camera model parameters from dictionary
        :param params: Dictionary with projective geometry parameters
        """
        super().dict_load(param_dict)
        self._chip_size = np.array(param_dict['chip_size'])



    def snap(self, meshes, materials=None, lights=None):
        """ Takes image of mesh using camera
        :return:
            - depth_image - Depth image of scene, pixels seeing no object are set to NaN
            - color_image - Color image (RGB) of scene, pixels seeing no object are set to NaN
            - P - Scene points (only valid points)
        """
        # If no materials defined, create default material for each mesh
        if materials is None:
            materials = []
            for _ in range(len(meshes)):
                materials.append(Material())
        # If no lights defined, place point light directly at the camera position
        if lights is None:
            lights = [ LightPoint(self._pose.get_translation()) ]
        # Run raytracer: we do "eye-based path tracing" starting from
        # a ray from each camera pixel until we hit an object; first raytracer
        # is one that traces rays hitting mirroring objects back to a solid object
        rays = self.get_rays()
        rt = RayTracer(rays, meshes, materials)
        rt.run()
        # Run shader
        shader = Shader(meshes, rt.r, materials, lights, self._pose.get_translation())
        shader.run()
        # Calculate color, depth images and point cloud
        depth_image, color_image = self.scene_points_to_depth_image( \
            rt.r.initial_points, shader.colors)
        # Point cloud
        pcl = pcl_create(point_positions=rt.r.initial_points,
            point_normals=rt.r.normals,
            point_colors=shader.colors)
        return depth_image, color_image, pcl
