import numpy as np

from camsimlib.light import Light, sky_visible_from_points



class LightParallel(Light):

    def __init__(self, direction, color=(1., 1., 1.)):
        super().__init__()
        self._dir = np.asarray(direction)
        self._color = np.asarray(color)



    def __str__(self):
        return f'LightParallel(dir={self._dir})'



    def get_direction(self):
        return self._dir



    def get_color(self):
        return self._color



    def get_light_vectors(self, points):
        n = points.shape[0]
        return np.tile(-self._dir / np.linalg.norm(self._dir), (n, 1))



    def get_illumination_mask_and_colors(self, points, normals, meshes, materials):
        illu_mask = sky_visible_from_points(
            -self._dir, points, normals, meshes, materials)
        light_colors = self._color
        return illu_mask, light_colors
