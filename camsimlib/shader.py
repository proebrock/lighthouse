import numpy as np

from common.math_utils import normalize_3d, reflect_3d



class Shader():

    def __init__(self, meshes, ray_tracer_result, materials, lights, view_point):
        self._meshes = meshes
        self._rtr = ray_tracer_result
        self._materials = materials
        self._lights = lights
        self._view_point = view_point
        self.colors = np.zeros((0, 3))



    def run(self):
        num_hits = self._rtr.scales.size
        self.colors = np.zeros((num_hits, 3))
        for mesh_index in range(len(self._meshes)):
            mesh_mask = self._rtr.mesh_indices == mesh_index
            mesh_points = self._rtr.points[mesh_mask]
            mesh_normals = self._rtr.normals[mesh_mask]
            for light in self._lights:
                # Get mask of points illuminated by current light source
                illu_mask, light_colors = light.get_illumination_mask_and_colors(
                    mesh_points, mesh_normals, self._meshes, self._materials)
                # Points in current mesh and illuminated
                mesh_mask_illu = mesh_mask.copy()
                mesh_mask_illu[mesh_mask_illu] = illu_mask
                # Extract properties for illuminated points and calculate reflectance model
                points = self._rtr.points[mesh_mask_illu]
                normals = self._rtr.normals[mesh_mask_illu]
                obj_colors = self._rtr.colors[mesh_mask_illu]
                light_vecs = light.get_light_vectors(points)
                view_vecs = normalize_3d(-points + self._view_point)
                reflect_vecs = -reflect_3d(light_vecs, normals)
                self.colors[mesh_mask_illu, :] += self._materials[mesh_index].reflection_model( \
                    normals, light_vecs, view_vecs, reflect_vecs,
                    obj_colors, light_colors
                )
                # Points in current mesh and not illuminated
                mesh_mask_shade = mesh_mask.copy()
                mesh_mask_shade[mesh_mask_shade] = ~illu_mask
                obj_colors = self._rtr.colors[mesh_mask_shade]
                self.colors[mesh_mask_shade, :] += self._materials[mesh_index].reflection_model_shade( \
                    obj_colors)
            self.colors /= len(self._lights)
            self.colors = np.clip(self.colors, 0.0, 1.0)




