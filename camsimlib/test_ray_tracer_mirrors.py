import numpy as np

from common.geometry_utils import mesh_create_triangle
from camsimlib.rays import Rays
from camsimlib.material import Material
from camsimlib.ray_tracer_mirrors import RayTracerMirrors



def test_basic_setup():
    a = 100.0
    # Generate rays
    rayorigs = np.array([
        [a, a/2, 1.0],
        [a, a/2, 1.0],
        [a, a/2, 1.0],
        [a, a/2, 1.0],
        [a/2, 1, 1.0],
        [a, a/2, 1.0],
        ])
    raydirs = np.array([
        [  1,   0, 0.0 ], # miss
        [ -1,  -1, 0.0 ], # hit
        [ -1,   0, 0.0 ], # mirror, miss
        [ -0.1, 1, 0.0 ], # mirror, hit
        [ -1,   1, 0.0 ], # mirror, mirror, miss
        [ -1,   1, 0.0 ], # mirror, mirror, hit
        ])
    rays = Rays(rayorigs, raydirs)
    rays.normalize()
    # Generate mesh list
    # Along x axis facing north
    t0 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [a/2, 0, a/2], [a, 0, 0]],
        vertex_normals=[[0, 1, 0], [0, 1, 0], [0, 1, 0]])
    m0 = Material(is_mirror=False)
    # Along y axis facing east
    t1 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [0, a, 0], [0, a/2, a/2]],
        vertex_normals=[[1, 0, 0], [1, 0, 0], [1, 0, 0]])
    m1 = Material(is_mirror=True)
    # Parallel to x axis facing south
    t2 = mesh_create_triangle(
        vertex_positions=[[0, a, 0], [a, a, 0], [a/2, a, a/2]],
        vertex_normals=[[0, -1, 0], [0, -1, 0], [0, -1, 0]])
    m2 = Material(is_mirror=True)
    # Run raytracing
    meshes = [ t0, t1, t2 ]
    materials = [ m0, m1, m2 ]
    rt = RayTracerMirrors(rays, meshes, materials)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ False, True, False, True, False, True ])
    assert np.allclose(rt.r.scales, np.array([
        a/np.sqrt(2), 150.74813461, 3*a/np.sqrt(2)
        ]))
    assert np.allclose(rt.r.initial_points, np.array([
        [ a/2, 0, 1.0 ],
        [ 95,  a, 1.0 ],
        [ a/2, a, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.points, np.array([
        [ a/2, 0, 1.0 ],
        [ 85,  0, 1.0 ],
        [ a/2, 0, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.normals, np.array([
        [ 0.0, 1.0, 0.0 ],
        [ 0.0, 1.0, 0.0 ],
        [ 0.0, 1.0, 0.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.colors, np.array([
        [ 1.0, 1.0, 1.0 ],
        [ 1.0, 1.0, 1.0 ],
        [ 1.0, 1.0, 1.0 ],
        ]), atol=1e-5)
    assert rt.r.mesh_indices.tolist() == [ 0, 0, 0 ]



def test_mirror_front_and_backside():
    a = 100.0
    # Generate rays
    rayorigs = np.array([
        [a/2, a/4, 1.0],
        [a/2, -a/4, 1.0],
        ])
    raydirs = np.array([
        [  0, -1, 0.0 ], # hitting mirror from above, angle normal<->ray is 180 deg
        [  0,  1, 0.0 ], # hitting mirror from below, angle normal<->ray is 0 deg
        ])
    rays = Rays(rayorigs, raydirs)
    rays.normalize()
    # Generate mesh list
    # Three parallel triangles, 1st: Below x axis facing north
    t0 = mesh_create_triangle(
        vertex_positions=[[0, -a/2, 0], [a/2, -a/2, a/2], [a, -a/2, 0]],
        vertex_normals=[[0, 1, 0], [0, 1, 0], [0, 1, 0]])
    m0 = Material(is_mirror=False)
    # Three parallel triangles, 2nd: Along x axis facing north
    t1 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [a/2, 0, a/2], [a, 0, 0]],
        vertex_normals=[[0, 1, 0], [0, 1, 0], [0, 1, 0]])
    m1 = Material(is_mirror=True)
    # Three parallel triangles, 3rd: Above x axis facing south
    t2 = mesh_create_triangle(
        vertex_positions=[[0, a/2, 0], [a, a/2, 0], [a/2, a/2, a/2]],
        vertex_normals=[[0, -1, 0], [0, -1, 0], [0, -1, 0]])
    m2 = Material(is_mirror=False)
    # Run raytracing
    meshes = [ t0, t1, t2 ]
    materials = [ m0, m1, m2 ]
    rt = RayTracerMirrors(rays, meshes, materials)
    rt.run()
    # Check results: Default behavior: mirror meshes reflect on both sides
    assert np.all(rt.r.mask == [ True, True ])
    assert np.allclose(rt.r.scales, np.array([ 3*a/4, 3*a/4 ]))
    assert np.allclose(rt.r.initial_points, np.array([
        [ a/2,  0, 1.0 ],
        [ a/2,  0, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.points, np.array([
        [ a/2,  a/2, 1.0 ],
        [ a/2, -a/2, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.normals, np.array([
        [ 0.0, -1.0, 0.0 ],
        [ 0.0, 1.0, 0.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.colors, np.array([
        [ 1.0, 1.0, 1.0 ],
        [ 1.0, 1.0, 1.0 ],
        ]), atol=1e-5)
    assert rt.r.mesh_indices.tolist() == [ 2, 0 ]



def test_infinite_ray():
    a = 100.0
    # Generate rays
    rayorigs = np.array([
        [ a/2, a/2, 1.0],
        [ a/2, a/2, 1.0],
        [ a/2, a/2, 1.0],
        ])
    raydirs = np.array([
        [ -1, 0, 0.0 ], # hit
        [  1, 0, 0.0 ], # is mirrored infinitely
        [  0, 1, 0.0 ], # miss
        ])
    rays = Rays(rayorigs, raydirs)
    rays.normalize()
    # Generate mesh list
    # Along x axis facing north
    t0 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [a/2, 0, a/2], [a, 0, 0]],
        vertex_normals=[[0, 1, 0], [0, 1, 0], [0, 1, 0]])
    m0 = Material(is_mirror=True)
    # Along y axis facing east
    t1 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [0, a, 0], [0, a/2, a/2]],
        vertex_normals=[[1, 0, 0], [1, 0, 0], [1, 0, 0]])
    m1 = Material(is_mirror=False)
    # Parallel to x axis facing south
    t2 = mesh_create_triangle(
        vertex_positions=[[0, a, 0], [a, a, 0], [a/2, a, a/2]],
        vertex_normals=[[0, -1, 0], [0, -1, 0], [0, -1, 0]])
    m2 = Material(is_mirror=True)
    # Run raytracing
    meshes = [ t0, t1, t2 ]
    materials = [ m0, m1, m2 ]
    rt = RayTracerMirrors(rays, meshes, materials)
    rt.run()
    # Check results: Infinitely reflected ray should be discarded
    assert np.all(rt.r.mask == [ True, False, False ])
    assert np.allclose(rt.r.scales, np.array([ a/2 ]))
    assert np.allclose(rt.r.initial_points, np.array([
        [ 0,  a/2, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.points, np.array([
        [ 0,  a/2, 1.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.normals, np.array([
        [ 1.0, 0.0, 0.0 ],
        ]), atol=1e-5)
    assert np.allclose(rt.r.colors, np.array([
        [ 1.0, 1.0, 1.0 ],
        ]), atol=1e-5)
    assert rt.r.mesh_indices.tolist() == [ 1, ]
