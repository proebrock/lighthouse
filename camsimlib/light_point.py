import numpy as np

from common.math_utils import normalize_3d
from camsimlib.light import Light, point_visible_from_point



class LightPoint(Light):

    def __init__(self, position, color=(1., 1., 1.)):
        super().__init__()
        self._position = np.asarray(position)
        self._color = np.asarray(color)



    def __str__(self):
        return f'LightPoint(pos={self._position})'



    def get_position(self):
        return self._position



    def get_color(self):
        return self._color



    def get_light_vectors(self, points):
        return normalize_3d(-points + self._position)



    def get_illumination_mask_and_colors(self, points, normals, meshes, materials):
        illu_mask = point_visible_from_point(
            self._position, points, normals, meshes, materials)
        light_colors = self._color
        return illu_mask, light_colors
