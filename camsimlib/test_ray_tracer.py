import pytest
import numpy as np

from common.geometry_utils import mesh_create_empty, mesh_create_triangle, \
    mesh_create_sphere, mesh_translate, mesh_set_vertex_colors
from camsimlib.rays import Rays

from camsimlib.ray_tracer_embree import RayTracerEmbree
from camsimlib.ray_tracer_mirrors import RayTracerMirrors
from camsimlib.ray_tracer_python import RayTracerPython



@pytest.fixture(params=[RayTracerEmbree, RayTracerMirrors, RayTracerPython])
def RayTracerImplementation(request):
    return request.param



def test_no_rays(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
    ))
    ray_dirs = np.array((
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    triangle = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    meshes = [ triangle ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert rt.r.mask.size == 0
    assert rt.r.scales.size == 0
    assert rt.r.points.size == 0
    assert rt.r.normals.size == 0
    assert rt.r.colors.size == 0
    assert rt.r.mesh_indices.size == 0



def test_zero_length_rays(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.75, 0.75, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, 0.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    triangle = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    meshes = [ triangle ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ False ])
    assert rt.r.scales.size == 0
    assert rt.r.points.size == 0
    assert rt.r.normals.size == 0
    assert rt.r.colors.size == 0
    assert rt.r.mesh_indices.size == 0



def test_empty_mesh(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.05, 0.05, 2.0),
        (0.75, 0.75, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    empty = mesh_create_empty()
    meshes = [ empty ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ False ])
    assert rt.r.scales.size == 0
    assert rt.r.points.size == 0
    assert rt.r.normals.size == 0
    assert rt.r.colors.size == 0
    assert rt.r.mesh_indices.size == 0



def test_single_triangle_ray_miss(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.75, 0.75, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    triangle = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    meshes = [ triangle ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ False ])
    assert rt.r.scales.size == 0
    assert rt.r.points.size == 0
    assert rt.r.normals.size == 0
    assert rt.r.colors.size == 0
    assert rt.r.mesh_indices.size == 0



def test_single_triangle_two_rays_one_hit_one_miss(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.05, 0.05, 2.0),
        (0.75, 0.75, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    triangle = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    meshes = [ triangle ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ True, False ])
    assert np.all(np.abs(rt.r.scales - [2.0]) < 1e-6)
    assert np.all(np.abs(rt.r.points - [[0.05, 0.05, 0.0]]) < 1e-6)
    assert np.all(np.abs(rt.r.normals - [[-0.55602186, -0.55602186, 0.61780206]]) < 1e-6)
    assert np.all(np.abs(rt.r.colors - [[0.9, 0.05, 0.05 ]]) < 1e-6)
    assert rt.r.mesh_indices.tolist() == [ 0 ]



def test_single_triangle_shortest_intersection(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.05, 0.05, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    t0 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    t1 = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    mesh_translate(t1, (0.0, 0.0, 0.5))
    meshes = [ t0, t1 ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    # Check results
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.scales - [1.5]) < 1e-6)
    assert np.all(np.abs(rt.r.points - [[0.05, 0.05, 0.5]]) < 1e-6)
    assert np.all(np.abs(rt.r.normals - [[-0.55602186, -0.55602186, 0.61780206]]) < 1e-6)
    assert np.all(np.abs(rt.r.colors - [[0.9, 0.05, 0.05 ]]) < 1e-6)
    assert rt.r.mesh_indices.tolist() == [ 1 ]



def test_single_triangle_single_ray_different_scale(RayTracerImplementation):
    # Rays
    ray_origs = np.array((
        (0.25, 0.25, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)
    # Meshes
    triangle = mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    )
    meshes = [ triangle ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.isclose(rt.r.scales[0], 2.0)
    # Make ray twice as long, scale should be half
    rays.scale(2.0)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.isclose(rt.r.scales[0], 1.0)
    # Make scale a tenth, scale should be 10 times higher
    rays.scale(0.1)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.isclose(rt.r.scales[0], 10.0)



def test_different_normal_sources(RayTracerImplementation):
    """ Check how ray tracers determine normal vectors
    """
    # Rays
    ray_origs = np.array((
        (0.05, 0.05, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)

    # Meshes: with vertex normals
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.normals - [[-0.55602186, -0.55602186, 0.61780206]]) < 1e-6)

    # Meshes: with triangle normals
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        triangle_normals=[[0, 0, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.normals - [[0, 0, 1]]) < 1e-6)

    # Meshes: no normals
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    with pytest.raises(Exception):
        rt.run()



def test_different_color_sources(RayTracerImplementation):
    """ Check how ray tracers determine normal vectors
    """
    # Rays
    ray_origs = np.array((
        (0.05, 0.05, 2.0),
    ))
    ray_dirs = np.array((
        (0.0, 0.0, -1.0),
    ))
    rays = Rays(ray_origs, ray_dirs)

    # Meshes: with vertex colors
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        vertex_colors=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.colors - [[0.9, 0.05, 0.05]]) < 1e-6)

    # Meshes: with triangle colors
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
        triangle_colors=[[0.2, 0.6, 0.5]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.colors - [[0.2, 0.6, 0.5]]) < 1e-6)

    # Meshes: with no colors
    meshes = [ mesh_create_triangle(
        vertex_positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]],
        vertex_normals=[[-1, -1, 1], [1, -1, 1], [-1, 1, 1]],
    ) ]
    # Raytracing
    rt = RayTracerImplementation(rays, meshes)
    rt.run()
    assert np.all(rt.r.mask == [ True ])
    assert np.all(np.abs(rt.r.colors - [[1, 1, 1]]) < 1e-6)



def test_two_implementations():
    """ Compare implementations
    Run the two basic implementations, RayTracerEmbree and RayTracerPython
    against each other and compare results
    """
    # Rays
    rayorigs = (0, 0, 0)
    num_lat=21
    num_lon=41
    lat_angle_deg=30
    lat_angle = np.deg2rad(lat_angle_deg)
    lat_angle = np.clip(lat_angle, 0, np.pi)
    lat = np.linspace(0, lat_angle, num_lat)
    lon = np.linspace(0, 2 * np.pi, num_lon)
    lat, lon = np.meshgrid(lat, lon, indexing='ij')
    raydirs = np.zeros((num_lat, num_lon, 3))
    raydirs[:, :, 0] = np.sin(lat) * np.cos(lon)
    raydirs[:, :, 1] = np.sin(lat) * np.sin(lon)
    raydirs[:, :, 2] = np.cos(lat)
    raydirs = raydirs.reshape((-1, 3))
    rays = Rays(rayorigs, raydirs)
    # Meshes
    sphere_big = mesh_create_sphere(1.0, 20)
    sphere_big.translate((0.5, -0.5, 5))
    mesh_set_vertex_colors(sphere_big, (1, 0, 0))
    sphere_small = mesh_create_sphere(0.2, 20)
    sphere_small.translate((-0.1, 0.1, 3))
    mesh_set_vertex_colors(sphere_small, (0, 1, 0))
    meshes = [ sphere_big, sphere_small ]
    # Raytracing
    rt0 = RayTracerPython(rays, meshes)
    rt0.run()
    rt1 = RayTracerEmbree(rays, meshes)
    rt1.run()
    # Check results
    assert np.sum(rt0.r.mesh_indices == 0) > 50 # Setup should make sense: at least 50 hits on mesh 0
    assert np.sum(rt0.r.mesh_indices == 1) > 50 # Setup should make sense: at least 50 hits on mesh 1
    assert np.all(rt0.r.mask == rt1.r.mask)
    assert np.allclose(rt0.r.scales, rt1.r.scales)
    assert np.allclose(rt0.r.points, rt1.r.points, atol=1e-3)
    assert np.allclose(rt0.r.normals, rt1.r.normals, atol=1e-3)
    assert np.allclose(rt0.r.colors, rt1.r.colors, atol=1e-3)
    assert np.all(rt0.r.mesh_indices == rt1.r.mesh_indices)
