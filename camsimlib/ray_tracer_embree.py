import numpy as np
import open3d as o3d



from camsimlib.ray_tracer import RayTracer
from camsimlib.ray_tracer_result import get_interpolated_normals_and_colors



class RayTracerEmbree(RayTracer):

    def __init__(self, rays, meshes, materials=None):
        super().__init__(rays, meshes, materials)



    def run(self):
        """ Run ray tracing
        """
        # Reset result
        self.r.clear()

        scene = o3d.t.geometry.RaycastingScene()
        geometry_ids = []
        for mesh in self._meshes:
            gid = scene.add_triangles(mesh)
            geometry_ids.append(gid)
        result = scene.cast_rays(self._rays.to_o3d_tensors())
        mask = np.isfinite(result['t_hit'].numpy())
        self.r.mask = mask
        self.r.scales = result['t_hit'].numpy()[mask]
        self.r.points = self._rays.filter(mask).points(self.r.scales)
        self.r.initial_points = self.r.points.copy()
        # The values in result['geometry_ids'] refer to geometry ids
        # contained in geometry_ids which have been generated when registering meshes;
        # We find every result['geometry_ids'] in geometry_ids and this
        # way find the mesh index
        sorter = np.argsort(geometry_ids)
        gids = result['geometry_ids'].numpy()[mask]
        self.r.mesh_indices = sorter[np.searchsorted(geometry_ids, gids, sorter=sorter)]
        # Calculate barycentric coordinates of points
        barycentric_coordinates = np.zeros_like(self.r.points)
        barycentric_coordinates[:, 1:] = result['primitive_uvs'].numpy()[mask]
        barycentric_coordinates[:, 0] = 1.0 - barycentric_coordinates[:, 1] - \
            barycentric_coordinates[:, 2]
        # Interpolate normals and colors
        self.r.normals, self.r.colors = \
            get_interpolated_normals_and_colors(
                self._meshes,
                self.r.mesh_indices,
                result['primitive_ids'].numpy()[mask],
                barycentric_coordinates,
                )
