import numpy as np

from common.geometry_utils import mesh_create_quad, \
    mesh_translate
from camsimlib.material import Material
from camsimlib.light import point_visible_from_point, \
    sky_visible_from_points



def test_point_visible_from_point():
    point = np.array((0., 0., 0.))
    points = np.array((
        (0, 0, 0.5),
        (0, 0, 1.5),
        (0, 0, 1.0), # on quad
        (0, 0, 1.0), # on quad
    ))
    normals = np.array((
        (0, 0, -1.),
        (0, 0, -1.),
        (0, 0, -1.), # normals corrects in front of quad point
        (0, 0, 1.),  # normals corrects it behind quad
    ))
    quad = mesh_create_quad((2., 2.))
    mesh_translate(quad, (-1., -1., 1.))
    meshes = [ quad ]
    materials = [ Material() ]
    mask = point_visible_from_point(point, points, \
        normals, meshes, materials)
    assert np.all(mask == [ True, False, True, False])



def test_sky_visible_from_points():
    direction = np.array((0., 0., -1.))
    points = np.array((
        (0, 0, 0.5),
        (0, 0, 1.5),
        (0, 0, 1.0), # on quad
        (0, 0, 1.0), # on quad
    ))
    normals = np.array((
        (0, 0, -1.),
        (0, 0, -1.),
        (0, 0, -1.), # normals corrects in front of quad point
        (0, 0, 1.),  # normals corrects it behind quad
    ))
    quad = mesh_create_quad((2., 2.))
    mesh_translate(quad, (-1., -1., 1.))
    meshes = [ quad ]
    materials = [ Material() ]
    mask = sky_visible_from_points(direction, points, \
        normals, meshes, materials)
    assert np.all(mask == [ True, False, True, False])
