import numpy as np

from common.math_utils import normalize_3d



def get_interpolated_normals_and_colors(meshes, mesh_indices,
    triangle_indices, barycentric_coordinates):
    n = mesh_indices.size
    normals = np.zeros((n, 3))
    colors = np.zeros((n, 3))
    if n == 0:
        return normals, colors
    for mesh_index, mesh in enumerate(meshes):
        mask = mesh_indices == mesh_index
        tindices = triangle_indices[mask]
        vindices = mesh.triangle.indices.numpy()[tindices]
        # Normals
        try:
            vertex_normals = mesh.vertex.normals.numpy()[vindices]
            # Best case: we have vertex normals
            n = np.einsum('ijk, ij->ik', vertex_normals,
                barycentric_coordinates[mask])
            normals[mask] = normalize_3d(n)
        except KeyError:
            try:
                # Second best case: we have triangle normals
                normals[mask] = mesh.triangle.normals.numpy()[tindices]
            except KeyError:
                # Third best case: Error
                raise Exception(f'No normals found in mesh {mesh_index}')
        # Colors
        try:
            # Best case: we have vertex colors
            vertex_colors = mesh.vertex.colors.numpy()[vindices]
            colors[mask] = np.einsum('ijk, ij->ik', vertex_colors,
                barycentric_coordinates[mask])
        except KeyError:
            try:
                # Second best case: we have triangle colors
                colors[mask] = mesh.triangle.colors.numpy()[tindices]
            except KeyError:
                # Third best case: we assume white color
                colors[mask] = np.ones(3)
    return normals, colors



class RayTracerResult:
    """ Class to keep the results of a ray tracer run

    Contents:
    -> mask
    Mask with all rays intersecting with the mesh marked as True
    -> scales
    Lengths of the light path from ray origin to points
    -> initial_points
    Points where the ray tracer hit the first time (before maybe being reflected)
    -> points
    3D intersection points in cartesic coordinates (x, y, z)
    -> normals
    Interpolated normals of mesh at points
    -> colors
    Interpolated colors of mesh at points
    -> mesh_indices
    Indices of the meshes where points are located
    """

    def __init__(self, num_rays=0):
        self.clear(num_rays)



    def __str__(self):
        return f'RayTracerResult({np.sum(self.mask)/self.mask.size} intersections)'



    def clear(self, num_rays=0):
        self.mask = np.zeros(num_rays, dtype=bool)
        self.scales = np.zeros(num_rays)
        self.initial_points = np.zeros((num_rays, 3))
        self.points = np.zeros((num_rays, 3))
        self.normals = np.zeros((num_rays, 3))
        self.colors = np.zeros((num_rays, 3))
        self.mesh_indices = np.zeros(num_rays, dtype=int)



    def reduce_to_valid(self):
        self.scales = self.scales[self.mask]
        self.initial_points = self.initial_points[self.mask]
        self.points = self.points[self.mask]
        self.normals = self.normals[self.mask]
        self.colors = self.colors[self.mask]
        self.mesh_indices = self.mesh_indices[self.mask]



    def expand(self):
        n = self.mask.size
        tmp = RayTracerResult(n)
        tmp.mask = self.mask
        tmp.scales[self.mask] = self.scales
        tmp.initial_points[self.mask] = self.initial_points
        tmp.points[self.mask] = self.points
        tmp.normals[self.mask] = self.normals
        tmp.colors[self.mask] = self.colors
        tmp.mesh_indices[self.mask] = self.mesh_indices
        self.__dict__.update(tmp.__dict__)
