import json
import os
import sys
import time

import numpy as np

sys.path.append(os.path.abspath('../'))
from trafolib.trafo3d import Trafo3d
from common.image_utils import image_3float_to_rgb, image_save
from common.geometry_utils import mesh_create_quad, mesh_create_cs, \
    geometries_visualize
from common.aruco_utils import MultiAruco
from camsimlib.camera_model import CameraModel



def generate_object(pose):
    # Plane
    plane = mesh_create_quad((300, 200), color=(1, 1, 0))
    # Put coordinate system in center of plane
    plane.translate((-150, -100, 0))
    # Move plane slightly in negative Z; if we shot images of it, the markers will be above the plane
    plane.translate((0, 0, -1e-2))
    plane.transform((pose * Trafo3d(rpy=(np.pi, 0, 0))).get_homogeneous_matrix())
    # Markers
    markers = MultiAruco(length_pix=30, length_mm=30, pose=pose)
    markers.add_marker(0, Trafo3d(t=(-140, -90, 0)))
    markers.add_marker(1, Trafo3d(t=( 110, -90, 0)))
    markers.add_marker(2, Trafo3d(t=(-140,  60, 0)))
    markers.add_marker(3, Trafo3d(t=( 110,  60, 0)))
    # Meshes
    meshes = [ plane ]
    meshes.extend(markers.generate_mesh())
    return meshes, markers



if __name__ == "__main__":
     # Random but reproducible
    np.random.seed(42)
    # Path where to store the data
    data_dir = 'data'
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    print(f'Using data path "{data_dir}"')

    # Generate camera
    cam = CameraModel(chip_size=(40, 30),
                      focal_length=(35, 40),
                      distortion=(0.4, -0.2, 0, 0, 0))
    cam.scale_resolution(30)

    # Generate object
    world_to_object = Trafo3d(t=(-30, 20, 580), rpy=np.deg2rad((-10, -20, 20)))
    meshes, markers = generate_object(world_to_object)

    # Visualize
    if False:
        cam_cs = cam.get_cs(size=50.0)
        cam_frustum = cam.get_frustum(size=300.0)
        object_cs = mesh_create_cs(size=50)
        object_cs.transform(world_to_object.get_homogeneous_matrix())
        geometries_visualize([cam_cs, cam_frustum,
            object_cs, *meshes])

    # Snap scene
    basename = os.path.join(data_dir, f'cam00_image00')
    print(f'Snapping image {basename} ...')
    tic = time.monotonic()
    _, image, _ = cam.snap(meshes)
    toc = time.monotonic()
    print(f'    Snapping image took {(toc - tic):.1f}s')
    # Save generated snap
    image = image_3float_to_rgb(image)
    image_save(basename + '.png', image)
    # Save parameters
    params = {}
    params['cam'] = {}
    cam.dict_save(params['cam'])
    params['markers'] = {}
    markers.dict_save(params['markers'])
    with open(basename + '.json', mode='w', encoding='utf-8') as f:
       json.dump(params, f, indent=4, sort_keys=True)

