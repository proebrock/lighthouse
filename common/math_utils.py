import numpy as np



def hr_int(value):
    """ Human-readable string of long integers based on SI units
    :param value: Input value of type int
    :return: Human-readable string
    """
    suffixes = "kMGTPE"
    if -1000 < value < 1000:
        return f'{value}'
    suffix_index = 0
    while value <= -999_950 or value >= 999_950:
        value /= 1000
        suffix_index += 1
    return f'{value/1000.:.1f}{suffixes[suffix_index]}'



def length_3d(a):
    """ Length of 3D vectors
    Faster and more readable than: np.linalg.norm(a, axis=1)
    :param a: Input vectors, shape (n, 3)
    :return: Length, shape (n, )
    """
    return np.sqrt(
        a[:, 0] * a[:, 0] + \
        a[:, 1] * a[:, 1] + \
        a[:, 2] * a[:, 2])



def normalize_3d(a):
    """ Normalize vectors
    Faster and more readable than a = a / np.linalg.norm(a, axis=1)[:, np.newaxis]
    :param a: Input vectors, shape (n, 3)
    :return: Normalized vectors, shape (n, 3)
    """
    lengths = length_3d(a)
    if np.any(np.isclose(lengths, 0.0)):
        raise Exception('Normalization failed for some vectors')
    return a / lengths[:, np.newaxis]



def dot_3d(a, b):
    """ 3D dot product
    Faster and more readable than np.sum(a * b, axis=1)
    :param a: Input vector, shape (n, 3)
    :param b: Input vector, shape (n, 3)
    :return: Dot product, shape (n, )
    """
    return \
        a[:, 0] * b[:, 0] + \
        a[:, 1] * b[:, 1] + \
        a[:, 2] * b[:, 2]



def cross_3d(a, b):
    """ 3D cross product
    Faster alternative to np.cross(a, b, axis=1).
    :param a: Input vector, shape (n, 3)
    :param b: Input vector, shape (n, 3)
    :return: Cross product, shape (n, 3)
    """
    c = np.empty_like(a)
    c[:, 0] = a[:, 1] * b[:, 2] - a[:, 2] * b[:, 1]
    c[:, 1] = a[:, 2] * b[:, 0] - a[:, 0] * b[:, 2]
    c[:, 2] = a[:, 0] * b[:, 1] - a[:, 1] * b[:, 0]
    return c



def reflect_3d(incomings, normals):
    """ Reflect incoming vectors at an objects surface
    :param incomings: Outgoing vectors, shape (n, 3)
    :param normals: Surface normals, shape (n, 3)
    :return: Outgoing vectors, shape (n, 3)
    """
    dot_i_n = dot_3d(incomings, normals)
    return incomings - 2 * dot_i_n[:, np.newaxis] * normals



def angle_diff(a, b):
    """ Difference between two angles
    :param a: First angle
    :param b: Second angle
    :return: Angle difference
    """
    delta = np.abs(a - b)
    return np.min((delta, 2 * np.pi - delta))



def angle_mean(angles):
    """ Mean of multiple angles
    :param angles: Input angles
    :return: Mean angle
    """
    cossum = np.sum(np.cos(angles))
    sinsum = np.sum(np.sin(angles))
    return np.arctan2(sinsum, cossum)
