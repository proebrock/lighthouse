import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
import scipy.sparse

from trafolib.trafo3d import Trafo3d



def bundle_adjust_points_linear_all_visible(cams, p):
    """ Calculate bundle adjustment solving for 3D points
    This method is using a linearized model of the camera, only
    using the camera matrix (containing fx, fy, cx, cy) and the
    pose of the camera and ignoring distortion parameters.
    Advantage is speed, disadvantage is accuracy.
    Restriction: All points must be visible by all cameras.
    :param cams: List of n cameras
    :param p: 2D on-chip points, shape (m, n, 2)
    :return: 3D scene points, shape (m, 3)
    """
    # Check consistency
    assert p.ndim == 3
    num_points = p.shape[0]
    assert p.shape[1] == len(cams)
    num_views = p.shape[1]
    assert p.shape[2] == 2
    assert p.dtype == float

    # Check if enough observations available for all points
    assert not np.any(np.isnan(p)) # All points visible from all cams
    if not len(cams) >= 2:
        raise ValueError('Provide enough observations in p to solve bundle adjustment')

    # Matrix A contains 3x4 matrices for transforming a
    # 3D homogeneous scene point (4 components) and getting
    # 2D homogeneous chip point (3 components); this matrix
    # contains a camera matrix as well as the upper part of
    # the homogenous transformation matrix for cam_to_world
    A = np.zeros((3 * num_views, 4))
    for i, cam in enumerate(cams):
        cam_matrix = cam.get_camera_matrix()
        hom_matrix = cam.get_pose().inverse().get_homogeneous_matrix()
        A[3*i:3*i+3, :] = cam_matrix @ hom_matrix[0:3, :]
    # Matrix B contains image points
    B = np.ones((3 * num_views, num_points))
    for i, cam in enumerate(cams):
        B[3*i:3*i+2, :] = p[:, i, :].T
    # Calculate least squares optimal solution for scene points
    scene_points = np.linalg.pinv(A) @ B
    # Normalize by last component to get homogeneous vector
    # with 1 as last component
    scene_points /= scene_points[3, :]
    # Remove homogenous component
    return scene_points[0:3, :].T



def bundle_adjust_points_linear(cams, p):
    """ Calculate bundle adjustment solving for 3D points
    This method is using a linearized model of the camera, only
    using the camera matrix (containing fx, fy, cx, cy) and the
    pose of the camera and ignoring distortion parameters.
    Advantage is speed, disadvantage is accuracy.
    :param cams: List of n cameras
    :param p: 2D on-chip points, shape (m, n, 2)
    :return: 3D scene points, shape (m, 3)
    """
    # Check consistency
    assert p.ndim == 3
    num_points = p.shape[0]
    assert p.shape[1] == len(cams)
    num_views = p.shape[1]
    assert p.shape[2] == 2
    assert p.dtype == float

    # Check if enough observations available for all points
    mask_visible = np.all(np.isfinite(p), axis=2)
    mask_enough_observations = np.sum(mask_visible, axis=1) >= 2
    if not np.all(mask_enough_observations):
        raise ValueError('Provide enough observations in p to solve bundle adjustment')

    # Solve bundle adjust for every existing permutation of cameras observing points
    # If you have 4 cameras, *visibility* may be [ 1 0 0 1 ] (cameras 0 and 3 saw the points)
    # and *mask* is true for all points that have been seen by cameras 0 and 3.
    P = np.empty((num_points, 3))
    for visibility in np.unique(mask_visible, axis=0):
        vcams = [ cams[i] for i, v in enumerate(visibility) if v ]
        mask = np.all(mask_visible == visibility, axis=1)
        vp = p[mask, :, :][:, visibility, :]
        P[mask, :] = bundle_adjust_points_linear_all_visible(vcams, vp)
    return P



def _sparse_dilate(a, shape):
    result = scipy.sparse.lil_matrix(( \
        a.shape[0] * shape[0],
        a.shape[1] * shape[1]), dtype=a.dtype)
    rows, cols = a.nonzero()
    for row, col in zip(rows, cols):
        result[shape[0]*row:shape[0]*(row+1), \
               shape[1]*col:shape[1]*(col+1)] = a[row, col]
    return result

def _generate_sparsity_points(mask_visible):
    sparsity = scipy.sparse.lil_matrix( \
        (np.sum(mask_visible), mask_visible.shape[0]), dtype=int)
    r = 0
    for c in range(mask_visible.shape[0]):
        h = np.sum(mask_visible[c, :])
        sparsity[r:r+h, c] = 1
        r += h
    return _sparse_dilate(sparsity, (2, 3))

def _generate_sparsity_poses(mask_visible):
    sparsity = scipy.sparse.lil_matrix( \
        (np.sum(mask_visible), mask_visible.shape[1]), dtype=int)
    r = 0
    for i in range(mask_visible.shape[0]):
        h = np.sum(mask_visible[i, :])
        rows = np.arange(r, r+h)
        cols = np.where(mask_visible[i, :])[0]
        sparsity[rows, cols] = 1
        r += h
    return _sparse_dilate(sparsity, (2, 6))

def _generate_sparsity_points_and_poses(mask_visible):
    sparsity_points = _generate_sparsity_points(mask_visible)
    sparsity_poses = _generate_sparsity_poses(mask_visible)
    return scipy.sparse.hstack((sparsity_points, sparsity_poses))



def _objfun_bundle_adjust_points(x, cams, p, mask_visible):
    P = x.reshape((-1, 3))
    p_estim = np.zeros_like(p)
    for i, cam in enumerate(cams):
        p_estim[:, i, :] = cam.scene_to_chip(P)[:, 0:2] # Omit distance
    residuals = p_estim - p
    return residuals[mask_visible, :].ravel()



def bundle_adjust_points(cams, p, P_init=None, full=False, optimizer_opt={}):
    """ Calculate bundle adjustment solving for 3D points
    This bundle adjustment solver expects a number of cameras with fixed and
    known intrinsics and extrinsics and solves for a number of 3D points based
    on observations.
    The user provides an array p of 2D points of shape (m, n, 2).
    The i-th line of p with i in [0..m-1] contains the projection of a single unknown
    3D scene point onto the chips of up to n cameras. So p[i, j, :] contains the
    projection of said i-th 3D point onto the chip of the j-th camera with j in [0..n-1].
    In total the bundle adjustments reconstructs m 3D scene points.
    If a point was not observed by a camera, the user may mark it as (NaN, NaN).
    If any point was not seen by 2 or more cameras, it cannot be reconstructed and
    an exception is thrown.
    The residuals contain the on-chip reprojection error sqrt(du**2 + dv**2)
    in pixels, one for each point and camera. Residual is NaN if no observation
    by given camera.
    The distances are the distances of the camera rays to the finally reconstructed
    3D points.
    :param cams: List of n cameras
    :param p: 2D on-chip points, shape (m, n, 2)
    :param P_init: Initial guesses for 3D point positions, shape (m, 3)
    :param full: Provide just points (False) or additionally residuals and distance (True)
    :param optimizer_opt: Additional parameters provided to scipy.optimize.least_squares
    :return: 3D scene points, shape (m, 3); residuals, shape (m, n); distances, shape (m, n)
    """
    # Check consistency
    assert p.ndim == 3
    num_points = p.shape[0]
    assert p.shape[1] == len(cams)
    num_views = p.shape[1]
    assert p.shape[2] == 2
    assert p.dtype == float

    # Check if enough observations available for all points
    mask_visible = np.all(np.isfinite(p), axis=2)
    mask_enough_observations = np.sum(mask_visible, axis=1) >= 2
    if not np.all(mask_enough_observations):
        raise ValueError('Provide enough observations in p to solve bundle adjustment')

    # Initial estimates for the points
    if P_init is None:
        P_init = bundle_adjust_points_linear(cams, p)
    else:
        assert P_init.ndim == 2
        assert P_init.shape[0] == num_points
        assert P_init.shape[1] == 3
    x0 = P_init.ravel()

    # Setup sparse matrix with mapping of decision variables influncing residuals;
    # helps optimizer to efficiently calculate the Jacobian
    sparsity = _generate_sparsity_points(mask_visible)

    #plt.spy(sparsity)
    #plt.show()

    # Run numerical optimization
    # TODO: constraint optimization with Z>=0 ?
    # TODO: stable optimization with a loss function!?
    result = least_squares(_objfun_bundle_adjust_points, x0,
        args=(cams, p, mask_visible), jac_sparsity=sparsity, **optimizer_opt)
    if not result.success:
        raise Exception('Numerical optimization failed.')

    if False:
        residuals_x0 = _objfun_bundle_adjust_points(x0, cams, p, mask_visible)
        residuals = _objfun_bundle_adjust_points(result.x, cams, p, mask_visible)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(residuals_x0, label='before opt')
        ax.plot(residuals, label='after opt')
        ax.set_ylabel('Error (pixels)')
        ax.grid()
        ax.legend()
        plt.show()

    # Extract resulting points
    P = result.x.reshape((-1, 3))

    if not full:
        return P

    # Extract residuals
    residuals = np.empty_like(p)
    residuals[:] = np.nan
    residuals[mask_visible] = _objfun_bundle_adjust_points( \
        result.x, cams, p, mask_visible).reshape((-1, 2))
    # Calculate distance on chip in pixels: sqrt(dx**2+dy**2)
    residuals = np.sqrt(np.sum(np.square(residuals), axis=2))

    # Extract distances of camera rays to estimated 3D points
    distances = np.zeros((num_points, num_views))
    distances[:] = np.nan
    for i, cam in enumerate(cams):
        if np.sum(mask_visible[:, i]) == 0:
            continue
        rays = cam.get_rays(p[mask_visible[:, i], i, :])
        distances[mask_visible[:, i], i] = rays.to_points_distances( \
            P[mask_visible[:, i], :])

    return P, residuals, distances



def _param_to_x_objfun_bundle_adjust_points_and_poses(P, poses):
    # Scale for point coordinates and translations
    # to have similar sized decision variables
    # should be same value as in _x_to_param
    scale = 1000.0
    poses_coeff = []
    for pose in poses:
        p = np.concatenate((pose.get_translation() / scale,
            pose.get_rotation_rodrigues()))
        poses_coeff.append(p)
    poses_coeff = np.asarray(poses_coeff)
    x = np.concatenate((P.flatten() / scale, poses_coeff.flatten()))
    return x



def _x_to_param_objfun_bundle_adjust_points_and_poses(x, num_points):
    # Scale for point coordinates and translations
    # to have similar sized decision variables
    # should be same value as in _param_to_x
    scale = 1000.0
    P = scale * x[:3*num_points].reshape((num_points, 3))
    poses_coeff = x[3*num_points:].reshape((-1, 6))
    num_views = poses_coeff.shape[0]
    poses = []
    for i in range(num_views):
        T = Trafo3d(t=scale*poses_coeff[i, :3], rodr=poses_coeff[i, 3:])
        poses.append(T)
    return P, poses



def _objfun_bundle_adjust_points_and_poses(x, cam, p, mask_visible):
    P, poses = _x_to_param_objfun_bundle_adjust_points_and_poses(x, p.shape[0])
    p_estim = np.zeros_like(p)
    for i, pose in enumerate(poses):
        cam.set_pose(pose)
        p_estim[:, i, :] = cam.scene_to_chip(P)[:, 0:2] # Omit distance
    residuals = p_estim - p
    return residuals[mask_visible, :].ravel()



def bundle_adjust_points_and_poses(cam, p, P_init=None, pose_init=None,
    full=False, optimizer_opt={}):
    """ Calculate bundle adjustment solving for 3D points and poses
    This bundle adjustment solver expects a single camera with fixed and
    known intrinsics and solves for a number of 3D points and a number of
    camera poses based on observations.
    The user provides an array p of 2D points of shape (m, n, 2).
    The i-th line of p with i in [0..m-1] contains the projection of a single
    unknown 3D scene point onto the chip of the camera for any of its n poses.
    So p[i, j, :] contains the projection of said i-th 3D point onto the chip
    of the cameras j-th pose with j in [0..n-1].
    In total the bundle adjustments reconstructs m 3D scene points and
    n camera poses.
    If a point was not observed by the camera in any pose, the user may mark
    it as (NaN, NaN).
    If any point was not seen by 2 or more cameras, it cannot be reconstructed and
    an exception is thrown.
    The residuals contain the on-chip reprojection error sqrt(du**2 + dv**2)
    in pixels, one for each point and camera. Residual is NaN if no observation
    by given camera.
    :param cam: Camera model
    :param p: 2D on-chip points, shape (m, n, 2)
    :param P_init: Initial guesses for 3D point positions, shape (m, 3)
    :param pose_init: Inital guesses for camera poses (world to cam), list of Trafo3d with length n
    :param full: Provide just points (False) or additionally residuals (True)
    :param optimizer_opt: Additional parameters provided to scipy.optimize.least_squares
    :return: 3D scene points, shape (m, 3); camera poses, list of Trafo3d of length n, residuals, shape (m, n)
    """
    # Check consistency
    assert p.ndim == 3
    num_points = p.shape[0]
    num_views = p.shape[1]
    assert p.shape[2] == 2
    assert p.dtype == float

    # Check if enough observations available for all points
    mask_visible = np.all(np.isfinite(p), axis=2)
    mask_enough_observations = np.sum(mask_visible, axis=1) >= 2
    if not np.all(mask_enough_observations):
        raise ValueError('Provide enough observations in p to solve bundle adjustment')

    # Initial estimates for the points
    if P_init is None:
        P_init = np.zeros((num_points, 3))
        P_init[:, 2] = 500
    else:
        assert P_init.ndim == 2
        assert P_init.shape[0] == num_points
        assert P_init.shape[1] == 3
    if pose_init is None:
        pose_init = num_views * [ Trafo3d() ]
    else:
        assert len(pose_init) == num_views
    x0 = _param_to_x_objfun_bundle_adjust_points_and_poses(P_init, pose_init)

    # Setup sparse matrix with mapping of decision variables influncing residuals;
    # helps optimizer to efficiently calculate the Jacobian
    sparsity = _generate_sparsity_points_and_poses(mask_visible)

    # Run numerical optimization
    # TODO: constraint optimization with Z>=0 ?
    # TODO: stable optimization with a loss function!?
    result = least_squares(_objfun_bundle_adjust_points_and_poses, x0,
        args=(cam, p, mask_visible), jac_sparsity=sparsity, **optimizer_opt)
    if not result.success:
        raise Exception('Numerical optimization failed.')

    if False:
        # For testing the correct setting of the sparsity matrix:
        # Run optimization without 'jac_sparsity=sparsity',
        # with considerably low number of points and views and
        # some invisible points; then compare both sparsity matrices
        # in the console output
        with np.printoptions(threshold=100000, linewidth=100000):
            print()
            print(sparsity.toarray()) # Our calcuation of sparsity
            sparsity2 = (result.jac != 0).astype(int)
            print(sparsity2.toarray()) # Resulting sparsity from optimization
            assert np.all(sparsity.toarray() == sparsity2.toarray())

    if False:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(result.x)
        ax.set_ylabel('result.x')
        ax.grid()

        residuals_x0 = _objfun_bundle_adjust_points_and_poses(x0, cam, p, mask_visible)
        residuals = _objfun_bundle_adjust_points_and_poses(result.x, cam, p, mask_visible)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(residuals_x0, label='before opt')
        ax.plot(residuals, label='after opt')
        ax.set_ylabel('Error (pixels)')
        ax.grid()
        ax.legend()
        plt.show()

    # Extract resulting points
    P, poses = _x_to_param_objfun_bundle_adjust_points_and_poses(result.x, num_points)

    if not full:
        return P, poses

    # Extract residuals
    residuals = np.empty_like(p)
    residuals[:] = np.nan
    residuals[mask_visible] = _objfun_bundle_adjust_points_and_poses( \
        result.x, cam, p, mask_visible).reshape((-1, 2))
    # Calculate distance on chip in pixels: sqrt(dx**2+dy**2)
    residuals = np.sqrt(np.sum(np.square(residuals), axis=2))

    return P, poses, residuals