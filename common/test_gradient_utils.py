import pytest
import os
import numpy as np
import matplotlib.pyplot as plt

from common.gradient_utils import gradients_generate_half_ellipsoid, \
    gradients_generate_half_torus, gradients_generate_pyramid, gradients_generate_gaussian, \
    gradients_visualize, gradients_visualize_orthographic_projection, \
    gradients_to_normalmap, normalmap_to_gradients, \
    normalmap_load, normalmap_save, normalmap_visualize, \
    slant_tilt_to_normalmap, normalmap_to_slant_tilt, slant_tilt_visualize, \
    gradients_estimate, gradients_estimate_simple, \
    gradients_integrate_frankot



def test_correct_shapes_and_nan():
    shape = (240, 320)
    z, (p, q) = gradients_generate_half_ellipsoid( \
        shape, (160, 120, 0), (70, 50, 40))
    assert z.shape == shape
    assert p.shape == shape
    assert q.shape == shape
    assert np.all(np.isfinite(z) == np.isfinite(p))
    assert np.all(np.isfinite(z) == np.isfinite(q))

    z, (p, q) = gradients_generate_pyramid( \
        shape, (160, 120), (120, 80, 50))
    assert z.shape == shape
    assert p.shape == shape
    assert q.shape == shape

    z, (p, q) = gradients_generate_gaussian( \
        shape, 5e5, (160, 120), (1000, -400, -400, 1000))
    assert z.shape == shape
    assert p.shape == shape
    assert q.shape == shape



def test_gradient_normalmap_conversion_roundtrip():
    _, (p, q) = gradients_generate_half_ellipsoid( \
        (240, 320), (160, 120, 0), (70, 50, 40))
    mask = np.isfinite(p)
    # Gradients to normalmap
    nmap = gradients_to_normalmap(p, q)
    assert np.all(mask == np.all(np.isfinite(nmap), axis=2))
    # Normalmap back to gradients
    p_, q_ = normalmap_to_gradients(nmap)
    assert np.all(mask == np.isfinite(p_))
    assert np.all(mask == np.isfinite(q_))
    assert np.max(np.abs(p_[mask] - p[mask])) < 1e-6
    assert np.max(np.abs(q_[mask] - q[mask])) < 1e-6



def test_normal_slant_tilt_roundtrip():
    _, (p, q) = gradients_generate_half_ellipsoid( \
        (240, 320), (160, 120, 0), (70, 50, 40))
    nmap = gradients_to_normalmap(p, q)
    mask = np.all(np.isfinite(nmap), axis=2)
    slant, tilt = normalmap_to_slant_tilt(nmap)
    nmap_ = slant_tilt_to_normalmap(slant, tilt)
    mask_ = np.all(np.isfinite(nmap_), axis=2)
    assert np.all(mask == mask_)
    diff = np.linalg.norm(nmap[mask] - nmap_[mask_], axis=1)
    assert np.all(diff < 1e-6)



def test_normalmap_save_load_roundtrip(tmp_path):
    _, (p, q) = gradients_generate_half_ellipsoid( \
        (240, 320), (160, 120, 0), (70, 50, 40))
    nmap = gradients_to_normalmap(p, q)
    mask = np.all(np.isfinite(nmap), axis=2)
    filename = os.path.join(tmp_path, 'nmap.png')
    normalmap_save(filename, nmap)
    nmap_ = normalmap_load(filename)
    mask_ = np.all(np.isfinite(nmap_), axis=2)
    assert np.all(mask == mask_)
    diff = np.linalg.norm(nmap[mask] - nmap_[mask_], axis=1)
    assert np.all(diff < 1e-4)



def test_gradient_estimation_simple():
    z = np.array([
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  2,  2,  2,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
    ])
    p, q = gradients_estimate(z)
    p_expected = np.array([
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  1,  1,  0, -1, -1,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
    ])
    q_expected = np.array([
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0,  1,  1,  1,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
        [ 0,  0, -1, -1, -1,  0,  0],
        [ 0,  0,  0,  0,  0,  0,  0],
    ])
    assert np.allclose(p, p_expected)
    assert np.allclose(q, q_expected)




def test_gradient_estimation_ellipsoid():
    z, (p, q) = gradients_generate_half_ellipsoid( \
        (240, 320), (160, 120, 0), (70, 50, 40))
    mask = np.isnan(z)
    z[mask] = 0.
    p[mask] = 0.
    q[mask] = 0.
    p_, q_ = gradients_estimate(z)
    # Problem with comparison of gradients for this particular object:
    # The numerically estimated gradients in general are very accurate;
    # just in areas where the surface is nearly facing away from the viewer
    # the estimated gradients are bad.
    # We just check if 90% of the gradients are good enough
    assert np.percentile(np.abs(p_ - p)[~mask], 90.) < 0.1
    assert np.percentile(np.abs(q_ - q)[~mask], 90.) < 0.1



def test_gradient_estimation_torus():
    z, (p, q) = gradients_generate_half_torus( \
        (240, 320), (160, 120), (40, 70))
    mask = np.isnan(z)
    z[mask] = 0.
    p[mask] = 0.
    q[mask] = 0.
    p_, q_ = gradients_estimate(z)
    # Problem with comparison of gradients for this particular object:
    # The numerically estimated gradients in general are very accurate;
    # just in areas where the surface is nearly facing away from the viewer
    # the estimated gradients are bad.
    # We just check if 90% of the gradients are good enough
    assert np.percentile(np.abs(p_ - p)[~mask], 90.) < 0.01
    assert np.percentile(np.abs(q_ - q)[~mask], 90.) < 0.01



def test_gradient_estimation_pyramid():
    z, (p, q) = gradients_generate_pyramid( \
        (240, 320), (160, 120), (120, 80, 50))
    mask = np.isnan(z)
    z[mask] = 0.
    p[mask] = 0.
    q[mask] = 0.
    p_, q_ = gradients_estimate(z)
    # Problem with comparison of gradients for this particular object:
    # The numerically estimated gradients in general are very accurate;
    # just in areas where the surface is nearly facing away from the viewer
    # the estimated gradients are bad.
    # We just check if 90% of the gradients are good enough
    assert np.percentile(np.abs(p_ - p)[~mask], 95.) < 1e-6
    assert np.percentile(np.abs(q_ - q)[~mask], 95.) < 1e-6



def test_gradient_estimation_gaussian():
    z, (p, q) = gradients_generate_gaussian( \
        (240, 320), 5e5, (160, 120), (1000, -400, -400, 1000))
    p_, q_ = gradients_estimate(z)
    assert np.max(np.abs(p_ - p)) < 1e-2
    assert np.max(np.abs(q_ - q)) < 1e-2
    p_, q_ = gradients_estimate_simple(z)
    assert np.max(np.abs(p_ - p)) < 1e-2
    assert np.max(np.abs(q_ - q)) < 1e-2



def test_gradient_integration_frankot():
    z, (p, q) = gradients_generate_gaussian( \
        (240, 320), 5e5, (160, 120), (1000, -400, -400, 1000))
    # Positive gradients should yield the original surface
    z_ = gradients_integrate_frankot(p, q)
    # z and z_ can be different up to a constant, so subtract mean from diff
    diff = z_ - z
    diff -= np.mean(diff)
    assert np.max(np.abs(diff)) < 1e-3



def test_gradient_integration_frankot_negative():
    z, (p, q) = gradients_generate_gaussian( \
        (240, 320), 5e5, (160, 120), (1000, -400, -400, 1000))
    # Negative gradients should yield negative z surface
    z_ = gradients_integrate_frankot(-p, -q)
    # z and z_ can be different up to a constant, so subtract mean from diff
    diff = z_ - (-z)
    diff -= np.mean(diff)
    assert np.max(np.abs(diff)) < 1e-3



def test_surface_tangent_vectors_orthogonal_to_normals():
    """
    Check if surface tangent vectors are orthogonal to the normal vectors

    See Antensteiner et al: "A Review of Depth and Normal Fusion Algorithms"
    2018, Equation (9): Tx=(1, 0, p) and Ty=(0, 1, q) are the surface
    tangent vectors with cross(Tx, Ty)=n
    Since Tx and Ty are perpendicular to n, we have dot(n, Tx)=dot(n, Ty)=0,
    equivalent to nx+p*nz=0 and ny+q*nz=0
    """
    z, (p, q) = gradients_generate_gaussian( \
        (240, 320), 5e5, (160, 120), (1000, -400, -400, 1000))
    nmap = gradients_to_normalmap(p, q)
    p = p.ravel()
    q = q.ravel()
    n = nmap.reshape((-1, 3))
    assert np.all(np.isclose(n[:, 0] + p * n[:, 2], 0.0))
    assert np.all(np.isclose(n[:, 1] + q * n[:, 2], 0.0))



#@pytest.mark.skip()
def test_foo():
    shape = (240, 320)

    z0, (p0, q0) = gradients_generate_half_ellipsoid( \
        shape, (160, 120, 0), (70, 50, 50))
    z1, (p1, q1) = gradients_generate_half_torus( \
        shape, (160, 120), (40, 70))
    z2, (p2, q2) = gradients_generate_pyramid( \
        shape, (160, 120), (120, 80, 60))
    z3, (p3, q3) = gradients_generate_gaussian( \
        shape, 3e5, (160, 120), (1000, -400, -400, 1000))

    z = np.vstack((
        np.hstack((z0, z1)),
        np.hstack((z2, z3)),
    ))
    z[np.isnan(z)] = 0.
    p = np.vstack((
        np.hstack((p0, p1)),
        np.hstack((p2, p3)),
    ))
    p[np.isnan(p)] = 0.
    q = np.vstack((
        np.hstack((q0, q1)),
        np.hstack((q2, q3)),
    ))
    q[np.isnan(q)] = 0.

    _, ax = plt.subplots()
    ax.imshow(z)

    nmap = gradients_to_normalmap(p, q)
    normalmap_visualize(nmap)
    plt.show()

    gradients_visualize_orthographic_projection( \
        z, p, q, normal_scale=10)

    z_ = gradients_integrate_frankot(p, q)
    gradients_visualize_orthographic_projection( \
        z_, p, q, normal_scale=10)



def test_bar():
    nmap = normalmap_load('data/normal_map.png')
    #nmap = normalmap_load('data/scholar.png')
    mask = np.any(np.isnan(nmap), axis=2)
    nmap[mask, 0:2] = 0.
    nmap[mask, 2] = 1.
    normalmap_visualize(nmap)
    #nmap[:, :, 1] = -nmap[:, :, 1] # Y has to be negated!?
    #nmap[:, :, 2] = -nmap[:, :, 2]
    plt.show()

    p, q = normalmap_to_gradients(nmap)
    #q = -q
    z = gradients_integrate_frankot(p, q)
    gradients_visualize_orthographic_projection( \
        z, p, q, mode='surface', size=10)
