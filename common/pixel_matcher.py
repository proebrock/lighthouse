""" Module for correspondence matching of pixels
"""

from abc import ABC, abstractmethod
import os
import sys
import json

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../'))
from common.image_utils import image_rgb_to_gray, image_gray_to_rgb



class LineMatcher(ABC):
    """ Abstract base class (ABC) for any line matcher
    """

    def __init__(self, num_pixels=100):
        """ Constructor
        :param num_pixels: Number of pixels to match
        """
        if not isinstance(num_pixels, int):
            raise ValueError('Provide number of pixels as integer')
        self._num_pixels = num_pixels



    def num_pixels(self):
        """ Get number of pixels to match
        :return: Number of pixels
        """
        return self._num_pixels



    def dict_save(self, param_dict):
        """ Save object to dictionary
        :param param_dict: Dictionary to store data in
        """
        param_dict['num_pixels'] = self._num_pixels



    def dict_load(self, param_dict):
        """ Load object from dictionary
        :param param_dict: Dictionary with data
        """
        self._num_pixels = param_dict['num_pixels']



    @abstractmethod
    def num_time_steps(self):
        """ Get number of time steps necessary to match the number of pixels
        Dependent on implementation; override in derived class
        :return: Number of time steps
        """
        raise NotImplementedError



    @abstractmethod
    def generate(self):
        """ Generates stack of lines
        Shape is (num_time_steps, num_pixels, 3), dtype np.uint8 (RGB)
        Dependent on implementation; override in derived class
        :return: Stack of lines
        """
        raise NotImplementedError



    @abstractmethod
    def _match(self, images, image_blk, image_wht, full=False):
        """ Match stack of image lines
        internal implementation without consistency checks and
        only dealing with 1-dimensional image lines (plus another dimension for RGB)
        Dependent on implementation; override in derived class
        :param images: Line stack to match, shape (num_time_steps, n, 3), dtype np.uint8
        :param image_blk: all black image, shape (n, 3), dtype np.uint8
        :param image_wht: all white image, shape (n, 3), dtype np.uint8
        :param full: provide full result, e.g. amplitude, residuals
        :return: Indices refering to originally generated stack of lines,
            shape (n, ), dtype float (sub-pixel accuracy possible),
            invalid matches represented by np.nan
        """
        raise NotImplementedError



    def match(self, images, image_blk=None, image_wht=None, full=False):
        """ Match stack of image lines or images
        :param images: Line stack to match, shape (num_time_steps, n, m, 3),
                       3 color channels RGB, dtype np.uint8
        :param image_blk: all black image, shape (n, m, 3), dtype np.uint8
        :param image_wht: all white image, shape (n, m, 3), dtype np.uint8
        :return: Indices refering to originally generated stack of lines,
            shape (n, m), dtype float (sub-pixel accuracy possible),
            invalid matches represented by np.nan
        """
        if images.ndim <= 2:
            raise ValueError('Provide proper images')
        if images.shape[0] != self.num_time_steps():
            raise ValueError('Provide correct number of images')
        if images.shape[-1] != 3:
            raise ValueError('Provide color images')
        if images.dtype != np.uint8:
            raise ValueError('Provide images of correct type')
        if image_blk is None:
            image_blk = np.zeros_like(images[0], dtype=np.uint8)
        elif not np.all(images[0].shape == image_blk.shape):
            raise ValueError('Provide properly shaped black image')
        if image_wht is None:
            image_wht = 255 * np.ones_like(images[0], dtype=np.uint8)
        elif not np.all(images[0].shape == image_wht.shape):
            raise ValueError('Provide properly shaped white image')
        if full:
            indices, others = self._match( \
                images.reshape((self.num_time_steps(), -1, 3)),
                image_blk.reshape(-1, 3),
                image_wht.reshape(-1, 3),
                full)
            indices = indices.reshape(images.shape[1:-1])
            for i in range(len(others)):
                # If any content of others is numpy array,
                # reshape content to original image shape
                # just like indices itself
                if isinstance(others[i], np.ndarray):
                    others[i] = others[i].reshape(images.shape[1:-1])
            return indices, others
        else:
            indices = self._match( \
                images.reshape((self.num_time_steps(), -1, 3)),
                image_blk.reshape(-1, 3),
                image_wht.reshape(-1, 3),
                full)
            return indices.reshape(images.shape[1:-1])



    def analyze_plot_init(self):
        """ Initialize matplotlib figure to analyze image data and matching

        Overload in derived classes to add matcher specific information;
        is called by ImageMatcher::analyze_plot().
        """
        fig, ax = plt.subplots()
        indices = np.arange(self.num_time_steps())
        values = np.zeros(self.num_time_steps())
        dots, = ax.plot(indices, values, '-ob')
        ax.set_ylim(0, 255)
        ax.set_xlabel('time step')
        ax.set_ylabel('pixel brightness')
        return fig, ax, [ dots ]



    def analyze_plot_update(self, fig, objs, pix_over_time):
        """ Update matplotlib figure to analyze image data and matching

        Overload in derived classes to add matcher specific information;
        is called by ImageMatcher::analyze_plot().

        :param fig: Figure; created by analyze_plot_init
        :param objs: Data object to update; created by analyze_plot_init
        :param pix_over_time: Pixel data over time of currently user-selected pixel
        """
        indices = np.arange(self.num_time_steps())
        objs[0].set_data(indices, image_rgb_to_gray(pix_over_time))
        fig.canvas.draw_idle()



class LineMatcherBinary(LineMatcher):
    """ Line matcher implementation based on binary patterns
    """

    def __init__(self, num_pixels=100):
        super().__init__(num_pixels)
        self._min_amplitude = 10
        self._separation_threshold = 0.6



    def num_time_steps(self):
        # Determine a power of two that: 2**power <= _num_pixels
        power = 0
        while 2**power < self._num_pixels:
            power += 1
        return power



    def dict_save(self, param_dict):
        """ Save object to dictionary
        :param param_dict: Dictionary to store data in
        """
        super().dict_save(param_dict)
        param_dict['min_amplitude'] = self._min_amplitude
        param_dict['separation_threshold'] = self._separation_threshold



    def dict_load(self, param_dict):
        """ Load object from dictionary
        :param param_dict: Dictionary with data
        """
        super().dict_load(param_dict)
        self._min_amplitude = param_dict['min_amplitude']
        self._separation_threshold = param_dict['separation_threshold']



    def _generate_lookup_tables(self):
        """ Lookup tables generation for binary <-> gray code conversion
        """
        # We need more than self.num_pixels() entries to be able to
        # provide tables for forward and backward translation
        num_entries = 1 << self.num_time_steps()
        bin_to_gray = np.zeros(num_entries, dtype=int)
        gray_to_bin = np.zeros(num_entries, dtype=int)
        for binary in range(self.num_pixels()):
            gray = binary ^ (binary >> 1)
            bin_to_gray[binary] = gray
            gray_to_bin[gray] = binary
        return bin_to_gray, gray_to_bin



    def generate(self):
        # Generate binary to gray code translation table
        # and resize to number of pixels
        bin_to_gray, _ = self._generate_lookup_tables()
        bin_to_gray = bin_to_gray[0:self.num_pixels()]
        num_time_steps = self.num_time_steps()
        lines = np.zeros((num_time_steps, self._num_pixels), dtype=np.uint8)
        for i in range(num_time_steps):
            mask = 1 << (num_time_steps - i - 1)
            lines[i, (bin_to_gray & mask) > 0] = 255
        return image_gray_to_rgb(lines)



    def _match(self, images, image_blk, image_wht, full=False):
        # Convert image to gray
        img_gray = image_rgb_to_gray(images)
        img = img_gray.astype(float)
        # Determine min/max of each pixel and range
        # np.min(img, axis=0) and np.max(img, axis=0) do not work here
        # because the a code in one pixel over time may contain
        # all-zeros or all-ones and then automatic range determination fails
        imin = image_rgb_to_gray(image_blk)
        imax = image_rgb_to_gray(image_wht)
        # Filter pixels where the amplitude of the signal
        # (difference between min and max brightness) is high enough
        irange = imax - imin
        valid = irange >= self._min_amplitude
        phi = (1.0 - np.clip(self._separation_threshold, 0, 1)) / 2.0
        valid[valid] = np.all(np.logical_or(
            # Brighnesses must be either lower than (min + phi * range) for "low"...
            img[:, valid] < (imin[np.newaxis, valid] + irange[valid] * phi),
            # ... or higher than (min + (1-phi) * range) for "high"
            img[:, valid] > (imin[np.newaxis, valid] + irange[valid] * (1 - phi)),
        ), axis=0)
        # Binarized brighness values
        binarized_img = img[:, valid] > imin[np.newaxis, valid] + irange[valid] * 0.5
        # Factors are powers of 2
        factors = np.zeros_like(binarized_img, dtype=int)
        factors = np.power(2, np.arange(img.shape[0])[::-1])[:, np.newaxis]
        # Gray code indices
        gray_idx = np.sum(binarized_img * factors, axis=0).astype(int)
        # Convert into binary indices
        indices = np.empty(img.shape[1])
        indices[:] = np.nan
        _, gray_to_bin = self._generate_lookup_tables()
        indices[valid] = gray_to_bin[gray_idx].astype(float)

        if not full:
            return indices

        # Amplitude of signal
        amplitudes = np.zeros(images.shape[1])
        amplitudes[:] = np.nan
        amplitudes[valid] = irange

        return indices, [ amplitudes ]



class LineMatcherPhaseShift(LineMatcher):
    """ Line matcher implementation based on phase shifted sine patterns
    """

    def __init__(self, num_pixels=100, num_time_steps=21, num_phases=2):
        super().__init__(num_pixels)
        self._num_time_steps = num_time_steps
        self._margin = 0.05
        self._num_phases = num_phases
        self._min_amplitude = 10



    def num_time_steps(self):
        return self._num_time_steps



    def dict_save(self, param_dict):
        """ Save object to dictionary
        :param param_dict: Dictionary to store data in
        """
        super().dict_save(param_dict)
        param_dict['num_time_steps'] = self._num_time_steps
        param_dict['margin'] = self._margin
        param_dict['num_phases'] = self._num_phases
        param_dict['min_amplitude'] = self._min_amplitude



    def dict_load(self, param_dict):
        """ Load object from dictionary
        :param param_dict: Dictionary with data
        """
        super().dict_load(param_dict)
        self._num_time_steps = param_dict['num_time_steps']
        self._margin = param_dict['margin']
        self._num_phases = param_dict['num_phases']
        self._min_amplitude = param_dict['min_amplitude']



    @staticmethod
    def _model(phases, angles):
        values =  np.tile(phases, (len(angles), 1))
        values += np.tile(angles, (len(phases), 1)).T
        return np.sin(values)



    def _get_angles(self):
        return np.linspace(0, self._num_phases * 2 * np.pi,
            self._num_time_steps + 1)[:-1]



    def generate(self):
        # Phase shift angles used to encode pixel index;
        # margin parameter is used to avoid ambiguities around 0°/360°
        phases = np.linspace(self._margin, 2 * np.pi - self._margin,
            self.num_pixels())
        # Angles
        angles = self._get_angles()
        values = self._model(phases, angles)
        lines = (255.0 * (values + 1.0)) / 2.0
        lines = lines.astype(np.uint8)
        return image_gray_to_rgb(lines)



    @staticmethod
    def _sine_phase_fit(values, angles):
        """
        Implemented after IEEE-STD-1057 for 3 parameters (known frequency).

        Fitting this model with 3 parameters (omega is constant):
        value = amplitude * sin(omega*t + phi) + offset

        Equivalent to
        value = amplitude * sin(phi) * cos(omega*t) + amplitude * cos(phi) * sin(omega*t) + offset

        which is
        value = A * cos(omega*t) + B * sin(omega*t) + C
        (with A = amplitude * sin(phi), B = amplitude * cos(phi), C = offset)

        arrange as matrices with X = [ A, B, C ] and solve linear equations for X
        D = X * values <=> X = pinv(D) * values

        :param values: shape (number of angles, number of curves to fit)
        :param angles: shape (number of angles, )
        """
        D = np.empty((angles.size, 3))
        D[:, 0] = np.cos(angles)
        D[:, 1] = np.sin(angles)
        D[:, 2] = 1.0
        Dinv = np.linalg.pinv(D)
        X = Dinv @ values
        phases = np.arctan2(X[0, :], X[1, :])
        amplitudes = X[0, :] / np.sin(phases)
        offsets = X[2, :]
        residuals_rms = np.sqrt(np.mean(np.square(values - D @ X), axis=0))
        return amplitudes, offsets, phases, residuals_rms



    def _match(self, images, image_blk, image_wht, full=False):
        # Convert image to gray
        img_gray = image_rgb_to_gray(images)
        img = img_gray.astype(float)
        # Determine min/max of each pixel and range
        imin = np.min(img, axis=0)
        imax = np.max(img, axis=0)
        # Filter pixels where the amplitude of the signal
        # (difference between min and max brightness) is high enough
        irange = imax - imin
        valid = irange >= self._min_amplitude
        # Fit sine functions
        angles = self._get_angles()
        amp, _, phases, res = self._sine_phase_fit(img[:, valid], angles)
        phases = (phases + 2*np.pi) % (2*np.pi) # Wrap to [0..2*pi]
        # Calculate indices from phases
        indices = np.zeros(images.shape[1])
        indices[:] = np.nan
        indices[valid] = ((phases - self._margin) * (self._num_pixels - 1)) / \
            (2*np.pi - 2*self._margin)

        if not full:
            return indices

        # Amplitude of signal
        amplitudes = np.zeros(images.shape[1])
        amplitudes[:] = np.nan
        amplitudes[valid] = amp
        # Residuals (RMS)
        residuals = np.zeros(images.shape[1])
        residuals[:] = np.nan
        residuals[valid] = res

        return indices, [ amplitudes, residuals ]



class ImageMatcher:
    """ Image matcher for matching 2D images using two line matchers for rows/cols
    """

    def __init__(self, shape=(100, 160), row_matcher=None, col_matcher=None):
        if row_matcher is None:
            self._row_matcher = LineMatcherPhaseShift(shape[0])
        else:
            self._row_matcher = row_matcher
        if col_matcher is None:
            self._col_matcher = LineMatcherPhaseShift(shape[1])
        else:
            self._col_matcher = col_matcher



    def num_time_steps(self):
        """ Get number of time steps necessary to match the image
        Dependent on line matchers plus 2 images for black and white image.
        :return: Number of time steps
        """
        return 2 + self._row_matcher.num_time_steps() + self._col_matcher.num_time_steps()



    def dict_save(self, param_dict):
        """ Save object to dictionary
        :param param_dict: Dictionary to store data in
        """
        # Row matcher
        param_dict['row_matcher'] = {}
        param_dict['row_matcher']['module_name'] = __name__
        param_dict['row_matcher']['class_name'] = self._row_matcher.__class__.__name__
        self._row_matcher.dict_save(param_dict['row_matcher'])
        # Column matcher
        param_dict['col_matcher'] = {}
        param_dict['col_matcher']['module_name'] = __name__
        param_dict['col_matcher']['class_name'] = self._col_matcher.__class__.__name__
        self._col_matcher.dict_save(param_dict['col_matcher'])



    def dict_load(self, param_dict):
        """ Load object from dictionary
        :param param_dict: Dictionary with data
        """
        # Row matcher
        module_name = param_dict['row_matcher']['module_name']
        class_name = param_dict['row_matcher']['class_name']
        cls = getattr(sys.modules[module_name], class_name)
        self._row_matcher = cls()
        self._row_matcher.dict_load(param_dict['row_matcher'])
        # Column matcher
        module_name = param_dict['col_matcher']['module_name']
        class_name = param_dict['col_matcher']['class_name']
        cls = getattr(sys.modules[module_name], class_name)
        self._col_matcher = cls()
        self._col_matcher.dict_load(param_dict['col_matcher'])



    def json_save(self, filename):
        """ Save object to json file
        :param filename: Filename (and path)
        """
        param_dict = {}
        self.dict_save(param_dict)
        with open(filename, mode='w', encoding='utf-8') as file_handle:
            json.dump(param_dict, file_handle, indent=4, sort_keys=True)



    def json_load(self, filename):
        """ Load object from json file
        :param filename: Filename (and path)
        """
        with open(filename, mode='r', encoding='utf-8') as file_handle:
            param_dict = json.load(file_handle)
        self.dict_load(param_dict)



    def generate(self):
        """ Generates a stack of images
        Those images can be displayed sequentially by a projector or a screen
        and later

        The image stack has a shape of (k, l, m, 3) and contains k images
        of shape (l, m). Data type is uint8 (RGB).

        :return: Stack of images
        """
        images = np.empty((
            self.num_time_steps(),
            self._row_matcher.num_pixels(),
            self._col_matcher.num_pixels(),
            3), dtype=np.uint8)
        images[0, :, :, :] = 0   # All black
        images[1, :, :, :] = 255 # All white
        # Row images
        offs = 2
        lines = self._row_matcher.generate()
        for i in range(lines.shape[0]):
            images[offs+i, :, :, :] = lines[i, :, np.newaxis]
        # Column images
        offs += lines.shape[0]
        lines = self._col_matcher.generate()
        for i in range(lines.shape[0]):
            images[offs+i, :, :, :] = lines[i, np.newaxis, :] # Col images
        return images



    def match(self, images, full=False):
        """ Match stack of 2D images
        :param images: Image stack of n RGB images, shape (n, rows, cols, 3)
        :param full: provide full result, e.g. amplitude, residuals
        """
        # Check consistency of images
        if images.ndim != 4:
            raise ValueError('Provide proper images')
        if images.shape[0] != self.num_time_steps():
            raise ValueError('Provide correct number of images')
        if images.shape[-1] != 3:
            raise ValueError('Provide color images')
        if images.dtype != np.uint8:
            raise ValueError('Provide images of correct type')
        # Separate different images
        n = 2 + self._row_matcher.num_time_steps()
        image_black = images[0]
        image_white = images[1]
        images_rows = images[2:n]
        images_cols = images[n:]
        # Run matching
        indices = np.empty((images.shape[1], images.shape[2], 2))
        if full:
            indices[:, :, 0], others_row = self._row_matcher.match( \
                images_rows, image_black, image_white, full)
            indices[:, :, 1], others_col = self._col_matcher.match( \
                images_cols, image_black, image_white, full)
            return indices, others_row + others_col
        else:
            indices[:, :, 0] = self._row_matcher.match( \
                images_rows, image_black, image_white, full)
            indices[:, :, 1] = self._col_matcher.match( \
                images_cols, image_black, image_white, full)
            return indices



    def analyze_plot(self, images):
        """ Create interactive matplotlib figures to analyze image stack and matching
        :param images: Image stack of n RGB images, shape (n, rows, cols, 3)
        """
        # Check consistency of images
        if images.ndim != 4:
            raise ValueError('Provide proper images')
        if images.shape[0] != self.num_time_steps():
            raise ValueError('Provide correct number of images')
        if images.shape[-1] != 3:
            raise ValueError('Provide color images')
        if images.dtype != np.uint8:
            raise ValueError('Provide images of correct type')
        # Separate different images
        n = 2 + self._row_matcher.num_time_steps()
        #image_black = images[0]
        image_white = images[1]
        images_rows = images[2:n]
        images_cols = images[n:]
        # Main plot that reacts to mouse-over events
        display_fig, display_ax = plt.subplots()
        display_ax.imshow(image_white)
        display_ax.set_xlabel('column')
        display_ax.set_ylabel('row')
        # Debug plots showing values of single pixel in time for row/col matching
        row_fig, row_ax, row_objs = self._row_matcher.analyze_plot_init()
        row_ax.set_title('row matching')
        col_fig, col_ax, col_objs = self._col_matcher.analyze_plot_init()
        col_ax.set_title('col matching')

        def display_mouse_move(event):
            x, y = event.xdata, event.ydata
            if x is None or y is None:
                return
            row = np.round(y).astype(int)
            col = np.round(x).astype(int)
            self._row_matcher.analyze_plot_update(row_fig, row_objs,
                images_rows[:, row, col, :])
            self._col_matcher.analyze_plot_update(col_fig, col_objs,
                images_cols[:, row, col, :])

        def display_close_event(event):
            # pylint: disable=unused-argument
            plt.close(row_fig)
            plt.close(col_fig)

        display_fig.canvas.mpl_connect('motion_notify_event', display_mouse_move)
        display_fig.canvas.mpl_connect('close_event', display_close_event)
        plt.show(block=True)
