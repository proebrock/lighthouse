import numpy as np
import matplotlib.pyplot as plt

import cv2
import open3d as o3d

from common.geometry_utils import pcl_create, pcl_set_colors, \
    lineset_create, mesh_create_cs, geometries_visualize



def gradients_generate_half_ellipsoid(shape, center, semi_axes):
    """ Generates depth image and gradients of an ellipsoid.

                       X
                    shape[1]
                  semi_axes[0]
                  -----------
                 |           |
         Y       |     Z     |
      shape[0]   |     X     |    Image of ellipsoid
    semi_axes[1] |           |
                 |           |
                  -----------

    The parameter center contains the center of the half-ellipsoid in
    X, Y and Z in unit pixels. The parameter semi_axes contains the
    size of the half-ellipsoid in X, Y and Z in unit pixels.

    An ellipsoid with center (x0, y0, z0) and semi-axes (a, b, c)
    is defined by the equation:

    ((x-x0)/a)^2 + ((y-y0)/b)^2 + ((z-z0)/c)^2 = 1

    Written as a function

    f(z) = c * zz + z0
    with zz = np.sqrt(1 -((x-x0)/a)^2 + ((y-y0)/b)^2)

    An partial derivatives

    dz/dx = (-c * (x - x0)) / (a^2 * zz)
    dz/dy = (-c * (y - y0)) / (b^2 * zz)

    :param shape: Shape of resulting depth image in pixels, shape (2, )
    :param center: Center of ellipsoid, shape (3, )
    :param semi_axes: Semi-axes of ellipsoid, shape (3, )
    :return: depth image (z), gradient (dz/dx, dz/dy)
    """
    assert len(shape) == 2
    assert len(center) == 3
    assert len(semi_axes) == 3
    # Create X/Y with meshgrid
    x = np.arange(shape[1], dtype=float)
    y = np.arange(shape[0], dtype=float)
    x, y = np.meshgrid(x, y, indexing='xy')
    # Depth image
    x0, y0, z0 = center
    a, b, c = semi_axes
    zz_sqr = (1.0 - ((x - x0) / a)**2 - ((y - y0) / b)**2)
    mask = zz_sqr >= 1e-3
    z = np.empty(shape)
    z[:] = np.nan
    z[mask] = c * np.sqrt(zz_sqr[mask]) + z0
    # Gradient fields
    p = np.empty(shape)
    p[:] = np.nan
    p[mask] = (-c * (x[mask] - x0))/(a**2 * np.sqrt(zz_sqr[mask]))
    q = np.empty(shape)
    q[:] = np.nan
    q[mask] = (-c * (y[mask] - y0))/(b**2 * np.sqrt(zz_sqr[mask]))
    return z, (p, q)



def gradients_generate_half_torus(shape, center, radii):
    assert len(shape) == 2
    assert len(center) == 2
    assert len(radii) == 2
    assert radii[0] < radii[1]
    # Create X/Y with meshgrid
    x = np.arange(shape[1], dtype=float)
    y = np.arange(shape[0], dtype=float)
    x, y = np.meshgrid(x, y, indexing='xy')
    # Depth image
    x0, y0 = center
    r, R = radii
    rr = np.sqrt((x-x0)**2 + (y-y0)**2)
    zz_sqr = r**2 - (rr - R)**2
    mask = zz_sqr >= 1e-3
    z = np.empty(shape)
    z[:] = np.nan
    z[mask] = np.sqrt(zz_sqr[mask])
    # Gradient fields
    g = (R - rr[mask]) / (z[mask] * rr[mask])
    p = np.empty(shape)
    p[:] = np.nan
    p[mask] = g * (x[mask] - x0)
    q = np.empty(shape)
    q[:] = np.nan
    q[mask] = g * (y[mask] - y0)
    return z, (p, q)



def gradients_generate_pyramid(shape, center, semi_axes):
    assert len(shape) == 2
    assert len(center) == 2
    assert len(semi_axes) == 3
    # Create X/Y with meshgrid
    x = np.arange(shape[1], dtype=float)
    y = np.arange(shape[0], dtype=float)
    x, y = np.meshgrid(x, y, indexing='xy')
    # Depth image
    x0, y0 = center
    a, b, c = semi_axes
    z = np.empty(shape)
    z[:] = np.nan
    mask_inner = np.logical_and.reduce((
        x >= (x0 - a),
        x <= (x0 + a),
        y >= (y0 - b),
        y <= (y0 + b),
    ))
    # Points below the two diagonals of the pyramid
    mask_below0 = b * (x - x0) <=  a * (y - y0)
    mask_below1 = b * (x - x0) <= -a * (y - y0)
    # Points on triangular shaped faces of pyramid: north, east, south, west
    mask_n = np.logical_and.reduce((mask_inner, ~mask_below0,  mask_below1))
    mask_e = np.logical_and.reduce((mask_inner, ~mask_below0, ~mask_below1))
    mask_s = np.logical_and.reduce((mask_inner,  mask_below0, ~mask_below1))
    mask_w = np.logical_and.reduce((mask_inner,  mask_below0,  mask_below1))
    # Calculate height
    z[mask_n] =  c * (y[mask_n] - y0 + b) / b
    z[mask_e] = -c * (x[mask_e] - x0 - a) / a
    z[mask_s] = -c * (y[mask_s] - y0 - b) / b
    z[mask_w] =  c * (x[mask_w] - x0 + a) / a
    # Gradient fields
    p = np.empty(shape)
    p[:] = np.nan
    p[mask_n] = 0.
    p[mask_e] = -c / a
    p[mask_s] = 0.
    p[mask_w] = c / a
    q = np.empty(shape)
    q[:] = np.nan
    q[mask_n] = c / b
    q[mask_e] = 0.
    q[mask_s] = -c / b
    q[mask_w] = 0.
    return z, (p, q)



def gradients_generate_gaussian(shape, amplitude, mean, cov=(1., 0., 0., 1.)):
    """ Generates depth image and gradients of a Gaussian.

                       X
                    shape[1]
                  semi_axes[0]
                  -----------
                 |           |
         Y       |     Z     |
      shape[0]   |     X     |    Image of Gaussian
    semi_axes[1] |           |
                 |           |
                  -----------

    Based on the probability density function of a multivariate Gaussian of
    dimension k=2 with mean vector mu and covariance matrix sigma

    f(z) = exp(-0.5 * (x-mu)^T * sigma^-1 * (x-mu)) / sqrt((2*pi)^k * det(sigma))

    :param shape: Shape of resulting depth image in pixels, shape (2, )
    :param amplitude: Amplitude (this factor determines heigth in z)
    :param mean: Location of maximum in X/Y, shape (2, )
    :param cov: Covariance matrix, shape (4, )
    :return: depth image (z), gradient (dz/dx, dz/dy)
    """
    assert len(shape) == 2
    assert len(mean) == 2
    assert len(cov) == 4
     # Create X/Y with meshgrid
    x = np.arange(shape[1], dtype=float)
    y = np.arange(shape[0], dtype=float)
    x, y = np.meshgrid(x, y, indexing='xy')
    # Depth image
    x0, y0 = mean
    cov_mat = np.asarray(cov).reshape(2, 2)
    cov_inv = np.linalg.inv(cov_mat)
    z = amplitude * np.exp(-0.5 * ( \
        cov_inv[0, 0]*(x-x0)**2 + cov_inv[0, 1]*(x-x0)*(y-y0) + \
        cov_inv[1, 0]*(x-x0)*(y-y0)  + cov_inv[1, 1]*(y-y0)**2)) / \
        (2 * np.pi * np.sqrt(np.linalg.det(cov_mat)))
    # Gradient fields
    p = -0.5 * (2*cov_inv[0, 0]*(x-x0) + \
                cov_inv[0, 1]*(y-y0) + cov_inv[1, 0]*(y-y0)) * z
    q = -0.5 * (2*cov_inv[1, 1]*(y-y0) + \
                cov_inv[0, 1]*(x-x0) + cov_inv[1, 0]*(x-x0)) * z
    return z, (p, q)



def gradients_visualize(p, q, title=None):
    assert p.ndim == 2
    assert q.ndim == 2
    assert p.shape == q.shape

    fig, axs = plt.subplots(1, 2)
    pos = axs[0].imshow(p)
    cbar = fig.colorbar(pos, ax=axs[0])
    #cbar.set_label('')
    pos = axs[1].imshow(q)
    cbar = fig.colorbar(pos, ax=axs[1])
    #cbar.set_label('')

    if title is not None:
        axs[0].set_title(title + ': dz/dx')
        axs[1].set_title(title + ': dz/dy')
    else:
        axs[0].set_title('dz/dx')
        axs[1].set_title('dz/dy')



def gradients_visualize_orthographic_projection(z, p, q,
    mode='points', size=1.0):
    """ Visualize depth image and gradient field in 3D

    Careful, this does not contain projection with a camera model,
    just an orthographic projection.

    :param z: Depth image z(x, y), shape (m, n)
    :param p: Gradient field dz/dx, shape (m, n)
    :param p: Gradient field dz/dy, shape (m, n)
    :param normal_scale: Scaling factor for normals
    """
    assert z.ndim == 2
    assert z.shape == p.shape
    assert z.shape == q.shape
    mask = np.isfinite(z)
    n = np.sum(mask)
    assert np.all(mask == np.isfinite(p))
    assert np.all(mask == np.isfinite(q))
    # Surface as a point cloud with normals
    x = np.arange(z.shape[1], dtype=float)
    y = np.arange(z.shape[0], dtype=float)
    x, y = np.meshgrid(x, y, indexing='xy')
    points = np.empty((n, 3))
    points[:, 0] = x[mask].ravel()
    points[:, 1] = y[mask].ravel()
    points[:, 2] = z[mask].ravel()
    normals = gradients_to_normalmap(p, q)[mask]
    pcl = pcl_create(points, normals)
    pcl_set_colors(pcl, (1, 1, 1))
    # Coordinate system
    cs = mesh_create_cs(size=5*size)
    if mode == 'points':
        geometries_visualize([cs, pcl])
    elif mode == 'normals':
        # Normals as a line_set
        normal_points = np.vstack((
            points,
            points + size * normals
        ))
        normal_indices = np.vstack((
            np.arange(n),
            np.arange(n) + n
        )).T
        line_set = lineset_create(normal_points, normal_indices)
        # Visualize
        geometries_visualize([cs, pcl, line_set])
    elif mode == 'surface':
        mesh, _ = o3d.geometry.TriangleMesh.\
            create_from_point_cloud_poisson(pcl.to_legacy(), depth=9)
        geometries_visualize([cs, mesh])



def gradients_to_normalmap(p, q):
    assert p.ndim == 2
    assert q.ndim == 2
    assert p.shape == q.shape
    scales = np.sqrt(p**2 + q**2 + 1)
    # We divide by scales, so if scales is close to zero, the normal is invalid
    mask = ~np.isclose(scales, 0.0)
    nmap = np.empty((*p.shape, 3))
    nmap[:] = np.nan
    nmap[mask, 0] = -p[mask] / scales[mask]
    nmap[mask, 1] = -q[mask] / scales[mask]
    nmap[mask, 2] = 1.       / scales[mask]
    return nmap



def normalmap_to_gradients(nmap):
    assert nmap.ndim == 3
    assert nmap.shape[2] == 3
    # If z-direction of normal vector is 0, the gradient is not valid
    mask = ~np.isclose(nmap[:, :, 2], 0.0)
    p = np.empty(nmap.shape[0:2])
    p[:] = np.nan
    p[mask] = -nmap[mask, 0] / nmap[mask, 2]
    q = np.empty(nmap.shape[0:2])
    q[:] = np.nan
    q[mask] = -nmap[mask, 1] / nmap[mask, 2]
    return p, q



def normalmap_load(filename, normalize=True):
    """ Load normal map from image file

    Image file type is any image type supported by cv2.imwrite,
    recommended is e.g. png.

    If image contains pixels with all-zero color channels, these
    are interpreted as invalid normals and set to (NaN, NaN, NaN).

    :param filename: Image file name to read
    :param normalize: Re-normalize normal vectors after loading
    :return: Normal map, shape (height, width, 3)
    """
    # Load image
    img = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
    if img is None:
        raise Exception(f'Error reading normal map from {filename}')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # Invalid normals are where all color channels are zero
    mask_valid = np.any(img > 0, axis=2)
    # Create normal map
    nmap = np.empty(img.shape)
    nmap[:] = np.nan
    if img.dtype is np.dtype(np.uint8):
        # Input [0..255], output [-1..1]
        nmap[mask_valid] = ((img[mask_valid].astype(float) * 2.) / 255.) - 1.
    elif img.dtype is np.dtype(np.uint16):
        # Input [0..65535], output [-1..1]
        nmap[mask_valid] = ((img[mask_valid].astype(float) * 2.) / 65535.) - 1.
    else:
        raise Exception('Unknown channel width loading normals')
    if normalize:
        nlen = np.sqrt(np.sum(nmap[mask_valid]**2, axis=1))
        nmap[mask_valid] /= nlen[:, np.newaxis]
    return nmap



def normalmap_save(filename, nmap, dtype=np.uint16):
    """ Save normal map to image file

    Image file type is any image type supported by cv2.imwrite,
    recommended is e.g. png.

    If normal map contains normals with any component NaN, these
    are interpreted as invalid normals and set to (0, 0, 0).

    :param filename: Image file name to write
    :param nmap: Normal map, shape (height, width, 3)
    :param dtype: Data type for single color channel
    """
    assert nmap.ndim == 3
    assert nmap.shape[2] == 3
    # Invalid normals are NaN
    mask_valid = np.all(np.isfinite(nmap), axis=2)
    # Create image
    img = np.zeros(nmap.shape, dtype)
    if dtype is np.uint8:
        # Input [0..1], output [0..255]
        img[mask_valid] = np.round(((nmap[mask_valid] + 1.) * 255.) / 2.).astype(dtype)
    elif dtype is np.uint16:
        # Input [0..1], output [0..65535]
        img[mask_valid] = np.round(((nmap[mask_valid] + 1.) * 65535.) / 2.).astype(dtype)
    else:
        raise Exception('Unknown channel width writing normals')
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    retval = cv2.imwrite(filename, img)
    if not retval:
        raise Exception(f'Error writing normal map to {filename}')



def normalmap_visualize(nmap, ax=None):
    """ Visualize normal map

    https://en.wikipedia.org/wiki/Normal_mapping#Calculation

    :param nmap: Normal map, shape (height, width, 3)
    :param ax: Matplotlib axis object to draw to
    :return: Matplotlib axis object
    """
    assert nmap.ndim == 3
    assert nmap.shape[2] == 3
    mask_valid = np.all(np.isfinite(nmap), axis=2)
    img = np.zeros(nmap.shape, np.uint8)
    img[mask_valid] = np.round(((nmap[mask_valid, :] + 1.) * 255.) / 2.).astype(np.uint8)
    if ax is None:
        _, ax = plt.subplots()
        ax.set_title('normal map')
    ax.imshow(img)
    return ax



def slant_tilt_to_normalmap(slant, tilt):
    """
    :param slant: Slant in rad
    :param tilt: Tilt in rad
    """
    assert slant.ndim == 2
    assert tilt.ndim == 2
    assert slant.shape == tilt.shape
    nmap = np.empty((*slant.shape, 3))
    nmap[:, :, 0] = np.sin(slant) * np.cos(tilt)
    nmap[:, :, 1] = np.sin(slant) * np.sin(tilt)
    nmap[:, :, 2] = np.cos(slant)
    return nmap



def normalmap_to_slant_tilt(nmap):
    assert nmap.ndim == 3
    assert nmap.shape[2] == 3
    slant = np.arccos(nmap[:, :, 2]) # [0, pi]
    tilt = np.arctan2(nmap[:, :, 1], nmap[:, :, 0]) # [-pi, pi]
    return slant, tilt



def slant_tilt_visualize(slant, tilt):
    fig, axs = plt.subplots(1, 2)
    pos = axs[0].imshow(np.rad2deg(slant))
    cbar = fig.colorbar(pos, ax=axs[0])
    cbar.set_label('Angle (deg)')
    pos = axs[1].imshow(np.rad2deg(tilt))
    cbar = fig.colorbar(pos, ax=axs[1])
    cbar.set_label('Angle (deg)')
    axs[0].set_title('slant')
    axs[1].set_title('tilt')



def gradients_estimate_simple(z):
    """ Estimate gradient field from depth image using neighbors
    This is a discrete implementation instead of using np.gradient.
    """
    p = np.zeros(z.shape)
    # forward difference: left column
    p[:, 0] = z[:, 1] - z[:, 0]
    # central difference
    p[:, 1:-1] = (z[:, 2:] - z[:, :-2]) / 2.
    # backward difference: right column
    p[:, -1] = z[:, -1] - z[:, -2]

    q = np.zeros(z.shape)
    # forward difference: top row
    q[0, :] = z[1, :] - z[0, :]
    # central difference
    q[1:-1, :] = (z[2:, :] - z[:-2, :]) / 2.
    # backward difference: bottom row
    q[-1, :] = z[-1, :] - z[-2, :]

    return p, q



def gradients_estimate(z):
    """ Estimate gradient field from depth image using neighbors
    """
    p = np.gradient(z, axis=1)
    q = np.gradient(z, axis=0)
    return p, q



def _pad_grad_fields(p, q):
    p_pad = np.vstack((
        np.hstack((p,          -p[:, ::-1])),
        np.hstack((p[::-1, :], -p[::-1, ::-1]))
    ))
    q_pad = np.vstack((
        np.hstack((q,            q[:, ::-1])),
        np.hstack((-q[::-1, :], -q[::-1, ::-1]))
    ))
    return p_pad, q_pad

def _one_forth_of_array(a):
    return a[:a.shape[0]//2, :a.shape[1]//2]

def gradients_integrate_frankot(p, q):
    """ Integrates gradient field in order to reconstruct the depth image

    Method from:
    R. T. Frankot and R. Chellappa, "A method for enforcing integrability
    in shape from shading algorithms," in IEEE Transactions on Pattern
    Analysis and Machine Intelligence, vol. 10, no. 4, pp. 439-451,
    July 1988, doi: 10.1109/34.3909.
    https://webdocs.cs.ualberta.ca/~vis/courses/CompVis/readings/photometric/FrankotIntegrpami88.pdf

    :param p: Gradient field dz/dx, shape (m, n)
    :param p: Gradient field dz/dy, shape (m, n)
    :return: Depth image z, shape (m, n)
    """
    assert p.ndim == 2
    assert q.ndim == 2
    assert p.shape == q.shape
    p_pad, q_pad = _pad_grad_fields(p, q)
    shape = p_pad.shape
    wx, wy = np.meshgrid(2. * np.pi * np.fft.fftfreq(shape[1]),
                         2. * np.pi * np.fft.fftfreq(shape[0]), indexing='xy')
    numerator = -1j * wx * np.fft.fft2(p_pad) - \
                 1j * wy * np.fft.fft2(q_pad)
    denominator = wx**2 + wy**2
    denominator += np.finfo(float).eps # Avoid division by zero
    z = np.fft.ifft2(numerator / denominator).real
    z -= np.mean(z)
    return _one_forth_of_array(z.real)
