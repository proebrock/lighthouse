import glob

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
import open3d as o3d
import cv2

from trafolib.trafo3d import Trafo3d
from common.image_utils import image_load



def pcl_create(point_positions, point_normals=None,
    point_colors=None):
    dtype = o3d.core.float32
    device = o3d.core.Device('CPU:0')
    pcl = o3d.t.geometry.PointCloud(device)
    pcl.point.positions = o3d.core.Tensor(point_positions, dtype, device)
    if point_normals is not None:
        pcl.point.normals = o3d.core.Tensor(point_normals, dtype, device)
    if point_colors is not None:
        pcl.point.colors = o3d.core.Tensor(point_colors, dtype, device)
    return pcl



def pcl_create_empty():
    return pcl_create(point_positions=np.zeros((0, 3)))



def pcl_translate(pcl, dt):
    pcl.translate(dt)



def pcl_transform(pcl, trafo):
    pcl.transform(trafo.get_homogeneous_matrix())



def pcl_load(filename):
    pcl = o3d.t.io.read_point_cloud(filename)
    if pcl.point.positions.numpy().size == 0:
        raise Exception(f'Error reading point cloud {filename}')
    return pcl



def pcl_load_multiple(filenames_or_pattern):
    if isinstance(filenames_or_pattern, str):
        # Pattern
        filenames = sorted(glob.glob(filenames_or_pattern))
        if len(filenames) == 0:
            raise Exception(f'No filenames found under {filenames_or_pattern}')
    else:
        # List of files
        filenames = filenames_or_pattern
    pcls = []
    for filename in filenames:
        pcl = pcl_load(filename)
        pcls.append(pcl)
    return pcls



def pcl_save(filename, pcl):
    retval = o3d.t.io.write_point_cloud(filename, pcl)
    if not retval:
        raise Exception(f'Error writing point cloud {filename}')



def pcl_set_colors(pcl, colors=(0, 0, 0)):
    num_points = pcl.point.positions.numpy().shape[0]
    colors = np.asarray(colors)
    if colors.ndim == 1:
        assert colors.size == 3
        pcl.paint_uniform_color(np.array(colors, dtype=np.float32))
    elif colors.ndim == 2:
        assert colors.shape[0] == num_points
        assert colors.shape[1] == 3
        dtype = o3d.core.float32
        device = o3d.core.Device('CPU:0')
        pcl.point.colors = o3d.core.Tensor(colors, dtype, device)
    else:
        raise Exception('Invalid color format')



def pcl_colorize_by_scalar(pcl, values, cmap='viridis',
        min_max=None, nan_color=(1, 0, 0)):
    assert values.ndim == 1
    num_points = pcl.point.positions.numpy().shape[0]
    assert num_points == values.size
    cm = plt.get_cmap(cmap)
    isvalid = ~np.isnan(values)
    if min_max is None:
        min_max = (np.min(values[isvalid]), np.max(values[isvalid]))
    values_norm = np.clip((values[isvalid] - min_max[0]) /
        (min_max[1] - min_max[0]), 0, 1)
    colors = np.empty((num_points, 3))
    colors[isvalid, :] = cm(values_norm)[:, 0:3]
    colors[~isvalid, :] = nan_color
    pcl.point.colors = o3d.core.Tensor(colors)



def mesh_create(vertex_positions, triangle_indices,
        vertex_normals=None, triangle_normals=None,
        vertex_colors=None, triangle_colors=None):
    device = o3d.core.Device('CPU:0')
    dtype_f = o3d.core.float32
    dtype_i = o3d.core.int32
    mesh = o3d.t.geometry.TriangleMesh(device)
    mesh.vertex.positions = o3d.core.Tensor(vertex_positions, dtype_f, device)
    mesh.triangle.indices = o3d.core.Tensor(triangle_indices, dtype_i, device)
    if vertex_normals is not None:
        mesh.vertex.normals = o3d.core.Tensor(vertex_normals, dtype_f, device)
    if triangle_normals is not None:
        mesh.triangle.normals = o3d.core.Tensor(triangle_normals, dtype_f, device)
    if vertex_colors is not None:
        mesh.vertex.colors = o3d.core.Tensor(vertex_colors, dtype_f, device)
    if triangle_colors is not None:
        mesh.triangle.colors = o3d.core.Tensor(triangle_colors, dtype_f, device)
    return mesh



def mesh_create_empty():
    return mesh_create(
        vertex_positions=np.zeros((0, 3)),
        triangle_indices=np.zeros((0, 3))
        )



def mesh_create_triangle(vertex_positions=None,
    vertex_normals=None, triangle_normals=None,
    vertex_colors=None, triangle_colors=None):
    # Triangle indices
    triangle_indices = np.array((
        (0, 1, 2),
    ), dtype=int)
    # Vertex normals
    if vertex_normals is not None:
        vertex_normals_norm = np.array(vertex_normals) / \
            np.linalg.norm(vertex_normals, axis=1)[:, np.newaxis]
    else:
        vertex_normals_norm = None
    # Triangle normals
    if triangle_normals is not None:
        triangle_normals_norm = np.array(triangle_normals) / \
            np.linalg.norm(triangle_normals, axis=1)[:, np.newaxis]
    else:
        triangle_normals_norm = None
    # Create triangle
    triangle = mesh_create(
        vertex_positions=vertex_positions,
        triangle_indices=triangle_indices,
        vertex_normals=vertex_normals_norm,
        triangle_normals=triangle_normals_norm,
        vertex_colors=vertex_colors,
        triangle_colors=triangle_colors)
    return triangle



def mesh_create_quad(shape, color=(0, 0, 0)):
    """ Generate quad

    The quad is represented by two triangles

    Y
       /    Quad
       |
  s[1] |----------
       |       / |
       |    /    |
       | /       |
       .------------>
    Z            s[0]   X

    """
    vertex_positions = np.array([
        [ 0,        0,        0],
        [ shape[0], 0,        0],
        [ shape[0], shape[1], 0],
        [ 0,        shape[1], 0],
    ])
    triangle_indices = np.array([
        [0, 1, 3],
        [2, 3, 1],
    ], dtype=int)
    vertex_normals = np.array([
        [ 0, 0, 1],
        [ 0, 0, 1],
        [ 0, 0, 1],
        [ 0, 0, 1],
    ])
    triangle_normals = np.array([
        [ 0, 0, 1],
        [ 0, 0, 1],
    ])
    vertex_colors = np.array([
        color,
        color,
        color,
        color,
    ])
    triangle_colors = np.array([
        color,
        color,
    ])
    return mesh_create(vertex_positions, triangle_indices,
        vertex_normals, triangle_normals,
        vertex_colors, triangle_colors)



def mesh_create_cs(pose=None, size=1.0):
    cs = o3d.t.geometry.TriangleMesh.create_coordinate_frame(size=size)
    if pose is not None:
        cs.transform(pose.get_homogeneous_matrix())
    cs.compute_vertex_normals()
    return cs



def mesh_create_sphere(radius=1.0, resolution=20):
    sphere = o3d.t.geometry.TriangleMesh.create_sphere(radius, resolution)
    sphere.compute_vertex_normals()
    return sphere



def mesh_create_box():
    box = o3d.t.geometry.TriangleMesh.create_box()
    box.compute_vertex_normals()
    return box



def mesh_create_image(img, pixel_size=1.0):
    """ Convert raster image into mesh

    Each pixel of the image is encoded as four vertices and two triangles
    that creates a square region in the mesh of size pixel_size x pixel_size.
    This creates duplicate vertices, but we need to use the vertex colors
    to encode the colors, hence 4 unique vertices for each pixel.
    The image is placed in the X/Y plane.

    Z                  X
       X------------->
       |           |
       |  Image    |
       |           |
       |------------
       |
    Y  V

    :param img: Input image
    :param pixel_size: Size of one pixel in millimeter; can be single value for square
        pixels or a tupel of two values, first pixel size in x, second pixel size in y;
        unit is millimeters per pixel
    """
    if isinstance(pixel_size, float):
        pixel_sizes = np.array((pixel_size, pixel_size))
    else:
        pixel_sizes = np.asarray(pixel_size)
        assert pixel_sizes.size == 2
    vertex_positions = np.zeros((4 * img.shape[0] * img.shape[1], 3))
    triangle_indices = np.zeros((2 * img.shape[0] * img.shape[1], 3), dtype=int)
    vertex_normals = np.zeros((4 * img.shape[0] * img.shape[1], 3))
    vertex_normals[:, 2] = -1.0
    triangle_normals = np.zeros((2 * img.shape[0] * img.shape[1], 3))
    triangle_normals[:, 2] = -1.0
    vertex_colors = np.zeros((4 * img.shape[0] * img.shape[1], 3))
    for r in range(img.shape[0]):
        for c in range(img.shape[1]):
            i = 4 * (r * img.shape[1] + c)
            vertex_positions[i, 0:2]   = pixel_sizes[0] * c,     pixel_sizes[1] * r
            vertex_positions[i+1, 0:2] = pixel_sizes[0] * c,     pixel_sizes[1] * (r+1)
            vertex_positions[i+2, 0:2] = pixel_sizes[0] * (c+1), pixel_sizes[1] * r
            vertex_positions[i+3, 0:2] = pixel_sizes[0] * (c+1), pixel_sizes[1] * (r+1)
            vertex_colors[i:i+4, :] = img[r, c, :] / 255.0
            j = 2 * (r * img.shape[1] + c)
            triangle_indices[j, :]   = i, i+1,   i+3
            triangle_indices[j+1, :] = i+3, i+2, i
    return mesh_create(vertex_positions, triangle_indices,
        vertex_normals, triangle_normals,
        vertex_colors)



def mesh_create_image_from_file(filename, pixel_size=1.0, scale=1.0):
    """ Load image file from disk and generate mesh from it
    :param filename: Filename (and path) of image file
    :param pixel_size: Size of one pixel in millimeter; can be single value for square
        pixels or a tupel of two value, first pixel size in x, second pixel size in y;
        unit is millimeters per pixel
    :param scale: Scale image with this factor before converting into mesh
    """
    img = image_load(filename)
    img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_AREA)
    return mesh_create_image(img, pixel_size)



def mesh_create_surface_from_function(fun, xrange, yrange, num, scale):
    """ Use 3D function to generate a patch of 3D surface

    Y
       /
       |
       |------------
       |           |
       |  Surface  |
       |           |
       .------------->
    Z                  X

    xrange and yrange determine the domain the fun is evalued in.
    The resulting surface is scaled in X and Y to [0, 1] and then
    finally scaled with scale.

    :param fun: Callable function, input X, Y in shape (n, 2),
        output Z in shape (n, )
    :param xrange: Real range in X: (xmin, xmax)
    :param yrange: Real range in Y: (ymin, ymax)
    :param num: Number of pixels (num_x, num_y)
    :param scale: Scale to final size (xscale, yscale, zscale)
    """
    # Generate vertex positions
    x = np.linspace(xrange[0], xrange[1], num[0])
    y = np.linspace(yrange[0], yrange[1], num[1])
    x, y = np.meshgrid(x, y, indexing='ij')
    vertex_positions = np.zeros((num[0] * num[1], 3))
    vertex_positions[:, 0] = x.ravel()
    vertex_positions[:, 1] = y.ravel()
    vertex_positions[:, 2] = scale[2] * fun(vertex_positions[:, 0:2])
    vertex_positions[:, 0] = (scale[0] * (vertex_positions[:, 0] - xrange[0])) / (xrange[1] - xrange[0])
    vertex_positions[:, 1] = (scale[1] * (vertex_positions[:, 1] - yrange[0])) / (yrange[1] - yrange[0])
    # Generate triangle indices
    triangle_indices = np.zeros((2 * (num[0] - 1) * (num[1] - 1), 3), dtype=int)
    tindex = 0
    for iy in range(num[1] - 1):
        for ix in range(num[0] - 1):
            i = iy * num[0] + ix
            triangle_indices[tindex, :] = i+1, i, i+num[0]
            tindex += 1
            triangle_indices[tindex, :] = i+1, i+num[0], i+num[0]+1
            tindex += 1
    mesh = mesh_create(
        vertex_positions=vertex_positions,
        triangle_indices=triangle_indices,
    )
    # Calculate normals
    mesh.compute_triangle_normals()
    mesh.compute_vertex_normals()
    return mesh



def mesh_set_vertex_colors(mesh, colors=(0, 0, 0)):
    num_vertices = mesh.vertex.positions.numpy().shape[0]
    colors = np.asarray(colors, dtype=np.float32)
    if colors.ndim == 1:
        assert colors.size == 3
        colors = np.tile(colors, (num_vertices, 1))
        mesh.vertex.colors = o3d.core.Tensor(colors)
    elif colors.ndim == 2:
        assert colors.shape[0] == num_vertices
        assert colors.shape[1] == 3
        mesh.vertex.colors = o3d.core.Tensor(colors)
    else:
        raise Exception('Invalid color format')



def mesh_set_triangle_colors(mesh, color=(0, 0, 0)):
    num_triangles = mesh.triangle.indices.numpy().shape[0]
    colors = np.asarray(colors, dtype=np.float32)
    if colors.ndim == 1:
        assert colors.size == 3
        colors = np.tile(colors, (num_triangles, 1))
        mesh.triangle.colors = o3d.core.Tensor(colors)
    elif colors.ndim == 2:
        assert colors.shape[0] == num_triangles
        assert colors.shape[1] == 3
        mesh.triangle.colors = o3d.core.Tensor(colors)
    else:
        raise Exception('Invalid color format')



def mesh_translate(mesh, dt):
    mesh.translate(dt)



def mesh_transform(mesh, trafo):
    mesh.transform(trafo.get_homogeneous_matrix())



def mesh_load(filename):
    mesh = o3d.t.io.read_triangle_mesh(filename)
    if mesh.vertex.positions.numpy().size == 0:
        raise Exception(f'Error reading mesh {filename}')
    return mesh



def mesh_load_multiple(filenames_or_pattern):
    if isinstance(filenames_or_pattern, str):
        # Pattern
        filenames = sorted(glob.glob(filenames_or_pattern))
        if len(filenames) == 0:
            raise Exception(f'No filenames found under {filenames_or_pattern}')
    else:
        # List of files
        filenames = filenames_or_pattern
    meshes = []
    for filename in filenames:
        mesh = mesh_load(filename)
        meshes.append(mesh)
    return meshes



def mesh_save(filename, mesh):
    retval = o3d.t.io.write_triangle_mesh(filename, mesh)
    if not retval:
        raise Exception(f'Error writing mesh {filename}')



def mesh_pcl_signed_distances(mesh, pcl):
    """ Calculate signed distances between a point cloud and a mesh
    The distances are the signed orthogonal distances between every point
    in the point cloud and the mesh. The sign of the distances is
    determined by normal orientation.
    :param mesh: Mesh
    :param pcl: Point cloud
    :return: Signed distances, one for each point in pcl
    """
    scene = o3d.t.geometry.RaycastingScene()
    scene.add_triangles(mesh)
    signed_distances = scene.compute_signed_distance(pcl.point.positions)
    return signed_distances.numpy()



def mesh_pcl_icp_objfun(x, scene, points):
    """ Objective function for mesh_pcl_icp
    Decision variable is 6d: translation (3d) and rotation as rodrigues
    vector (3d), with some weighting.
    """
    T = Trafo3d(t=x[:3], rodr=x[3:]/100.)
    tpoints = o3d.core.Tensor(T * points, dtype=o3d.core.float32)
    signed_distances = scene.compute_signed_distance(tpoints)
    return signed_distances.numpy()



def mesh_pcl_icp(mesh, pcl, T_init=Trafo3d()):
    """ Estimates trafo that minimizes distances of pcl to mesh
    Estimates a 6d transformation that - applied to a point cloud - minimizes
    the distances of the points in the point cloud to the mesh.

    Resulting transformation is the one from the initial point cloud to the
    final point cloud, resulting distances are the final signed distances
    between the points of the point cloud and the mesh AFTER optimization
    (residual signed distances).

    This function can only estimate small transformations between point
    cloud and mesh ("local registration").

    For large meshes and point clouds the runtime might be an issue
    (the jacobian of the numerical optimation has the size 6*n for n points
    in the point cloud). One attempt to improve runtime is to random-sample
    the point cloud and estimate the transformation with the down-sampled
    point cloud.

    :param mesh: Mesh
    :param pcl: Point cloud
    :param T_init: Estimate for trafo pcl to pcl_final (if available)
    :return: Transformation, residual signed distances
    """
    T0 = T_init.inverse()
    x0 = np.concatenate((T0.get_translation(), \
                         T0.get_rotation_rodrigues()*100.))
    scene = o3d.t.geometry.RaycastingScene()
    scene.add_triangles(mesh)
    points = pcl.point.positions.numpy()
    args = (scene, points)
    res = least_squares(mesh_pcl_icp_objfun, x0, loss='soft_l1', args=args)
    if not res.success:
        raise Exception(f'Optimization failed: {res}')
    T = Trafo3d(t=res.x[:3], rodr=res.x[3:]/100.).inverse()
    signed_distances = mesh_pcl_icp_objfun(res.x, *args)
    return T, signed_distances



def lineset_create(point_positions, line_indices, line_colors=None):
    device = o3d.core.Device('CPU:0')
    dtype_f = o3d.core.float32
    dtype_i = o3d.core.int32
    line_set = o3d.t.geometry.LineSet(device)
    line_set.point.positions = o3d.core.Tensor(point_positions, dtype_f, device)
    line_set.line.indices = o3d.core.Tensor(line_indices, dtype_i, device)
    if line_colors is not None:
        line_set.line.colors = o3d.core.Tensor(line_colors, dtype_f, device)
    return line_set



def lineset_set_line_colors(line_set, colors=(0, 0, 0)):
    num_lines = line_set.line.indices.numpy().shape[0]
    colors = np.asarray(colors)
    if colors.ndim == 1:
        assert colors.size == 3
        colors = np.tile(colors, (num_lines, 1))
        line_set.line.colors = o3d.core.Tensor(colors)
    elif colors.ndim == 2:
        assert colors.shape[0] == num_lines
        assert colors.shape[1] == 3
        line_set.point.colors = o3d.core.Tensor(colors)
    else:
        raise Exception('Invalid color format')



def geometries_visualize(geo_list, size=None):
    geometries = geo_list.copy()
    if size is not None:
        cs = o3d.t.geometry.TriangleMesh.create_coordinate_frame(size)
        geometries.append(cs)
    o3d.visualization.draw(geometries)
