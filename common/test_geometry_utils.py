import numpy as np
import matplotlib.pyplot as plt
import open3d as o3d

from trafolib.trafo3d import Trafo3d
from common.geometry_utils import pcl_create, pcl_set_colors, mesh_create_quad, \
    mesh_create_image, mesh_create_cs, mesh_pcl_signed_distances, \
    mesh_pcl_icp, geometries_visualize



def test_mesh_generate_image_coordinates():
    # Generate image
    h = 60
    w = 80
    m = 5
    image = np.zeros((h, w, 3), dtype=np.uint8) # All black
    image[0:m, 0:m, :] = (255, 255, 255) # Top-left:   white
    image[0:m, w-m:w, :] = (255, 0, 0) # Top-right:    red
    image[h-m:h, 0:m, :] = (0, 255, 0) # Bottom-left:  green
    image[h-m:h, w-m:w, :] = (0, 0, 255) # Bottom-right: blue
    if False:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(image)
        plt.show()
    # Generate mesh from image
    pixel_size = 0.1
    mesh = mesh_create_image(image, pixel_size=pixel_size)
    if False:
        cs = mesh_create_cs(size=6.)
        geometries_visualize([cs, mesh])
    # Check color of mesh at certain coordinates
    ptree = o3d.geometry.KDTreeFlann(mesh.to_legacy())
    colors = mesh.vertex.colors.numpy()
    points = pixel_size * np.array((
        (w/2, h/2, 0), # Middle
        (0, 0, 0),     # Top-left
        (w, 0, 0),     # Top-right
        (0, h, 0),     # Bottom-left
        (w, h, 0),     # Bottom-right
    ))
    expected_colors = np.array((
        (0, 0, 0), # Black
        (1, 1, 1), # White
        (1, 0, 0), # Red
        (0, 1, 0), # Green
        (0, 0, 1), # Blue
    ))
    for p, ec in zip(points, expected_colors):
        [k, idx, _] = ptree.search_knn_vector_3d(p, 1)
        assert k ==  1
        assert np.all(colors[idx, :] == ec) # White



def test_mesh_pcl_signed_distances():
    #                         X     Y
    mesh = mesh_create_quad((200., 100.))
    pcl = pcl_create(np.array((
        # X     Y    Z
        (100., 10.,   0.), # On mesh
        (100., 30.,   7.), # Above mesh (relative to normal)
        (110., 20.,  -9.), # Below mesh (relative to normal)
        (230., 50.,   0.), # In plane of mesh, but outside
        (230., 50.,  40.), # In plane of mesh and above
        (230., 50., -60.), # In plane of mesh and below
    )))
    expected_distances = np.array((
        0., 7., -9., 30.,
        # Once we are outside the area of the mesh, distances are all positive
        np.sqrt(30.**2+40**2), np.sqrt(30.**2+60**2)
    ))
    distances = mesh_pcl_signed_distances(mesh, pcl)
    assert np.allclose(distances, expected_distances)



def test_mesh_pcl_icp():
    # Create mesh
    mesh = mesh_create_quad((200., 100.))
    # Create points on mesh
    x = np.linspace(0., 200, 51)
    y = np.linspace(0., 100, 31)
    xx, yy = np.meshgrid(x, y)
    points = np.zeros((xx.size, 3))
    points[:, 0] = xx.ravel()
    points[:, 1] = yy.ravel()
    # Transform points
    T = Trafo3d(t=(5, -10, 3), rpy=np.deg2rad((-2., 3, -1)))
    pcl = pcl_create(T * points)
    # Run ICP
    T_est, signed_distances = mesh_pcl_icp(mesh, pcl)
    # Test results
    dt, dr = T.distance(T_est)
    assert dt < 1e-3
    assert dr < 1e-3
    assert np.all(np.abs(signed_distances) < 1e-3)
    # Visualize final result
    if False:
        cs = mesh_create_cs(size=100.)
        tpcl = pcl_create(T_est.inverse() * pcl.point.positions.numpy())
        pcl_set_colors(pcl, (1., 0., 0.)) # Red: original point cloud
        pcl_set_colors(tpcl, (0., 1., 0.)) # Green: final
        geometries_visualize([cs, mesh, pcl, tpcl])
