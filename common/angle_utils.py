import numpy as np



def angle_diff(a, b):
    """ Difference between two angles
    :param a: First angle
    :param b: Second angle
    :return: Angle difference
    """
    delta = np.abs(a - b)
    return np.min((delta, 2 * np.pi - delta))



def angle_mean(angles):
    """ Mean of multiple angles
    :param angles: Input angles
    :return: Mean angle
    """
    cossum = np.sum(np.cos(angles))
    sinsum = np.sum(np.sin(angles))
    return np.arctan2(sinsum, cossum)
