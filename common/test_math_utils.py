import numpy as np

from common.math_utils import hr_int, angle_diff, angle_mean



def test_hr_int():
    assert hr_int(0) == '0'
    assert hr_int(999) == '999'
    assert hr_int(-999) == '-999'
    assert hr_int(1000) == '1.0k'
    assert hr_int(-1000) == '-1.0k'
    assert hr_int(110592) == '110.6k'
    assert hr_int(7077888) == '7.1M'
    assert hr_int(452984832) == '453.0M'
    assert hr_int(-28991029248) == '-29.0G'
    assert hr_int(1855425871872) == '1.9T'



def test_angle_diff():
    angle = angle_diff(*np.deg2rad((10., 20.)))
    assert np.isclose(np.rad2deg(angle), 10.)
    angle = angle_diff(*np.deg2rad((5., 355.)))
    assert np.isclose(np.rad2deg(angle), 10.)
    angle = angle_diff(*np.deg2rad((-175., 175.)))
    assert np.isclose(np.rad2deg(angle), 10.)



def test_angle_mean():
    angle = angle_mean(np.deg2rad((-10., 10.)))
    assert np.isclose(np.rad2deg(angle), 0.)
    angle = angle_mean(np.deg2rad((30, 60.)))
    assert np.isclose(np.rad2deg(angle), 45.)
    angle = angle_mean(np.deg2rad((-30, -60.)))
    assert np.isclose(np.rad2deg(angle), -45.)
    angle = angle_mean(np.deg2rad((-10., 340.)))
    assert np.isclose(np.rad2deg(angle), -15.)
    angle = angle_mean(np.deg2rad((-10., -20.)))
    assert np.isclose(np.rad2deg(angle), -15.)
    angle = angle_mean(np.deg2rad((-170., 160.)))
    assert np.isclose(np.rad2deg(angle), 175.)
