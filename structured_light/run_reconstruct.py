import os
import sys
import time
import glob

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../'))
from camsimlib.image_mapping import image_indices_to_points, \
    image_sample_points_bilinear
from camsimlib.camera_model import CameraModel
from camsimlib.light_projector import LightProjector
from common.image_utils import image_load
from common.geometry_utils import pcl_save, pcl_create, geometries_visualize
from common.pixel_matcher import ImageMatcher
from common.bundle_adjust import bundle_adjust_points



def cam_get_match_points(matches, projector, cam):
    """ Get matching projector and camera points
    Matches contain a matching projector pixel for each camera pixel,
    for some camera pixel there are no matches
    This function creates projector and camera points that
    match each other as well as a validity mask in the
    shape of the camera chip
    """
    # Projector indices, shape (n, 2)
    # these are the target pixels of the matching;
    # pixel matcher gives indices, not points
    pindices = matches.reshape((-1, 2))
    ppoints = image_indices_to_points(pindices)
    valid_mask = projector.points_on_chip_mask(ppoints)
    ppoints = ppoints[valid_mask]
    # Camera points, shape (n, 2)
    # These are the source pixels of the matching
    rows = np.arange(cam.get_chip_size()[1])
    cols = np.arange(cam.get_chip_size()[0])
    rows, cols = np.meshgrid(rows, cols, indexing='ij')
    cindices = np.vstack((rows.flatten(), cols.flatten())).T
    cpoints = image_indices_to_points(cindices)
    cpoints = cpoints[valid_mask]
    # The i-th index of cpoints maps the i-th index of ppoints
    assert ppoints.shape == cpoints.shape
    return ppoints, cpoints, valid_mask.reshape((cam.get_chip_size()[[1, 0]]))



def cluster_points(match_points):
    """ Cluster matching points
    Reverse matches: Dictionary
    *Key* of dictionary is tuple with indices of projector
    rounded to integer.
    *Value* of dictionary is list of lists for projector (index 0)
    and each camera. Each of these lists contain 2D points of that
    projector or camera that match to the same projector pixel.
    """
    num_cams = len(match_points)
    reverse_matches = {}
    for cam_no, (ppoints, cpoints, valid_mask) in enumerate(match_points):
        pindices = np.round(ppoints).astype(int)
        for pi, pp, cp in zip(pindices, ppoints, cpoints):
            _pi = tuple(pi.tolist())
            _pp = tuple(pp.tolist())
            _cp = tuple(cp.tolist())
            if _pi not in reverse_matches:
                new_entry = [ [] for _ in range(num_cams + 1) ]
                reverse_matches[_pi] = new_entry
            reverse_matches[_pi][0].append(_pp)
            reverse_matches[_pi][cam_no + 1].append(_cp)
    # Clustering is done, so omit key, from here we just need the values
    return list(reverse_matches.values())



def create_bundle_adjust_points(reverse_matches):
    """ Join clusters to form points that can be used for bundle adjustment
    """
    num_points = len(reverse_matches)
    num_cams = len(reverse_matches[0])
    points = np.zeros((num_points, num_cams, 2))
    points[:] = np.nan
    for point_no in range(num_points):
        for cam_no in range(num_cams):
            p = np.array(reverse_matches[point_no][cam_no])
            if p.size == 0:
                continue
            center = np.median(p, axis=0)
            points[point_no, cam_no, :] = center
    return points



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'structured_light')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    # Load configuration
    filename = os.path.join(data_dir, 'projector.json')
    projector = LightProjector()
    projector.json_load(filename)
    cam_filenames = sorted(glob.glob(os.path.join(data_dir, 'cam????.json')))
    cams = []
    for i, filename in enumerate(cam_filenames):
        cam = CameraModel()
        cam.json_load(filename)
        cams.append(cam)
    filename = os.path.join(data_dir, 'matcher.json')
    matcher = ImageMatcher()
    matcher.json_load(filename)

    # Load "white" images for later color reconstruction
    white_images = []
    for cam_no in range(len(cams)):
        filename = os.path.join(data_dir, f'pattern0001_cam{cam_no:04}.png')
        white_images.append(image_load(filename))

    # Load matches
    filename = os.path.join(data_dir, 'matches.npz')
    print('Loading matches ...')
    npz = np.load(filename)
    matches = []
    for cam_no in range(len(cams)):
        matches.append(npz[f'arr_{cam_no}'])
    print('    Done.')

    print('Reverse matching ...')
    match_points = []
    for cam_no in range(len(cams)):
        ppoints, cpoints, valid_mask = cam_get_match_points( \
            matches[cam_no], projector, cams[cam_no])
        match_points.append([ppoints, cpoints, valid_mask])
    print('Clustering matches ...')
    reverse_matches = cluster_points(match_points)
    print('Creating bundle adjust points ...')
    p = create_bundle_adjust_points(reverse_matches)

    # Reduce points to those visible by at least n projective geometries
    min_num_projective_geometries = 3
    print('Requiring points are at least visible by ' + \
        f'{min_num_projective_geometries} of {len(cams)+1} projective geometries ...')
    enough_mask = np.sum(np.isfinite(p[:, :, 0]), axis=1) >= \
        min_num_projective_geometries
    p = p[enough_mask, :, :]


    # Projective geometries (projectors AND cameras)
    bundle_adjust_cams = [ projector, ]
    bundle_adjust_cams.extend(cams)
    # Initial estimates for 3D points
    P_init = np.zeros((p.shape[0], 3))
    P_init[:, 2] = 500.0
    # Run bundle adjustment
    print(f'Running bundle adjustment on {p.shape[0]} points ...')
    tic = time.monotonic()
    P, residuals, distances = bundle_adjust_points(bundle_adjust_cams, p, P_init, full=True)
    toc = time.monotonic()
    print(f'Bundle adjustment image took {(toc - tic):.1f}s')


    # Filter point cloud based on errors in bundle adjustment:
    # use mean distance error in mm over all projective geometries
    distances = np.sqrt(np.nanmean(np.square(distances), axis=1))
    residuals = np.sqrt(np.nanmean(np.square(residuals), axis=1))
    max_distance_error_mm = 2.0
    distance_low_mask = distances < max_distance_error_mm
    p = p[distance_low_mask, :, :]
    P = P[distance_low_mask, :]
    distances = distances[distance_low_mask]
    residuals = residuals[distance_low_mask]
    print(f'After error filtering, {p.shape[0]} points remain')


    # Determine point colors
    i = 0
    if i == 0:
        print('Extracting colors ...')
        color_samples = []
        for cam_no in range(len(cams)):
            image = white_images[cam_no]
            points = p[:, cam_no + 1, :]
            samples, on_chip_mask = image_sample_points_bilinear(image, points)
            # Move samples to data structure of same size as points
            color_sample = np.zeros((points.shape[0], 3))
            color_sample[:] = np.nan
            color_sample[on_chip_mask] = samples
            color_samples.append(color_sample)
        color_samples = np.array(color_samples)
        # Take median color of all cameras; NaN values are ignored
        C = np.nanmedian(color_samples, axis=0) / 255.0
    elif i == 1:
        # Colorize points by how many cameras point was seen
        cmap = mpl.colormaps['viridis']
        colors = cmap(np.linspace(0, 1, len(cams)))[:, :3]
        visible_by_n_cams = np.sum(np.isfinite(p[:, :, 0]), axis=1) - 2 # -1 for projector, -1 to get to 0..num_cams-1
        C = colors[visible_by_n_cams]
    elif i == 2:
        # Colorize points by residuals RMS over all cameras for each point
        cmap = mpl.colormaps['viridis']
        norm_residuals = np.log(residuals)
        norm_residuals = norm_residuals / np.max(norm_residuals)
        C = cmap(norm_residuals)[:, :3]
    else:
        # Colorize points by distance to rays over all cameras for each point
        cmap = mpl.colormaps['viridis']
        norm_distances = np.log(distances)
        norm_distances = norm_distances / np.max(norm_distances)
        C = cmap(norm_distances)[:, :3]

    # Create Open3d point cloud
    pcl = pcl_create(point_positions=P, point_colors=C)
    filename = os.path.join(data_dir, 'point_cloud_raw.ply')
    pcl_save(filename, pcl)

    objects = [ pcl ]
    if False:
        objects.append(projector.get_cs(size=150.))
        objects.append(projector.get_frustum(size=300.))
        for cam in cams:
            objects.append(cam.get_cs(size=150.))
            objects.append(cam.get_frustum(size=300.))
    geometries_visualize(objects)
