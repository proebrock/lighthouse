import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../'))
from trafolib.trafo3d import Trafo3d
from common.image_utils import image_show_multiple, \
    image_3float_to_rgb, image_save
from common.geometry_utils import mesh_create_cs, mesh_create_quad, \
    geometries_visualize, mesh_load, mesh_save, mesh_transform
from common.pixel_matcher import LineMatcherBinary, LineMatcherPhaseShift, \
    ImageMatcher
from camsimlib.camera_model import CameraModel
from camsimlib.light_projector import LightProjector



def visualize_scene(mesh, projector, cams):
    mesh_create_cs(size=50.0)
    objects = [ mesh ]
    objects.append(projector.get_cs(size=100))
    objects.append(projector.get_frustum(size=200))
    for cam in cams:
        objects.append(cam.get_cs(size=50))
        objects.append(cam.get_frustum(size=200))
    geometries_visualize(objects)



if __name__ == '__main__':
     # Random but reproducible
    np.random.seed(42)
    # Path where to store the data
    data_dir = 'data'
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    print(f'Using data path "{data_dir}"')

    # Generate mesh of object
    if False:
        # Simple test mesh: plane
        mesh = mesh_create_quad((1000, 1000), color=(1, 1, 1))
        mesh.translate(-mesh.get_center())
        mesh_pose = Trafo3d(t=(0, 0, 650), rpy=np.deg2rad((10, 180, -5)))
    else:
        mesh = mesh_load('../data/fox_head.ply')
        mesh.translate(-mesh.get_center())
        mesh.scale(180, center=(0, 0, 0))
        mesh_pose = Trafo3d(t=(0, 0, 650), rpy=np.deg2rad((0, 160, 180)))
    mesh_transform(mesh, mesh_pose)
    mesh.compute_vertex_normals()

    # Generate projector
    projector_shape = (600, 800)
    projector_image = np.zeros((*projector_shape, 3), dtype=np.uint8)
    projector = LightProjector(image=projector_image,
        focal_length=0.9*np.asarray(projector_shape))
    projector_pose = Trafo3d(t=(0, 30, 0), rpy=np.deg2rad((10, 0, 0)))
    projector.set_pose(projector_pose)

    # Generate cameras
    cam0 = CameraModel(chip_size=(32, 20), focal_length=(32, 32))
    cam0.set_distortion((-0.1, 0.1, 0.05, -0.05, 0.2, 0.08))
    cam0_pose = Trafo3d(t=(-200, 10, 0), rpy=np.deg2rad((3, 16, 1)))
    cam0.set_pose(cam0_pose)
    cam1 = CameraModel(chip_size=(40, 30), focal_length=(40, 40))
    cam1.set_distortion((0.1, 0.05, 0.0, 0.05, -0.1, 0.12))
    cam1_pose = Trafo3d(t=(210, -5, 3), rpy=np.deg2rad((2, -14, -2)))
    cam1.set_pose(cam1_pose)
    cams = [ cam0, cam1 ]
    for cam in cams:
        cam.scale_resolution(40)

    # Visualize scene
    #visualize_scene(mesh, projector, cams)

    # Generate projector images
    if True:
        num_time_steps = 11
        num_phases = 2
        row_matcher = LineMatcherPhaseShift(projector_shape[0],
            num_time_steps, num_phases)
        col_matcher = LineMatcherPhaseShift(projector_shape[1],
            num_time_steps, num_phases)
    else:
        row_matcher = LineMatcherBinary(projector_shape[0])
        col_matcher = LineMatcherBinary(projector_shape[1])
    matcher = ImageMatcher(projector_shape, row_matcher, col_matcher)
    images = matcher.generate()
    #image_show_multiple(images, single_window=True)
    #plt.show()

    # Save pattern images
    for image_no in range(images.shape[0]):
        image_save(os.path.join(data_dir, f'pattern{image_no:04}.png'),
            images[image_no])

    # Snap camera images
    for image_no in range(images.shape[0]):
        for cam_no in range(len(cams)):
            basename = os.path.join(data_dir,
                f'pattern{image_no:04}_cam{cam_no:04}')
            print(f'Snapping image {basename} ...')
            projector.set_image(images[image_no])
            cam = cams[cam_no]
            tic = time.monotonic()
            _, cam_image, _ = cam.snap([ mesh ], \
                lights=[projector])
            toc = time.monotonic()
            print(f'Snapping image took {(toc - tic):.1f}s')
            # Save generated snap
            cam_image = image_3float_to_rgb(cam_image)
            image_save(basename + '.png', cam_image)

    # Save configuration
    filename = os.path.join(data_dir, 'mesh.ply')
    mesh_save(filename, mesh)
    filename = os.path.join(data_dir, 'projector.json')
    projector.json_save(filename)
    for i, cam in enumerate(cams):
        basename = os.path.join(data_dir, f'cam{i:04d}')
        cam.json_save(basename + '.json')
    filename = os.path.join(data_dir, 'matcher.json')
    matcher.json_save(filename)
