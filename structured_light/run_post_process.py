import os
import sys

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../'))
from common.geometry_utils import pcl_load, pcl_colorize_by_scalar, \
    pcl_set_colors, geometries_visualize
from camsimlib.light_projector import LightProjector



def fix_colors(pcl, point_light_source_location=(0., 0., 0.)):
    """ Fix color brightness at the borders of the point cloud
    Nice idea in theory, disappointing results in practice:
    When reconstructing the colors of a round(-ish) object, points at the borders
    are usually darker, because the angle of the object at theses points
    relative to the light source (projector, assumed as a point light source)
    is rather close to 90deg.
    Here we try to do the same as if we would calculate the shading
    of points under the point light source: dot product between vector towards
    light source and normal vector: if this 1, the part of the object is directly
    facing the light source and the point is very bright, if the dot product is close
    to zero, the point is very dark or black.
    We just use these brightnesses to retrospetively correct the brightness of
    the colors of the point cloud: where the dot product is 1, we don't have
    to correct anything, if the dot product is small, we need to make these points
    brighter
    """
    # Calculate vectors from each point to light source
    points = pcl.point.positions.numpy()
    point_to_light_vector = np.asarray(point_light_source_location) - points
    # Normalize vectors
    point_to_light_vector_len = np.sqrt(np.sum(np.square(point_to_light_vector), axis=1))
    mask_valid = ~np.isclose(point_to_light_vector_len, 0.0)
    point_to_light_vector /= point_to_light_vector_len[mask_valid, np.newaxis]
    # Calculate dot product with normal vectors
    normals = pcl.point.normals.numpy()
    dp = np.sum(point_to_light_vector * normals, axis=1)

    # Check result for plausibility
    #pcl_colorize_by_scalar(pcl, dp)
    #return

    dp = 1./dp
    # Attempts to reduce the effect
    #dp = np.clip(dp, 0., 100.)
    #mask_valid[mask_valid] = dp[mask_valid] < 100.

    # Correct colors
    colors = pcl.point.colors.numpy()
    colors[mask_valid, :] *= dp[mask_valid, np.newaxis]
    colors = np.clip(colors, 0., 1.)
    pcl_set_colors(pcl, colors)



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'structured_light')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    # Load configuration
    # projector
    filename = os.path.join(data_dir, 'projector.json')
    projector = LightProjector()
    projector.json_load(filename)
    # mesh
    pcl = pcl_load(os.path.join(data_dir, 'point_cloud_raw.ply'))


    # Outlier removal
    pcl, inlier_indices = pcl.remove_statistical_outliers(10, 3.0)
    if False:
        pcl_inlier = pcl.select_by_index(inlier_indices, invert=False)
        pcl_inlier.paint_uniform_color((0., 0., 0.))
        pcl_outlier = pcl.select_by_index(inlier_indices, invert=True)
        pcl_outlier.paint_uniform_color((1., 0., 0.))
        geometries_visualize([pcl_inlier, pcl_outlier])


    # Normal estimation
    pcl.estimate_normals(max_nn=30)
    #pcl.orient_normals_consistent_tangent_plane(k=30)
    pcl.orient_normals_towards_camera_location(projector.get_pose().get_translation())
    if False:
        vis = o3d.visualization.Visualizer()
        vis.create_window()
        vis.add_geometry(pcl)
        vis.get_render_option().point_show_normal = True
    #    vis.get_render_option().point_color_option = o3d.visualization.PointColorOption.Normal
        vis.run()
        vis.destroy_window()

    fix_colors(pcl, projector.get_pose().get_translation())
    geometries_visualize([pcl])