import os
import sys
import time
import glob

import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('../'))
from common.image_utils import image_load_multiple
from common.pixel_matcher import ImageMatcher
from camsimlib.camera_model import CameraModel
from camsimlib.light_projector import LightProjector



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'structured_light')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    # Load configuration
    filename = os.path.join(data_dir, 'projector.json')
    projector = LightProjector()
    projector.json_load(filename)
    cam_filenames = sorted(glob.glob(os.path.join(data_dir, 'cam????.json')))
    cams = []
    for i, filename in enumerate(cam_filenames):
        cam = CameraModel()
        cam.json_load(filename)
        cams.append(cam)
    filename = os.path.join(data_dir, 'matcher.json')
    matcher = ImageMatcher()
    matcher.json_load(filename)

    # Run matching
    matches = []
    for cam_no in range(len(cams)):
        print(f'Matching of cam{cam_no} ...')
        tic = time.monotonic()
        print('    Loading images ...')
        filenames = os.path.join(data_dir, f'pattern????_cam{cam_no:04}.png')
        images = image_load_multiple(filenames)
        print('    Matching pixels ...')
        m = matcher.match(images)
        toc = time.monotonic()
        print(f'    Done, took {(toc - tic):.1f}s')
        matches.append(m)

    # Save results
    print('Saving matches to file ...')
    filename = os.path.join(data_dir, 'matches.npz')
    np.savez(filename, *matches)
    print('    Done.')

