import os
import sys
import glob

import numpy as np
import matplotlib.pyplot as plt

import cv2

sys.path.append(os.path.abspath('../'))
from common.image_utils import image_load
from camsimlib.image_mapping import image_indices_to_points
from common.aruco_utils import CharucoBoard



def generate_circle_indices(radius):
    # Generate indices for rectangular region
    rows = np.arange(-radius, radius + 1)
    cols = np.arange(-radius, radius + 1)
    rows, cols = np.meshgrid(rows, cols, indexing='ij')
    indices = np.vstack((rows.flatten(), cols.flatten())).T
    # Reduce indices to circular region
    mask = np.sum(np.square(indices), axis=1) < (radius * radius)
    indices = indices[mask, :]
    return indices



def transform_cam_point_to_proj_point(cam_img_point, circle_indices,
    cpoints, ppoints):
    # Get indices in a circular region around the corner of the calibration
    # board given by the image point of the camera
    ci = circle_indices + cam_img_point.astype(int)
    # Get cam points and projector points of that region
    # TODO: This may go wrong if the circle radius is large and the
    # cam_img_point is located close to the boundaries of the
    # cpoints/ppoints arrays... so check boundaries?
    cp = cpoints[ci[:, 1], ci[:, 0], :]
    pp = ppoints[ci[:, 1], ci[:, 0], :]
    # Filter both by validity of projector points
    mask = np.all(np.isfinite(pp), axis=1)
    cp = cp[mask, :]
    pp = pp[mask, :]
    if cp.shape[0] < 4:
        raise Exception('Not enough points to calculate homography')
    # Calculate local homography, methods: 0, cv2.RANSAC, cv2.LMEDS, cv2.RHO
    H, mask = cv2.findHomography(cp, pp, method=cv2.LMEDS)
    # Translate camera image point to projector using homography
    x = H @ np.array((cam_img_point[0], cam_img_point[1], 1.0))
    if np.isclose(x[2], 0.0):
        raise Exception('Invalid homography')
    return np.array((x[0] / x[2], x[1] / x[2]))



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'projector_calibrate')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    # Load configuration
    # board
    board_filename = os.path.join(data_dir, 'board0000.json')
    board = CharucoBoard()
    board.json_load(board_filename)
    # num_cams
    pattern = 'board0000_pattern0000_cam????.png'
    num_cams = len(glob.glob(os.path.join(data_dir, pattern)))
    print(f'Detected {num_cams} cams ...')
    # num_boards
    pattern = 'board????_pattern0000_cam0000.png'
    num_boards = len(glob.glob(os.path.join(data_dir, pattern)))
    print(f'Detected {num_boards} board poses ...')

    # projector_shape
    filename = os.path.join(data_dir, 'pattern0000.png')
    image = image_load(filename)
    projector_shape = np.array(image.shape[0:2])
    print(f'Shape projector {projector_shape}')
    # cam_shapes
    pattern = 'board0000_pattern0000_cam????.png'
    filenames = sorted(glob.glob(os.path.join(data_dir, pattern)))
    cam_shapes = []
    for i, filename in enumerate(filenames):
        image = image_load(filename)
        cam_shape = np.array(image.shape[0:2])
        cam_shapes.append(cam_shape)
        print(f'Shape cam{i} {cam_shape}')
    # matches
    filename = os.path.join(data_dir, 'matches.npz')
    print('Loading matches ...')
    npz = np.load(filename)
    matches = []
    for cam_no in range(num_cams):
        matches.append(npz[f'arr_{cam_no}'])
    print('    Done.')


    cam_image_points = np.empty((num_cams, num_boards, board.max_num_points(), 2))
    cam_image_points[:] = np.nan
    for cam_no in range(num_cams):
        for board_no in range(num_boards):
            filename_white = os.path.join(data_dir, \
                f'board{board_no:04}_pattern0001_cam{cam_no:04}.png')
            white_image = image_load(filename_white)
            obj_points, img_points, ids = board.detect_obj_img_points(white_image, with_ids=True)
            cam_image_points[cam_no, board_no, ids, :] = img_points
    object_points = board.get_object_points()


    circle_indices = generate_circle_indices(radius=20)
    if True:
        """ Plot one image with camera image point and region used
        to calculate local homography
        """
        cam_no = 0
        board_no = 0
        point_no = 7
        filename_white = os.path.join(data_dir, \
            f'board{board_no:04}_pattern0001_cam{cam_no:04}.png')
        white_image = image_load(filename_white)
        cam_img_point = cam_image_points[cam_no, board_no, point_no, :]
        ci = circle_indices + cam_img_point.astype(int)
        pi = matches[cam_no][board_no, ci[:, 1], ci[:, 0]]
        fig = plt.figure()
        ax = fig.add_subplot(121)
        ax.set_title(f'cam{cam_no} chip, board{board_no}, point{point_no}')
        ax.imshow(white_image)
        ax.plot(ci[:, 0], ci[:, 1], '.r', alpha=0.2)
        ax.plot(cam_img_point[0], cam_img_point[1], '+b')
        ax = fig.add_subplot(122)
        ax.set_title(f'projector chip')
        ax.set_xlim(0, projector_shape[1])
        ax.set_ylim(0, projector_shape[0])
        ax.invert_yaxis()
        ax.plot(pi[:, 1], pi[:, 0], '.r')
        plt.show()


    all_projector_image_points = np.empty((num_cams, num_boards, board.max_num_points(), 2))
    all_projector_image_points[:] = np.nan
    for cam_no in range(num_cams):
        # Camera points
        shape = cam_shapes[cam_no]
        rows = np.arange(shape[0])
        cols = np.arange(shape[1])
        rows, cols = np.meshgrid(rows, cols, indexing='ij')
        cindices = np.vstack((rows.flatten(), cols.flatten())).T
        cpoints = image_indices_to_points(cindices)
        cpoints = cpoints.reshape((*shape, 2))
        for board_no in range(num_boards):
            # Projector points
            pindices = matches[cam_no][board_no].reshape((-1, 2))
            ppoints = image_indices_to_points(pindices)
            ppoints = ppoints.reshape(matches[cam_no][board_no].shape)
            assert ppoints.shape == cpoints.shape
            for point_no in range(board.max_num_points()):
                cam_img_point = cam_image_points[cam_no, board_no, point_no, :]
                if not np.all(np.isfinite(cam_img_point)):
                    continue
                proj_img_point = transform_cam_point_to_proj_point(cam_img_point,
                    circle_indices, cpoints, ppoints)
                all_projector_image_points[cam_no, board_no, point_no, :] = proj_img_point
    # Average over all cameras to get final projector image points
    projector_image_points = np.nanmedian(all_projector_image_points, axis=0)
    # Calculate errors, shape (num_cams, num_boards, num_corners)
    errors = all_projector_image_points - projector_image_points[np.newaxis, :, :, :]
    errors = np.sqrt(np.sum(np.square(errors), axis=3)) # Calculate distance on chip


    # Save results
    filename = os.path.join(data_dir, 'points.npz')
    np.savez(filename,
        object_points=object_points,
        cam_image_points=cam_image_points,
        projector_image_points=projector_image_points)

    print('Chessboard corners per cam:')
    for cam_no in range(num_cams):
        num_corners = np.sum(np.isfinite(cam_image_points[cam_no, :, :, 0]))
        max_num_corners = board.max_num_points() * num_boards
        print(f'    cam{cam_no}: {num_corners}/{max_num_corners} corners')
    print('Chessboard corners per board:')
    for board_no in range(num_boards):
        num_corners = np.sum(np.isfinite(cam_image_points[:, board_no, :, 0]))
        max_num_corners = board.max_num_points() * num_cams
        print(f'    board{board_no}: {num_corners}/{max_num_corners} corners')

    print('\nErrors per cam:')
    for cam_no in range(num_cams):
        error_rms = np.sqrt(np.nanmean(np.square(errors[cam_no, :, :])))
        print(f'    cam{cam_no}: {error_rms:.3f} pixel RMS')
    print('Errors per board:')
    for board_no in range(num_boards):
        error_rms = np.sqrt(np.nanmean(np.square(errors[:, board_no, :])))
        print(f'    board{board_no}: {error_rms:.3f} pixel RMS')


    if True:
        """ For all board poses, plot all the camera image points for all cameras
        to the projector chip and visualize if the homography from both cameras work:
        projecting the same camera image point from different cameras to the same
        point on the projector chip
        """
        fig, axes = plt.subplots(3, 4) # Adjust to number of boards if necessary
        fig.tight_layout()
        axes = axes.flatten()
        for board_no in range(num_boards):
            ax = axes[board_no]
            error_rms = np.sqrt(np.nanmean(np.square(errors[:, board_no, :])))
            ax.set_title(f'board{board_no}, error {error_rms:.3f}pix RMS')
            colors = [ 'r', 'g', 'b', 'c', 'm', 'y' ]
            for cam_no in range(num_cams):
                pp = all_projector_image_points[cam_no, board_no, :, :]
                ax.plot(pp[:, 0], pp[:, 1], '+', color=colors[cam_no], label=f'cam{cam_no}')
            pp = projector_image_points[board_no, :, :]
            ax.plot(pp[:, 0], pp[:, 1], '+k', label='final')
            ax.set_xlim(0, projector_shape[1])
            ax.set_ylim(0, projector_shape[0])
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ax.invert_yaxis()
            ax.legend()
        plt.show()

