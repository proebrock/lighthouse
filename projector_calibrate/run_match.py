import os
import sys
import time
import glob

import numpy as np

sys.path.append(os.path.abspath('../'))
from common.image_utils import image_load_multiple
from common.pixel_matcher import ImageMatcher
from common.aruco_utils import CharucoBoard



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'projector_calibrate')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    # Load configuration
    # board
    board_filename = os.path.join(data_dir, 'board0000.json')
    board = CharucoBoard()
    board.json_load(board_filename)
    # num_cams
    pattern = 'board0000_pattern0000_cam????.png'
    num_cams = len(glob.glob(os.path.join(data_dir, pattern)))
    print(f'Detected {num_cams} cams ...')
    # num_boards
    pattern = 'board????_pattern0000_cam0000.png'
    num_boards = len(glob.glob(os.path.join(data_dir, pattern)))
    print(f'Detected {num_boards} board poses ...')
    # load matcher
    filename = os.path.join(data_dir, 'matcher.json')
    matcher = ImageMatcher()
    matcher.json_load(filename)

    # Run matching
    matches = []
    for cam_no in range(num_cams):
        board_matches = []
        for board_no in range(num_boards):
            print(f'Matching of cam{cam_no} and board{board_no} ...')
            tic = time.monotonic()
            print('    Loading images ...')
            filenames = os.path.join(data_dir, \
                f'board{board_no:04}_pattern????_cam{cam_no:04}.png')
            images = image_load_multiple(filenames)
            print('    Matching pixels ...')
            m = matcher.match(images)
            toc = time.monotonic()
            print(f'    Done, took {(toc - tic):.1f}s')
            board_matches.append(m)
        board_matches = np.asarray(board_matches)
        matches.append(board_matches)

    # Save results
    print('Saving matches to file ...')
    filename = os.path.join(data_dir, 'matches.npz')
    np.savez(filename, *matches)
    print('    Done.')

