import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt
import cv2

from scipy.stats import multivariate_normal

sys.path.append(os.path.abspath('../'))
from trafolib.trafo3d import Trafo3d
from common.geometry_utils import mesh_create_cs, mesh_create_surface_from_function, \
    geometries_visualize, mesh_save
from common.image_utils import image_3float_to_rgb, image_save
from camsimlib.material import Material
from camsimlib.camera_model import CameraModel
from camsimlib.screen import Screen
from common.pixel_matcher import LineMatcherPhaseShift, ImageMatcher

# For various debugging purposes
from common.geometry_utils import lineset_create, lineset_set_line_colors
from common.image_utils import image_load
from camsimlib.ray_tracer_embree import RayTracerEmbree as RayTracer
from common.math_utils import reflect_3d



if __name__ == '__main__':
     # Random but reproducible
    np.random.seed(42)
    # Path where to store the data
    data_dir = 'data'
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
    print(f'Using data path "{data_dir}"')

    # Generate cameras
    cam0 = CameraModel(chip_size=(12, 9),
                      focal_length=(10., 8.),
                    )
    cam0.place((248, 121, 102))
    cam0.look_at((53, 59, 0))
    cam1 = CameraModel(chip_size=(12, 9),
                      focal_length=(10.1, 8.2),
                    )
    cam1.place((253, -122, 101))
    cam1.look_at((51, -52, 0))
    cam2 = CameraModel(chip_size=(15, 12),
                      focal_length=(12.2, 9.9),
                    )
    cam2.place((251, -2, 202))
    cam2.look_at((48, 0, 0))
    cams = [ cam0, cam1, cam2 ]

    # Generate image and screens
    if True:
        # Both images have same aspect ratio
        image0 = np.zeros((900, 1200, 3), dtype=np.uint8)
        image1 = np.zeros((768, 1024, 3), dtype=np.uint8)
    else:
        # Test image
        image = image_load('../data/lena.jpg')
        # Duplicate two instances of image next to each
        # other with changed colors
        image = np.hstack((image, image[:,:,2::-1]))
        # Both images have same aspect ratio
        image0 = cv2.resize(image, (1200, 900))
        image1 = cv2.resize(image, (1024, 768))
    width_mm = 400.0
    height_mm = 300.0
    screen0_pose = Trafo3d(t=(-201.0, 1.0, 31.0), rpy=np.deg2rad((0.2, -40, -0.1)))
    screen0 = Screen((width_mm, height_mm), image0, screen0_pose)
    screen1_pose = Trafo3d(t=(-202.0, -300.0, 29.0), rpy=np.deg2rad((.1, -41, .3)))
    screen1 = Screen((width_mm, height_mm), image1, screen1_pose)
    screens = [ screen0, screen1 ]
    screen0_mesh = screen0.get_mesh()
    screen1_mesh = screen1.get_mesh()
    screen_meshes = [ screen0_mesh, screen1_mesh ]

    # Generate surface: a lightly convex or concace (sign of first expression)
    # surface with one or two dents in it (second expression)
    fun = lambda x: \
        2.5e3 * multivariate_normal.pdf(x, mean=[0., 0.], cov=[[30, 0.0], [0.0, 30]]) \
        + 3. * multivariate_normal.pdf(x, mean=[3., 4.], cov=[[0.4, 0.0], [0.0, 0.3]])
    surface = mesh_create_surface_from_function(fun,
        xrange=(-10.0, 10.0), yrange=(-10.0, 10.0),
        num=(100, 100), scale=(400.0, 400.0, -1.5))
    surface.translate(-surface.get_center()) # De-mean

    # Visualize scene
    if False:
        world_cs = mesh_create_cs(size=50.0)
        geometries = [ world_cs, surface, *screen_meshes ]
        for cam in cams:
            geometries.append(cam.get_cs(size=50))
            geometries.append(cam.get_frustum(size=100))
        for screen in screens:
            geometries.append(screen.get_cs(size=50))
        geometries_visualize(geometries)

    # Visualize rays: It is not easy to arrange screens, object and
    # cameras in a way that the reflected camera rays hit a screen
    # and every area on the object is covered by at least two cameras.
    if False:
        lines = []
        colors = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
        for i, cam in enumerate(cams):
            print(f'Camera cam{i}:')
            # Generate camera rays and find intersections with surface
            rays = cam.get_rays()
            rt0 = RayTracer(rays, [ surface ], [ Material() ])
            rt0.run()
            print(f'    {len(rays)} rays')
            n0 = np.sum(rt0.r.mask)
            print(f'    {n0} hit the surface')
            lines_cam = lineset_create(
                np.vstack([
                    cam.get_pose().get_translation(),
                    rt0.r.points
                    ]),
                np.vstack([
                    np.zeros(n0),
                    np.arange(1, n0+1)
                ]).T
            )
            lineset_set_line_colors(lines_cam, colors[i])
            lines.append(lines_cam)
            # Mirror rays and find intersections with screens
            rays.dirs = reflect_3d( \
                rays.dirs[rt0.r.mask], rt0.r.normals)
            rays.origs = rt0.r.points
            rt1 = RayTracer(rays, screen_meshes, len(screen_meshes) * [ Material() ])
            rt1.run()
            n1 = np.sum(rt1.r.mask)
            print(f'    {n1} hit any of the screens')
            lines_screen = lineset_create(
                np.vstack([
                    rt0.r.points[rt1.r.mask],
                    rt1.r.points
                ]),
                np.vstack([
                    np.arange(n1),
                    np.arange(n1) + n1
                ]).T
            )
            lineset_set_line_colors(lines_screen, colors[i])
            lines.append(lines_screen)
        # Visualize result
        world_cs = mesh_create_cs(size=50.0)
        geometries = [ world_cs, surface, *screen_meshes, *lines ]
        for cam in cams:
            geometries.append(cam.get_cs(size=10))
        geometries_visualize(geometries)

    # Scale up cameras
    for cam in cams:
        cam.scale_resolution(80)

    # Generate screen images
    num_time_steps = 11
    num_phases = 2
    images = []
    matchers = []
    for screen in screens:
        row_matcher = LineMatcherPhaseShift(screen.get_shape()[0],
            num_time_steps, num_phases)
        col_matcher = LineMatcherPhaseShift(screen.get_shape()[1],
            num_time_steps, num_phases)
        matcher = ImageMatcher(screen.get_shape(), row_matcher, col_matcher)
        matchers.append(matcher)
        images.append(matcher.generate())

    # Snap images: We take our time with this: We display the patterns
    # at one screen at a time while keeping all other displays dark. We
    # need to do this because the patterns are only unique within one
    # screen. If you want to speed up image capture you would need to
    # create one pattern and distribute that over multiple screens which
    # is even more challenging if you want to support multiple screens
    # with individual resolutions...
    for screen_no, screen in enumerate(screens):
        for image_no, image in enumerate(images[screen_no]):
            screen.set_image(image)
            for cam_no, cam in enumerate(cams):
                basename = os.path.join(data_dir,
                    f'screen{screen_no:04d}_pattern{image_no:04d}_cam{cam_no:04d}')
                print(f'Snapping image {basename} ...')
                tic = time.monotonic()
                # We introduce just the current screen in the snap;
                # when using a real setup, you would have to set all other
                # screens to 'black'
                _, cam_image, _ = cam.snap([ screen.get_mesh(), surface ], \
                    materials = [ Material(is_mirror=False), \
                    Material(is_mirror=True) ])
                toc = time.monotonic()
                print(f'Snapping image took {(toc - tic):.1f}s')
                # Save generated snap
                cam_image = image_3float_to_rgb(cam_image)
                image_save(basename + '.png', cam_image)

    # Save configuration
    filename = os.path.join(data_dir, 'mesh.ply')
    mesh_save(filename, surface)
    for i, cam in enumerate(cams):
        basename = os.path.join(data_dir, f'cam{i:04d}')
        cam.json_save(basename + '.json')
    for i, screen in enumerate(screens):
        basename = os.path.join(data_dir, f'screen{i:04d}')
        screen.json_save(basename + '.json')
    for i, matcher in enumerate(matchers):
        basename = os.path.join(data_dir, f'matcher{i:04d}')
        matcher.json_save(basename + '.json')

