import numpy as np
import matplotlib.pyplot as plt

from scipy.interpolate import RegularGridInterpolator



class CamScreenInterpolator:

    def __init__(self, matches):
        """
        :param matches: shape (cam_height, cam_width, 2)
        """
        assert matches.ndim == 3
        assert matches.shape[2] == 2
        rows = np.arange(matches.shape[0])
        cols = np.arange(matches.shape[1])
        self._interp = [
            RegularGridInterpolator((rows, cols), matches[:, :, 0],
                method='linear', bounds_error=False),
            RegularGridInterpolator((rows, cols), matches[:, :, 1],
                method='linear', bounds_error=False),
        ]
        self._matches = matches



    def match(self, cam_indices):
        assert cam_indices.ndim == 2
        assert cam_indices.shape[1] == 2
        if False:
            cam_indices_int = np.round(cam_indices).astype(int)
            return self._matches[cam_indices_int[:, 0], cam_indices_int[:, 1]]
        else:
            screen_indices = np.empty_like(cam_indices)
            screen_indices[:] = np.nan
            mask = np.all(np.isfinite(cam_indices), axis=1)
            screen_indices[mask, 0] = self._interp[0](cam_indices[mask, :])
            screen_indices[mask, 1] = self._interp[1](cam_indices[mask, :])
            return  screen_indices



class CamMultiScreenInterpolator:

    def __init__(self, matches):
        """
        :param matches: shape (num_screens, cam_height, cam_width, 2)
        """
        assert matches.ndim == 4
        assert matches.shape[3] == 2
        self._interp = []
        for i in range(matches.shape[0]):
            if i > 0:
                assert matches[0].shape == matches[i].shape
            self._interp.append(CamScreenInterpolator(matches[i, :, :, :]))



    def match(self, cam_indices):
        assert cam_indices.ndim == 2
        assert cam_indices.shape[1] == 2
        screen_indices = np.empty_like(cam_indices)
        screen_indices[:] = np.nan
        screen_nos = -1 * np.ones(cam_indices.shape[0], dtype=int)
        for screen_no, interp in enumerate(self._interp):
            si = interp.match(cam_indices)
            mask = np.all(np.isfinite(si), axis=1)
            # TODO: detect multiple matches
            screen_indices[mask, :] = si[mask, :]
            screen_nos[mask] = screen_no
        return screen_indices, screen_nos



    def match3d(self, cam_indices, screens):
        assert cam_indices.ndim == 2
        assert cam_indices.shape[1] == 2
        assert len(screens) == len(self._interp)
        screen_indices, screen_nos = self.match(cam_indices)
        points_3d = np.empty((cam_indices.shape[0], 3))
        points_3d[:] = np.nan
        for screen_no in range(len(screens)):
            mask = screen_nos == screen_no
            points_3d[mask, :] = screens[screen_no].image_indices_to_scene( \
                screen_indices[mask, :])
        return points_3d

