import os
import sys
import time
import glob

import numpy as np
import matplotlib.pyplot as plt
import open3d as o3d

sys.path.append(os.path.abspath('../'))
from common.math_utils import length_3d, normalize_3d, dot_3d, hr_int
from common.geometry_utils import pcl_create, pcl_set_colors, \
    pcl_colorize_by_scalar, pcl_save, lineset_create, lineset_set_line_colors, \
    mesh_load, geometries_visualize
from camsimlib.camera_model import CameraModel
from camsimlib.screen import Screen
from camsimlib.image_mapping import image_points_to_indices
from match_interpolator import CamMultiScreenInterpolator



def calculate_normals(scene_points, matchers, cams, screens):
    assert scene_points.ndim == 2
    assert scene_points.shape[1] == 3
    assert len(matchers) == len(cams)
    print(f'    Calculating {hr_int(scene_points.shape[0] * len(cams))} normals')
    normals = np.empty((scene_points.shape[0], len(cams), 3))
    normals[:] = np.nan
    for cam_no, cam in enumerate(cams):
        # Project 3D scene points to camera chip
        on_chip_points = cam.scene_to_chip(scene_points)[:, 0:2] # Skip depth
        mask = cam.points_on_chip_mask(on_chip_points)
        on_chip_points = on_chip_points[mask, :]
        on_chip_indices = image_points_to_indices(on_chip_points)
        screen_points = matchers[cam_no].match3d(on_chip_indices, screens)
        matching_mask = np.all(np.isfinite(screen_points), axis=1)
        screen_points = screen_points[matching_mask]
        mask[mask] = matching_mask
        # Incoming rays: screen_points to scene points
        in_rays = scene_points[mask] - screen_points
        in_rays_len = length_3d(in_rays)
        # Outgoing rays: scene_points to camera
        cam_point = cam.get_pose().get_translation()
        out_rays = cam_point - scene_points[mask]
        out_rays_len = length_3d(out_rays)
        # Determine angles between incoming and outgoing rays
        angles = np.arccos(dot_3d(in_rays, out_rays) / \
            (in_rays_len * out_rays_len))
        # Ray lengths have to be sufficient;
        # if angle is close to 0 or +-180 deg, ray is directly
        # coming from screen to the camera without reflection;
        # we filter those rays here
        omega = np.deg2rad(5.)
        reflect_mask = np.logical_and.reduce((
            # Min length of incoming ray in mm
            in_rays_len > 10.,
            # Min length of outgoing ray in mm
            out_rays_len > 10.,
            # Angle close to zero: direct view of screen point
            angles > omega,
            # Angle close to 180deg: direct view and point behind screen
            angles < (np.pi - omega)
        ))
        mask[mask] = reflect_mask
        # Calculate normals (law of reflection)
        n = in_rays_len[reflect_mask, np.newaxis] * out_rays[reflect_mask, :] - \
            out_rays_len[reflect_mask, np.newaxis] * in_rays[reflect_mask, :]
        normals[mask, cam_no, :] = \
            normalize_3d(n)
    return normals



def get_normals_deviations(normals):
    assert normals.ndim == 3
    assert normals.shape[2] == 3
    num_points = normals.shape[0]
    num_cams = normals.shape[1]
    cam_pairs = []
    for i in range(num_cams):
        for j in range(i + 1, num_cams):
            cam_pairs.append([i, j])
    angles = np.empty((num_points, len(cam_pairs)))
    angles[:] = np.nan
    for i, pair in enumerate(cam_pairs):
        angles[:, i] = dot_3d(normals[:, pair[0], :], normals[:, pair[1], :])
    mask = np.any(np.isfinite(angles), axis=1)
    deviations = np.empty((num_points, ))
    deviations[:] = np.nan
    # Minimum dot product means maximum angle
    dotmin = np.nanmin(angles[mask, :], axis=1)
    deviations[mask] = np.arccos(dotmin)
    return deviations



def get_average_normals(normals):
    assert normals.ndim == 3
    assert normals.shape[2] == 3
    num_points = normals.shape[0]
    num_cams = normals.shape[1]
    num_normals = np.sum(np.all(np.isfinite(normals), axis=2), axis=1)
    mask = num_normals > 0
    avrg = np.empty((num_points, 3))
    avrg[:] = np.nan
    avrg[mask] = np.nansum(normals[mask, :, :], axis=1) / \
        num_normals[mask, np.newaxis]
    return avrg



def sample_rays(rays, factors, matchers, cams, screens):
    assert factors.ndim == 2
    assert len(rays) == factors.shape[1]
    P = rays.points(factors)
    normals = calculate_normals(P.reshape((-1, 3)), matchers, cams, screens)
    d = get_normals_deviations(normals)
    d = d.reshape((P.shape[0], len(rays)))
    mask = np.any(np.isfinite(d), axis=0)
    min_indices = np.nanargmin(d[:,mask], axis=0)
    fmin = factors[min_indices, mask]
    dmin = d[min_indices, mask]
    N = get_average_normals(normals)
    N = N.reshape((P.shape[0], len(rays), 3))
    N = N[min_indices, mask]
    P = P[min_indices, mask]
    return fmin, dmin, mask, P, N



def multi_linspace_slow(starts, stops, num):
    assert starts.size == stops.size
    n = starts.size
    samples = np.empty((num, n))
    for i in range(n):
        samples[:, i] = np.linspace(starts[i], stops[i], num)
    return samples



def multi_linspace(starts, stops, num):
    assert starts.size == stops.size
    n = starts.size
    samples = np.tile(np.arange(num, dtype=float), (n, 1)).T
    samples *= (stops - starts)[np.newaxis, :]
    samples = samples / (num - 1)
    samples += starts[np.newaxis, :]
    return samples



def find_minimum(rays, left, right, matchers, cams, screens):
    num_samples = 100
    lmbda = 10.
    vrays = rays.copy()
    all_mask = np.ones(len(rays), dtype=bool)
    lefts = left * np.ones(len(rays))
    rights = right * np.ones(len(rays))
    num_iter = 1
    max_num_iter = 4
    while True:
        print(f'Iteration {num_iter}/{max_num_iter} ...')
        print(f'    Search range width is {rights[0] - lefts[0]:.3f}mm')
        print(f'    With {num_samples} samples this is {(rights[0] - lefts[0]) / num_samples:.6f}mm sample distance')
        tic = time.monotonic()
        factors = multi_linspace(lefts, rights, num_samples)
        fmin, dmin, mask, P, N = sample_rays(vrays, factors, matchers, cams, screens)
        toc = time.monotonic()
        print(f'    Computation took {(toc - tic):.1f}s')
        all_mask[all_mask] = mask
        if num_iter == max_num_iter:
            return fmin, dmin, all_mask, P, N
        vrays = vrays.filter(mask)
        ihalf = (rights[mask] - lefts[mask]) / (2. * lmbda)
        lefts = fmin - ihalf
        rights = fmin + ihalf
        num_iter += 1



if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    rng = np.random.default_rng(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'structured_light')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    print('Loading configuration ...')
    # Load cameras
    cam_filenames = sorted(glob.glob(os.path.join(data_dir, 'cam????.json')))
    cams = []
    for i, filename in enumerate(cam_filenames):
        cam = CameraModel()
        cam.json_load(filename)
        cams.append(cam)
    print(f'    Found {len(cams)} cams')
    # Load screens
    screen_filenames = sorted(glob.glob(os.path.join(data_dir, 'screen????.json')))
    screens = []
    for i, filename in enumerate(screen_filenames):
        screen = Screen()
        screen.json_load(filename)
        screens.append(screen)
    print(f'    Found {len(screens)} screens')
    # Load matches
    filename = os.path.join(data_dir, 'matches.npz')
    npz = np.load(filename)
    matches2d = []
    for cam_no in range(len(cams)):
        matches2d.append(npz[f'arr_{cam_no}'])
    print('    Matches loaded.')
    filename = os.path.join(data_dir, 'mesh.ply')
    mesh = mesh_load(filename)
    print('    Mesh loaded.')

    # Initialize list of matchers for each camera
    matchers = []
    for cam_no, cam in enumerate(cams):
        matchers.append(CamMultiScreenInterpolator(matches2d[cam_no]))

    if False:
        # Visualize normals and normal angles for selected points
        # on one or multiple camera rays
        cam_no = 2
        p = np.array((
            (600., 480.),
            (500., 800.),
            (500., 500.),
        ))
        rays = cams[cam_no].get_rays(p)
        factors = np.tile(np.linspace(50, 550, 100), (len(rays), 1)).T
        P = rays.points(factors)
        normals = calculate_normals(P.reshape((-1, 3)), matchers, cams, screens)
        deviations = get_normals_deviations(normals).reshape((-1, len(rays)))
        # Plot deviation against distance along the ray
        colors = ('r', 'g', 'b', 'c', 'm', 'y')
        fig, ax = plt.subplots()
        for ray_no in range(len(rays)):
            ax.plot(np.rad2deg(deviations[:, ray_no]), color=colors[ray_no],
                label=f'ray{ray_no}')
        ax.legend()
        ax.set_xlabel('Distance along camera ray (mm)')
        ax.set_ylabel('Maximum angle of all normal pairs (deg)')
        plt.show()
        # Visualize normals in 3D
        colors = ((1., 0., 0.), (0., 1., 0.), (0., 0., 1.),
                  (0., 1., 1.), (1., 0., 1.), (1., 1., 0.))
        geos = [ mesh, cams[cam_no].get_cs(50.) ]
        N = normals.reshape((P.shape[0], len(rays), len(cams), 3))
        normal_size = 10.
        for ray_no in range(len(rays)):
            pcl = pcl_create(P[:, ray_no, :])
            pcl_set_colors(pcl, colors[ray_no])
            geos.append(pcl)
            for cam_no in range(len(cams)):
                n = N[:, ray_no, cam_no, :]
                mask = np.all(np.isfinite(n), axis=1)
                p = P[mask, ray_no, :]
                n = n[mask, :]
                i = np.sum(mask)
                line_set = lineset_create(
                    np.vstack((p, p + normal_size * n)),
                    np.vstack((np.arange(i), i+np.arange(i))).T
                )
                lineset_set_line_colors(line_set, colors[cam_no])
                geos.append(line_set)
        geometries_visualize(geos)
        exit()

    # 3D reconstruction based on points on rays of single camera;
    # for full reconstruction use all cameras, for this example we just use one
    cam_no = 2
    rays = cams[cam_no].get_rays()
    rays = rays.sample_random(rng, 10000) # Random sample rays to speed up things
    if False:
        # Rather expensive method: generate thousand of points on all rays
        # and then take points along each ray with minimum angles between normals
        factors = np.tile(np.linspace(50, 550, 10000), (len(rays), 1)).T
        fmin, dmin, mask, P, N = sample_rays(rays, factors, matchers, cams, screens)
    else:
        # Iterative process of finding the minimum angle of normals along
        # each ray
        fmin, dmin, mask, P, N = find_minimum(rays, 50., 550., matchers, cams, screens)
    # Filter by minimum angle found: if minimum angle is not close to zero
    # we may not have found a sufficient minimum
    mask = dmin < np.deg2rad(.2)
    # Create point cloud object
    print(f'Final point cloud contains {np.sum(mask)} points')
    pcl = pcl_create(P[mask, :], N[mask, :])
    filename = os.path.join(data_dir, 'pcl.ply')
    pcl_save(filename, pcl)

    # Colorize point cloud by minimum residual angle between normals
    #pcl_colorize_by_scalar(pcl, dmin[mask])

    if False:
        # Outlier removal
        filtered_pcl, inlier_mask = pcl.remove_statistical_outliers(50, 2.0)
        pcl_inlier = pcl.select_by_mask(inlier_mask, invert=False)
        pcl_inlier.paint_uniform_color((0., 0., 0.))
        pcl_outlier = pcl.select_by_mask(inlier_mask, invert=True)
        pcl_outlier.paint_uniform_color((1., 0., 0.))
        geometries_visualize([pcl_inlier, pcl_outlier])
        pcl = filtered_pcl

    geometries_visualize([mesh, pcl, cams[cam_no].get_cs(50.)])

    if False:
        # For debugging: Visualize normals
        vis = o3d.visualization.Visualizer()
        vis.create_window()
        vis.add_geometry(pcl.to_legacy())
        vis.get_render_option().point_show_normal = True
    #    vis.get_render_option().point_color_option = o3d.visualization.PointColorOption.Normal
        vis.run()
        vis.destroy_window()
