# Lighthouse: 3D Reconstruction with Stereo Deflectometry

## The problem

3D reconstructions of specular reflective surfaces (e.g. highly polished metal) are tricky. Cameras looking onto the object do not see the object itself but just the distorted mirror image of the surroundings. A method for 3D reconstructing specular surfaces is the *deflectometry*. The *stereo deflectometry* approach uses one or more screens placed around the specular object. The screens display a time series of different patterns and the reflections of these patterns are recorded by multiple cameras. These images can be used for 3D reconstruction.

This approach requires models of cameras and screens and knowledge of all the model parameters and the exact 6d pose of all screens and cameras in a world coordinate system. This knowledge has to be determined by a calibration process, which is not described in this article.

## Test setup

Our test setup has a roughly planar object in bright gray in the center. The object has a slightly concave form an has a slight dent to the right of the world coordinate system in the center. There are three cameras looking directly onto the object. On the far side are two screens tilted towards the viewer.

![](images/setup.png)

The arrangement of cameras and screens relative to the object is crucial to have a good coverage for a 3D reconstruction:

* Most camera rays should hit the object
* Most reflected rays should hit a screen
* Every area of the object should be covered by rays from at least two cameras

Regarding these rules, our test setup is good enough: the rays shown here start at the cameras positions according to the viewing direction (cam0: red, cam1: green, cam2: blue), are reflected by the object and hit one of the screens.

![](images/setup_rays.png)

## Correspondence matching

The subject of correspondence matching has already been covered in the chapter about 3D reconstruction with structured light, so we don't get too far into details. The essence is: We display a series of patterns on the screens over a period of time. By observing this pattern with the cameras, we are able to find for each camera pixel a corresponding pixel on any of the screens if either the screen pixel is directly visible by the camera or its reflection in the object.

These plots show the camera images, the colors indicate if we found a matching pixel on a screen and - if so - on which screen. Most of the matching areas are on the bottom two-thirds of the camera image, these are the reflections of the screen in the object. The area at the top is the direct view of the screen. The setup explains that screen0 is visible on the right, screen1 on the left.

![](images/matches_cam0.png)
![](images/matches_cam1.png)
![](images/matches_cam2.png)

This plot show the screen images and where to camera pixels map to. The colors encode the camera numbers. Each cross denotes where the pixel of a camera is mapped to the screen (to make this plot more visible, the camera resolutions have been drastically reduced). The different view angles of cam0 and cam1 compared to cam2 can be seen in the mapping: the blue points reach further up. The dent is clearly visible as changes in the regular pattern of e.g. the blue dots.

![](images/matches_screens.png)

Full camera pixel coordinates map to sub-pixel locations on the screen. For our calculations we need to map sub-pixel camera locations to a screen. We do that by a grid-based interpolation that is able to handle missing values in the grid (non-existing mappings).

## Calculating normal vectors

Key to 3D reconstruction in stereo deflectometry is the estimation of a normal vector for an arbitrary 3D point $P_o$ (that may lay on the surface of the object) by using a single camera view:

* Project the 3D object point candidate $P_o$ to the camera chip using the camera model yielding $p_o$; if the point is not on the chip, abort
* For $p_o$ find a corresponding 2D point $p_s$ on any of the screens (if not abort)
* Transform 2D point into 3D point $P_s$ using the screen model (the 3D location of the pixel in world coordinates, simple for a planar screen, more complex for curved screens)
* Determine the incoming ray from screen to object point: $\vec{i}=P_o-P_s$
* Determine the outgoing ray from object point to camera: $\vec{o}=P_c-P_o$ with $P_c$ being the location of the camera
* If the angle between $\vec{i}$ and $\vec{o}$ is close to 180deg or close to zero the camera looks directly on the screen without reflections, in this case abort
* Calculate the normal vector $\vec{n}$ under the assumption incoming and outgoing rays result from a reflection $\vec{n}=|\vec{i}|\vec{o}-|\vec{o}|\vec{i}$ and normalize $\vec{n}$

## Finding object points

As shown in the previous section, we can calculate a normal vector for arbitrary points from a single camera. But how does it help to find points on the surface of the object? The solution is the following: We calculate the normal vectors for the point from multiple cameras, one normal vector per camera. If the normal vectors coincide, the point is on the surface of the object. If not, the point is not.

Lets generate a number of equidistant points on the a camera ray and visualize the normal vectors for each point calculated from the three cameras: The camera is located at the top left (not visible). The darker gray area at the bottom is the surface. The first points at the op left are only visible by a single camera, so we have a single normal. Later we have three normal vectors with certain angles between them. Later we have two remaining normal vectors and the angle between them is lower and lower, close to the surface the angle is near zero. Underneath the surface the angles increase again (not visible here).

![](images/normals_along_ray.png)

Lets define an error function for normal vectors: For two or more normal vectors the error is the maximum angle of all angles between all pairs of vectors (for 3 normal vectors, we have 3 pairs; for 4 normal vectors 6 pairs and so on). For less than two normal vectors the function returns NaN. For three arbitrary rays we generate points along these rays, calculate the normal vectors and evaluate the error function. As a result the error function (angle in degrees) is plotted over the distance of the points from the camera along the ray. As expected, all three curves have an absolute minimum close to zero. For example the plot for ray0 has a minimum at around 50mm distance from the camera. This is where an object point is located. The curve for ray1 ends after 82mm. Behind that the points on the ray are not visible by more than a single camera. Curves may  have local minima. This happens e.g. when the number of rays for a point drops and the most deviant disappears.

![](images/angles_along_ray.png)

Finding a minimum of these curves is not trivial due to the local minima.

## 3D reconstruction

To find object points, we try to minimize the angle error along each of the camera rays. A brute force approach would just sample each ray at many points and choose the point with the minimum angle error. We use some kind of bi-sectioning approach by coarse sampling along the ray and then re-sampling around the minimum of the coarse sampling.

With this approach we can reconstruct a dense point cloud of 780k points (rays just from cam2) within a few minutes. The RMS error of the raw point cloud compared to the mesh used for image generation is 0.29mm. The point cloud is colored by normal map which helps us to see the dent in surface.

![](images/pcl_normalmap.png)
