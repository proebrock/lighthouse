import os
import sys
import time
import glob

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

sys.path.append(os.path.abspath('../'))
from common.image_utils import image_load_multiple
from camsimlib.camera_model import CameraModel
from camsimlib.screen import Screen
from common.pixel_matcher import ImageMatcher


if __name__ == "__main__":
    # Random but reproducible
    np.random.seed(42)
    # Get data path
    data_path_env_var = 'LIGHTHOUSE_DATA_DIR'
    if data_path_env_var in os.environ:
        data_dir = os.environ[data_path_env_var]
        data_dir = os.path.join(data_dir, 'structured_light')
    else:
        data_dir = 'data'
    data_dir = os.path.abspath(data_dir)
    print(f'Using data from "{data_dir}"')

    print('Loading configuration ...')
    # Load cameras
    cam_filenames = sorted(glob.glob(os.path.join(data_dir, 'cam????.json')))
    cams = []
    for i, filename in enumerate(cam_filenames):
        cam = CameraModel()
        cam.json_load(filename)
        cams.append(cam)
    print(f'    Found {len(cams)} cams')
    # Load screens
    screen_filenames = sorted(glob.glob(os.path.join(data_dir, 'screen????.json')))
    screens = []
    for i, filename in enumerate(screen_filenames):
        screen = Screen()
        screen.json_load(filename)
        screens.append(screen)
    print(f'    Found {len(screens)} screens')
    # Load matchers
    matcher_filenames = sorted(glob.glob(os.path.join(data_dir, 'matcher????.json')))
    matchers = []
    for i, filename in enumerate(matcher_filenames):
        matcher = ImageMatcher()
        matcher.json_load(filename)
        matchers.append(matcher)
    print(f'    Found {len(matchers)} matchers')
    assert len(matchers) == len(screens)

    # Run matching
    matches = []
    for cam_no, cam in enumerate(cams):
        print(f'Matching for cam{cam_no} ...')
        coords = np.zeros(
            (len(screens), cam.get_chip_size()[1], cam.get_chip_size()[0], 2)
        )
        for screen_no, screen in enumerate(screens):
            print(f'    Matching for screen{screen_no} ...')
            filenames = os.path.join(data_dir,
                f'screen{screen_no:04d}_pattern????_cam{cam_no:04d}.png')
            images = image_load_multiple(filenames)
            coords[screen_no, :, :, :] = matchers[screen_no].match(images)
        matches.append(coords)

    # Filter duplicate matches
    # We expect every camera pixel to match to no screen or to one screen,
    # but not to multiple screens; if this is the case, we rather remove
    # these duplicate matches: better no match than a wrong one...
    invalid_masks = []
    for cam_no, match in enumerate(matches):
        num_matches = np.sum(np.all(np.isfinite(match), axis=3), axis=0)
        invalid_mask = num_matches >= 2
        print(f'Filtered {np.sum(invalid_mask)} invalid matches for cam{cam_no}')
        match[:, invalid_mask, :] = np.nan
        invalid_masks.append(invalid_mask)

    if True:
        # Show matches per camera
        colors = np.array(((1., 0, 0), (0, 1., 0), (0, 0, 1.),
            (0, 1., 1.), (1., 0, 1.), (1., 1., 0)))
        for cam_no, cam in enumerate(cams):
            img = np.zeros((cam.get_chip_size()[1], cam.get_chip_size()[0], 3),
                dtype=np.uint8)
            img[invalid_masks[cam_no]] = (255, 255, 255)
            legend_handles = []
            for screen_no in range(len(screens)):
                mask = np.all(np.isfinite(matches[cam_no][screen_no, :, : ,:]), axis=2)
                img[mask] = 255 * colors[screen_no].astype(np.uint8)
                legend_handles.append(mpatches.Patch(
                    color=colors[screen_no], label=f'screen{screen_no}'))
            fig, ax = plt.subplots()
            ax.imshow(img)
            ax.set_title(f'cam{cam_no}')
            ax.legend(handles=legend_handles)
        # Show matches per screen
        for screen_no in range(len(screens)):
            fig, ax = plt.subplots()
            for cam_no in range(len(cams)):
                points = matches[cam_no][screen_no, :, : ,:].reshape((-1, 2))
                ax.plot(points[:, 0], points[:, 1], '.', color=colors[cam_no],
                    label=f'cam{cam_no}', ms=1)
            ax.set_title(f'screen{screen_no}')
            ax.set_aspect('equal')
            ax.axes.xaxis.set_ticks([])
            ax.axes.yaxis.set_ticks([])
            ax.legend()
        plt.show()

    # Save results
    print('Saving matches to file ...')
    filename = os.path.join(data_dir, 'matches.npz')
    np.savez(filename, *matches)
    print('    Done.')
